#!/bin/env python
#
# xbootimg.py - version 1.0
#
# (c) 2010 Thomas Giesel
#
# This software is provided 'as-is', without any express or implied
# warranty.  In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# Thomas Giesel skoe directbox com
#

import sys
import os
from getopt import getopt
import struct

def usage():
    print '''Usage: %(prog)s [options] <bin-file> <boot-file>

Add a length indicator and a CRC to <bin-file> to make it bootable by
XS1-L1 ROM SPI boot loader. Save the result to <boot-file>.

The format is described in the XS1-L System Specification.

Options:

   --reverse (-r)           Reverse bits in output bytes, used for SPI load
   --no-crc (-n)            Set CRC to 0x0D15AB1E to disable the CRC check
   --help (-h)              Show this usage message
''' % {"prog" : os.path.basename(sys.argv[0])}

poly = 0xedb88320

def crc32_word(crc, data, poly):
    '''Does the same as the XMOS opcode.'''
    for i in range(0, 32):
        if (crc & 1):
            crc = (crc >> 1) ^ poly
        else:
            crc = crc >> 1
    return crc ^ data


def crc32_byte(crc, data, poly):
    '''Does the same as the XMOS opcode.'''
    for i in range(0, 8):
        if (crc & 1):
            crc = (crc >> 1) ^ poly
        else:
            crc = crc >> 1
    return crc ^ (data << 24)


def reverse_bits(str):
    '''Reverse the bit order in all bytes in STR'''
    ret = []
    for c in str:
        val = ord(c)
        rev = 0
        for pos in range(0, 8):
            if val & (1 << pos):
                rev |= (1 << (7 - pos))
        ret.append('%c' % rev)
    return ''.join(ret)


def xbootimg(bin_name, boot_name, reverse, no_crc):
    try:
        bin_file = open(bin_name, 'rb')
        bin_data = bin_file.read()
    except Exception, e:
        print >>sys.stderr, 'Failed to read file: ' + str(e)
        return 1
    words = (len(bin_data) + 3) / 4
    while (len(bin_data) < words * 4):
        bin_data += '\0'

    try:
        boot_file = open(boot_name, 'wb')
        crc = 0xffffffff

        # length of image
        out = struct.pack("i", words)
        crc = crc32_word(crc, words, poly)

        # image data
        out += bin_data
        for c in bin_data:
            crc = crc32_byte(crc, ord(c), poly)

        # crc
        crc = crc32_word(crc, 0, poly)
        if no_crc:
            crc = 0x0D15AB1E
        crc ^= 0xffffffff
        out += struct.pack("I", crc)

        if reverse:
            out = reverse_bits(out)
        boot_file.write(out)
        boot_file.close()
    except Exception, e:
        print >>sys.stderr, 'Failed to write boot file: ' + str(e)
        return 1


def main():
    try:
        opts, args = getopt(sys.argv[1:], "rnh",
                            ["reverse", "no-crc", "help"])
    except Exception, e:
        print >>sys.stderr, "Failed to parse command line: " + str(e)
        return 2

    # Process options
    reverse = 0
    no_crc = 0
    for opt, value in opts:
        if opt in ["--reverse", "-r"]:
            reverse = 1
        elif opt in ["--no-crc", "-n"]:
            no_crc = 1
        elif opt in ["--help", "-h"]:
            usage()
            return 0
        else:
            print >>sys.stderr, "Unknown option '%s'" % opt
            print >>sys.stderr, "Use --help for help"
            return 3

    # Process arguments
    if len(args) != 2:
        print >>sys.stderr, "Wrong number of arguments"
        print >>sys.stderr, "Use --help for help"
        return 3

    bin_name  = args[0]
    boot_name = args[1]
    return xbootimg(bin_name, boot_name, reverse, no_crc);


if __name__ == "__main__":
    sys.exit(main())

