#!/bin/env python
# -*- coding: utf-8 -*-
#
# xbootimg.py - version 1.0
#
# (c) 2010 Thomas Giesel
#
# This software is provided 'as-is', without any express or implied
# warranty.  In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# Thomas Giesel skoe directbox com
#

import sys
import os
from getopt import getopt
from select import select
import struct
import serial
import tty
import termios

def usage():
    print '''Usage: %(prog)s [options] [<boot-file>]

Send a boot image through a serial port to EBoot and/or start a UART terminal.

Options:

    --reverse (-r)         Reverse bit order in bytes
    --device=<dev> (-d)    Serial port to be used (/dev/ttyS0)
    --command=<cmd> (-c)   Send this command followed by <cr><lf> first
    --terminal (-t)        Start UART terminal after file has been transfered
    --help (-h)            Show this usage message
''' % {"prog" : os.path.basename(sys.argv[0])}


def reverse_bits(str):
    '''Reverse the bit order in all bytes in STR'''
    ret = []
    for c in str:
        val = ord(c)
        rev = 0
        for pos in range(0, 8):
            if val & (1 << pos):
                rev |= (1 << (7 - pos))
        ret.append('%c' % rev)
    return ''.join(ret)


def transmit_image(ser, dev, reverse, command, name):
    try:
        img_file = open(name, 'rb')
        img_data = img_file.read()
        img_file.close()
    except Exception, e:
        print >>sys.stderr, 'Failed to read file: ' + str(e)
        return 1
    if reverse:
        img_data = reverse_bits(img_data)

    try:
        print '\n=== XTransmit ===\n'
        success = False
        if command:
            ser.write(command);
            ser.write('\r\n');

        while not success:
            sys.stdout.write('Waiting for EBoot... ')
            sys.stdout.flush()
            handshake_ok = False
            while not handshake_ok:
                ser.write('!')
                response = ser.read(1)
                if response and response[0] == '?':
                    handshake_ok = True
            ser.write('>')
            print 'ok'
            print 'Sending file...'
            ser.write(img_data)
            ser.flush()
            
            sys.stdout.write('Waiting for confirmation... ')
            sys.stdout.flush()
            confirm_rcvd = False
            while not confirm_rcvd:
                response = ser.read(1)
                if response:
                    if response[0] in ['A', 'N']:
                        confirm_rcvd = True
            if response[0] == 'A':
                success = True;
            else:
                print 'error, retry'
        print 'ok'
    except serial.SerialException, e:
        print >>sys.stderr, 'Failed to access ' + dev + ': ' + str(e)
        return 1
    return 0


def terminal(ser, dev):
    input = sys.stdin.fileno()
    old_settings = termios.tcgetattr(input)
    try:
        tty.setraw(input)
        end = False
        while not end:
            events = select([ser, input], [], [])
            if input in events[0]:
                c = sys.stdin.read(1)
                if c == chr(3): # Ctrl + C
                    end = True
                else:
                    ser.write(c)
            if ser in events[0]:
                c = ser.read(1)
                sys.stdout.write(c)
                sys.stdout.flush()
    except serial.SerialException, e:
        termios.tcsetattr(input, termios.TCSADRAIN, old_settings)
        print >>sys.stderr, 'Failed to access ' + dev + ': ' + str(e)
        return 1
    finally:
        termios.tcsetattr(input, termios.TCSADRAIN, old_settings)
    print '\npfff!'
    return 0


def main():
    try:
        opts, args = getopt(sys.argv[1:], "rd:c:th",
                            ["reverse", "device=", "command=", "terminal", "help"])
    except Exception, e:
        print >>sys.stderr, "Failed to parse command line: " + str(e)
        return 2

    # Process options
    reverse = start_term = False
    command = None
    dev = '/dev/ttyS0'
    for opt, value in opts:
        if opt in ["--reverse", "-r"]:
            reverse = True
        elif opt in ["--device", "-d"]:
            dev = value
        elif opt in ["--command", "-c"]:
            command = value
        elif opt in ["--terminal", "-t"]:
            start_term= True
        elif opt in ["--help", "-h"]:
            usage()
            return 0
        else:
            print >>sys.stderr, "Unknown option '%s'" % opt
            print >>sys.stderr, "Use --help for help"
            return 3

    # Process arguments
    if len(args) > 1:
        print >>sys.stderr, "Wrong number of arguments"
        print >>sys.stderr, "Use --help for help"
        return 3
    elif len(args) == 1:
        name  = args[0]
    else:
        name = None

    rv = 0

    ser = serial.Serial(dev, 115200, timeout=0.1);
    if name:
        rv = transmit_image(ser, dev, reverse, command, name)
    if rv == 0 and start_term:
        terminal(ser, dev)
    ser.close()

    return rv

if __name__ == "__main__":
    sys.exit(main())

