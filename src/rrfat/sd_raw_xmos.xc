
/*
 * Copyright (c) 2006-2010 by Roland Riegel <feedback@roland-riegel.de>
 * XMOS port:
 * Copyright (c) 2010 by Thomas Giesel <skoe@directbox.com>
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#include <xclib.h>
#include <xs1.h>
#include <stdint.h>
#include <config.h>

/* cannot include sd_raw.h here */

static clock blk1 = XS1_CLKBLK_1;
static clock blk2 = XS1_CLKBLK_2;

/* configure these ports in your *.xn file */
static out port ss              = PORT_SD_SPI_SS;
static out buffered port:8 mosi = PORT_SD_SPI_MOSI;
static out buffered port:8 sclk = PORT_SD_SPI_CLK;
static in buffered port:8 miso  = PORT_SD_SPI_MISO;


void sdcard_xmit_spi(unsigned char b);
unsigned char sdcard_rcvr_spi();

void sd_raw_platform_init()
{
    // configure ports and clock blocks
    //configure_clock_rate(blk1, 100, /* spi_clock_div */ 10);

    // divide 1 => 100 MHz / (2 * 1) => 50 MHz =>
    //      50M sclk edges per second => 25 MHz SPI clock
    // divide 63 => 100 MHz / (2 * 63) => ~794 kHz =>
    //      794k sclk edges per second => ~397 kHz SPI clock
    configure_clock_ref(blk1, 1);

    // send sclk bits with this frequency
    configure_out_port(sclk, blk1, 0);

    // use sclk as input for second clock block
    configure_clock_src(blk2, sclk);

    // end the second clock block for mosi/miso
    configure_out_port(mosi, blk2, 0);
    configure_in_port(miso, blk2);

    clearbuf(mosi);
    clearbuf(sclk);
    start_clock(blk1);
    start_clock(blk2);

    // is this needed when I use '1' as initial state?
    sclk <: 0xFF;


#if 0
    /* enable inputs for reading card status */
    configure_pin_available();
    configure_pin_locked();

    /* enable outputs for MOSI, SCK, SS, input for MISO */
    configure_pin_mosi();
    configure_pin_sck();
    configure_pin_ss();
    configure_pin_miso();

    unselect_card();

    /* initialize SPI with lowest frequency; max. 400kHz during identification mode of card */
    SPCR = (0 << SPIE) | /* SPI Interrupt Enable */
           (1 << SPE)  | /* SPI Enable */
           (0 << DORD) | /* Data Order: MSB first */
           (1 << MSTR) | /* Master mode */
           (0 << CPOL) | /* Clock Polarity: SCK low when idle */
           (0 << CPHA) | /* Clock Phase: sample on rising SCK edge */
           (1 << SPR1) | /* Clock Frequency: f_OSC / 128 */
           (1 << SPR0);
    SPSR &= ~(1 << SPI2X); /* No doubled clock frequency */
#endif
}

/**
 * \ingroup sd_raw
 * Switch to highest SPI frequency possible.
 */
void sd_raw_fast_spi()
{
}

/**
 * \ingroup sd_raw
 * Sends a raw byte to the memory card.
 *
 * \param[in] b The byte to sent.
 * \see sd_raw_rec_byte
 */
void sd_raw_send_byte(uint8_t b)
{
    // send MSB first
    unsigned x = bitrev(b) >> 24;
    // set data t obe sent
    mosi <: x;
    // pattern for 8 clock cycles
    sclk <: 0xAA;
    sclk <: 0xAA;
    // wait until 8 clock cycles were sent
    sync(sclk);
    // discard incoming data
    miso :> void;
}

/**
 * \ingroup sd_raw
 * Receives a raw byte from the memory card.
 *
 * \returns The byte which should be read.
 * \see sd_raw_send_byte
 */
uint8_t sd_raw_rec_byte()
{
    unsigned x;
    clearbuf(miso);
    // pattern for 8 clock cycles
    sclk <: 0xAA;
    sclk <: 0xAA;
    // wait until 8 clock cycles were sent
    sync(sclk);
    // read the result
    miso :> x;
    // received MSB first
    return bitrev(x) >> 24;
}

/**
 * \ingroup sd_raw
 * Select the memory card.
 */
void sd_raw_select_card()
{
    ss <: 0;
}

/**
 * \ingroup sd_raw
 * Unselect the memory card.
 */
void sd_raw_unselect_card()
{
    ss <: 1;
}
