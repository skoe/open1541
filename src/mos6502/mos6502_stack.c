/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008, 2009 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>

#include "mos6502.h"


void mos6502_prepare_irq(void)
{
    unsigned val;

    // push PC
    // PC points to next op already, but on stack there must be (next - 1)
    mos6502_pc--;
    mos6502_pc &= 0xffff;
    mos6502_memw_raw(0x100 + (mos6502_sp & 0xff), mos6502_pc >> 8);
    --mos6502_sp;
    mos6502_memw_raw(0x100 + (mos6502_sp & 0xff), mos6502_pc);
    --mos6502_sp;

    // push status
    // always push B=1 (?)
    val = mos6502_flags | MOS6502_BFLAG | MOS6502_ONE_FLAG;
    mos6502_memw_raw(0x100 + (mos6502_sp & 0xff), val);
    --mos6502_sp;

    // Read IRQ vector
    mos6502_pc = (mos6502_memr_raw(0xfffe) +
                  (mos6502_memr_raw(0xffff) << 8));
    mos6502_flags |= MOS6502_IFLAG;
    // The whole IRQ entry takes 7 cycles
    mos6502_time += 7 * TICKS_PER_US;
}

void op_08(void)
{ // PHP
    unsigned val;

    // PHP always pushs B=1
    val = mos6502_flags | MOS6502_BFLAG | MOS6502_ONE_FLAG;

    mos6502_memw_raw(0x100 + (mos6502_sp & 0xff), val);
    --mos6502_sp;
}


void op_20(void)
{ // JSR $FFFF
    unsigned dest;

    dest = mos6502_memr_arg16();

    // PUSH high byte
    // PC points to next op, on stack there must be (next - 1)
    mos6502_pc--;
    mos6502_pc &= 0xffff;
    mos6502_memw_raw(0x100 + (mos6502_sp & 0xff), mos6502_pc >> 8);
    --mos6502_sp;
    mos6502_memw_raw(0x100 + (mos6502_sp & 0xff), mos6502_pc);
    --mos6502_sp;

    mos6502_pc = dest;
}


void op_28(void)
{ // PLP
    unsigned val;

    ++mos6502_sp;
    val = mos6502_memr_raw(0x100 + (mos6502_sp & 0xff));
    val &= ~MOS6502_BFLAG;
    mos6502_flags = val;
}


void op_48(void)
{ // PHA
    mos6502_memw_raw(0x100 + (mos6502_sp & 0xff), mos6502_a);
    --mos6502_sp;
}


void op_40(void)
{ // RTI
    unsigned val;

    ++mos6502_sp;
    val = mos6502_memr_raw(0x100 + (mos6502_sp & 0xff));
    val &= ~MOS6502_BFLAG;
    mos6502_flags = val;
    ++mos6502_sp;
    val  = mos6502_memr_raw(0x100 + (mos6502_sp & 0xff));
    ++mos6502_sp;
    val |= mos6502_memr_raw(0x100 + (mos6502_sp & 0xff)) << 8;

    mos6502_pc = val + 1;
    mos6502_pc &= 0xffff;
}


void op_60(void)
{ // RTS
    unsigned dest;

    ++mos6502_sp;
    dest  = mos6502_memr_raw(0x100 + (mos6502_sp & 0xff));
    ++mos6502_sp;
    dest |= mos6502_memr_raw(0x100 + (mos6502_sp & 0xff)) << 8;

    mos6502_pc = dest + 1;
    mos6502_pc &= 0xffff;
}


void op_68(void)
{ // PLA
    ++mos6502_sp;
    mos6502_a = mos6502_memr_raw(0x100 + (mos6502_sp & 0xff));
    update_nz(mos6502_a);
}


void op_9a(void)
{ // TXS
    mos6502_sp = mos6502_x;
}


void op_ba(void)
{ // TSX
    mos6502_x = (mos6502_sp & 0xff);
    update_nz(mos6502_x);
}
