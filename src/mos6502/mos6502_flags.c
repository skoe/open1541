/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008, 2009 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe//directbox.com
 */

#include "mos6502.h"


void do_bit(unsigned val) {
    // remove V, N, Z
    mos6502_flags &= ~(MOS6502_VFLAG | MOS6502_NFLAG | MOS6502_ZFLAG);

    mos6502_flags |= val & (MOS6502_NFLAG | MOS6502_VFLAG);

    if ((mos6502_a & val) == 0)
        mos6502_flags |= MOS6502_ZFLAG;
}


// update flags N and Z, only take into account the lower 8 bits of val
void update_nz(unsigned val)
{
    mos6502_flags &= ~(MOS6502_ZFLAG | MOS6502_NFLAG);

    if ((val & 0xff) == 0)
        mos6502_flags |= MOS6502_ZFLAG;

    mos6502_flags |= val & MOS6502_NFLAG;
}


void op_24(void)
{ // BIT $FF
    do_bit(mos6502_memr_zp());
}


void op_2c(void)
{ // BIT $FFFF
    do_bit(mos6502_memr_abs());
}



#if 0
void op_00(void)
{ // BRK
#endif

void op_xx(void)
{
}
