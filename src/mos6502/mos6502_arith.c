/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <stdio.h>
#include "mos6502.h"


// todo: optimize me!
// reference: http://www.6502.org/tutorials/decimal_mode.html#A
void do_adc(unsigned v) {
    int      carry, tmp;
    int      sres;
    unsigned ures;

    carry = !!(mos6502_flags & MOS6502_CFLAG);

    // remove C, V, N, Z
    mos6502_flags &=
            ~(MOS6502_CFLAG | MOS6502_VFLAG | MOS6502_NFLAG | MOS6502_ZFLAG);

    if (mos6502_flags & MOS6502_DFLAG)
    {
        // decimal mode
        tmp = (mos6502_a & 0x0f) + (v & 0x0f) + carry;
        if (tmp >= 0x0a)
            tmp = ((tmp + 0x06) & 0x0f) + 0x10;

        ures = (mos6502_a & 0xf0) + (v & 0xf0) + tmp;

        if (ures >= 0xa0)
            ures += 0x60;

        // 2c. A = (A & $F0) + (B & $F0) + AL, using signed (twos complement) arithmetic
        sres = (int8_t)(mos6502_a & 0xf0) + (int8_t)(v & 0xf0) + (int8_t)tmp;

        // 2e. The N flag result is 1 if bit 7 of A is 1, and is 0 if bit 7 if A is 0
        /* set N */
        if (sres & 0x80)
            mos6502_flags |= MOS6502_NFLAG;

        // 2f. The V flag result is 1 if A < -128 or A > 127, and is 0 if -128 <= A <= 127
        /* set V */
        if (sres < -128 || sres > 127)
            mos6502_flags |= MOS6502_VFLAG;

        /* set Z - from binary add */
        if (!((mos6502_a + v + carry) & 0xff))
            mos6502_flags |= MOS6502_ZFLAG;

        mos6502_a = ures & 0xff;
    }
    else
    {
        // binary mode
        sres = (int)((int8_t)mos6502_a) + (int8_t)v + carry;
        ures = mos6502_a + v + carry;

        /* set N */
        if (ures & 0x80)
            mos6502_flags |= MOS6502_NFLAG;

        /* set V */
        if (sres < -128 || sres > 127)
            mos6502_flags |= MOS6502_VFLAG;

        mos6502_a = ures & 0xff;

        /* set Z */
        if (!mos6502_a)
            mos6502_flags |= MOS6502_ZFLAG;
    }

    /* set C */
    if (ures > 0xff)
        mos6502_flags |= MOS6502_CFLAG;
}


// todo: optimize me!
void do_sbc(unsigned v) {
    int      carry;
    int      sres, tmp;
    unsigned ures;

    carry = !!(mos6502_flags & MOS6502_CFLAG);

    sres = (int)((int8_t)mos6502_a) - (int8_t)v + 1 - carry;
    ures = mos6502_a - v + 1 - carry;

    // remove C, V, N, Z
    mos6502_flags &=
            ~(MOS6502_CFLAG | MOS6502_VFLAG | MOS6502_NFLAG | MOS6502_ZFLAG);

    if (mos6502_flags & MOS6502_DFLAG)
    {
        // 3a. AL = (A & $0F) - (B & $0F) + C-1
        tmp = (int8_t)(mos6502_a & 0x0f) - (int8_t)(v & 0x0f) + carry - 1;

        // 3b. If AL < 0, then AL = ((AL - $06) & $0F) - $10
        if (tmp < 0)
            tmp = ((tmp - 0x06) & 0x0f) - 0x10;

        // 3c. A = (A & $F0) - (B & $F0) + AL
        tmp = (int8_t)(mos6502_a & 0xf0) - (int8_t)(v & 0xf0) + tmp;
        
        // 3d. If A < 0, then A = A - $60
        if (tmp < 0)
            tmp -= 0x60;
        
        // 3e. The accumulator result is the lower 8 bits of A
        mos6502_a = tmp & 0xff;
    }
    else
    {
        mos6502_a = ures & 0xff;
    }


    /* set C */
    if (!(ures & 0x100))
        mos6502_flags |= MOS6502_CFLAG;

    /* set V */
    if (sres < -128 || sres > 127)
        mos6502_flags |= MOS6502_VFLAG;

    /* set N */
    if (mos6502_a & 0x80)
        mos6502_flags |= MOS6502_NFLAG;

    /* set Z */
    if (!mos6502_a)
        mos6502_flags |= MOS6502_ZFLAG;
}


/*******************************************************************************
 * Do DEC, write back the value to memory and update flags
 *
 ******************************************************************************/
static void do_dec_write_back(unsigned val) {
    --val;
    mos6502_memw_back(val);
    update_nz(val);
}


/*******************************************************************************
 * Do INC, write back the value to memory and update flags
 *
 ******************************************************************************/
static void do_inc_write_back(unsigned val) {
    ++val;
    mos6502_memw_back(val);
    update_nz(val);
}


void op_61(void)
{ // ADC ($FF,X)
    do_adc(mos6502_memr_indx());
}


void op_65(void)
{ // ADC $FF
    do_adc(mos6502_memr_zp());
}


void op_69(void)
{  // ADC #$FF
    do_adc(mos6502_memr_arg8());
}


void op_6d(void)
{ // ADC $FFFF
    do_adc(mos6502_memr_abs());
}


void op_71(void)
{ // ADC ($FF),Y
    do_adc(mos6502_memr_indy());
}


void op_75(void)
{ // ADC $FF,X
    do_adc(mos6502_memr_zpx());
}


void op_79(void)
{ // ADC $FFFF,Y
    do_adc(mos6502_memr_absy());
}


void op_7d(void)
{ // ADC $FFFF,X
    do_adc(mos6502_memr_absx());
}


void op_c6(void)
{ // DEC $FF
    do_dec_write_back(mos6502_memr_zp());
}


void op_ce(void)
{ // DEC $FFFF
    do_dec_write_back(mos6502_memr_abs());
}


void op_d6(void)
{ // DEC $FF,X
    do_dec_write_back(mos6502_memr_zpx());
}


void op_de(void)
{ // DEC $FFFF,X
    do_dec_write_back(mos6502_memr_absx3());
}


void op_e1(void)
{ // SBC ($FF,X)
    do_sbc(mos6502_memr_indx());
}


void op_e5(void)
{ // SBC $FF
    do_sbc(mos6502_memr_zp());
}


void op_e6(void)
{ // INC $FF
    do_inc_write_back(mos6502_memr_zp());
}


void op_e9(void)
{ // SBC #$FF
    do_sbc(mos6502_memr_arg8());
}


void op_ed(void)
{ // SBC $FFFF
    do_sbc(mos6502_memr_abs());
}


void op_ee(void)
{ // INC $FFFF
    do_inc_write_back(mos6502_memr_abs());
}


void op_f1(void)
{ // SBC ($FF),Y
    do_sbc(mos6502_memr_indy());
}


void op_f5(void)
{ // SBC $FF,X
    do_sbc(mos6502_memr_zpx());
}


void op_f6(void)
{ // INC $FF,X
    do_inc_write_back(mos6502_memr_zpx());
}


void op_f9(void)
{ // SBC $FFFF,Y
    do_sbc(mos6502_memr_absy());
}


void op_fd(void)
{ // SBC $FFFF,X
    do_sbc(mos6502_memr_absx());
}


void op_fe(void)
{ // INC $FFFF,X
    do_inc_write_back(mos6502_memr_absx3());
}
