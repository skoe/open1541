/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008, 2009 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#ifndef MOS6502_H
#define MOS6502_H

#include <xs1.h>
#include <stdint.h>

/*******************************************************************************
 ******************************************************************************/

// maximal number of breakpoints
#define NUM_BREAKPOINTS 3

// Size of client RAM
#define MOS6502_RAM_SIZE (2 * 1024)

// Size of client ROM
#define MOS6502_ROM_SIZE (16 * 1024)

// How many timer ticks we have in one us
#define TICKS_PER_US XS1_TIMER_MHZ

// This lets the machine run slower
//#define TICKS_PER_US (XS1_TIMER_MHZ * 10)


#define MOS6502_CTRL_RELEASE_IRQ_VIA1   0x100
#define MOS6502_CTRL_ASSERT_IRQ_VIA1    0x101
#define MOS6502_CTRL_RELEASE_IRQ_VIA2   0x102
#define MOS6502_CTRL_ASSERT_IRQ_VIA2    0x103
#define MOS6502_CTRL_SETV               0x104
#define MOS6502_CTRL_DISABLE            0x105
#define MOS6502_CTRL_ENABLE             0x106
#define MOS6502_CTRL_RESET              0x107
#define MOS6502_CTRL_RUN                0x108
#define MOS6502_CTRL_STOP               0x109
#define MOS6502_CTRL_STEP               0x10a
#define MOS6502_CTRL_REGS               0x10b

// If any of these bits is set, the IRQ line is asserted
#define MOS6502_IRQ_LINE_VIA1           0x01
#define MOS6502_IRQ_LINE_VIA2           0x02

/*******************************************************************************
 ******************************************************************************/
/* from mos6502_thread.xc */
#ifdef __XC__
void mos6502_thread(unsigned start_time,
                    streaming chanend mos6502_ctrl_ce,
                    streaming chanend mos6502_via1_ce,
                    streaming chanend mos6502_via2_ce,
                    streaming chanend mos6502_ui_ce);
#endif
void mos6502_sync_to_last_cycle(void);
void mos6502_exec_ctrl_cmd(unsigned ctrl_cmd);


/* from mos6502.c */
void mos6502_set_running(unsigned running);
void mos6502_step(void);
void mos6502_zp_x(void);
void mos6502_zp_y(void);

/* from mos6502_if.c */
void mos6502_init(void);
void mos6502_set_last_op_start(unsigned t);
unsigned mos6502_get_last_op_start(void);
void mos6502_dump_regs(void);
uint16_t mos6502_get_pc(void);
void mos6502_dump_mem(unsigned start, unsigned stop);
void mos6502_fill_mem(uint16_t from, uint16_t to, uint8_t val);
void mos6502_reset(void);
void mos6502_show_breakpoints(void);
void mos6502_set_breakpoint(unsigned addr);
void mos6502_rm_breakpoint(void);

/* from mos6502_dis.c */
uint16_t mos6502_dis(uint16_t start, uint16_t stop);

/* from mos6502_mem.c */

#ifdef __XC__
void mos6502_mem_set_via_chanends(streaming chanend mos6502_via1_ce,
                                  streaming chanend mos6502_via2_ce);
#else
void mos6502_mem_set_via_chanends(unsigned mos6502_via1_ca,
                                  unsigned mos6502_via2_ca);
#endif

unsigned mos6502_memr_raw(unsigned offset);

void mos6502_arg_to_addr8(void);
void mos6502_arg_to_addr16(void);
void mos6502_lda_immediate(void);
void mos6502_ldx_immediate(void);
void mos6502_ldy_immediate(void);
void mos6502_m_to_a(void);
void mos6502_m_to_x(void);
void mos6502_m_to_y(void);
void mos6502_a_to_m(void);
void mos6502_x_to_m(void);
void mos6502_y_to_m(void);
void mos6502_arg_to_addr16_x(void);
void mos6502_arg_to_addr16_y(void);
void mos6502_indirect_1(void);
void mos6502_indirect_2(void);
void mos6502_indirect_2_y(void);

unsigned mos6502_memr_opcode(void);
unsigned mos6502_memr_arg8(void);
unsigned mos6502_memr_arg16(void);

unsigned mos6502_memr_zp(void);
unsigned mos6502_memr_zpx(void);
unsigned mos6502_memr_zpy(void);
unsigned mos6502_memr_abs(void);
unsigned mos6502_memr_absx(void);
unsigned mos6502_memr_absx3(void);
unsigned mos6502_memr_absy(void);
unsigned mos6502_memr_indx(void);
unsigned mos6502_memr_indy(void);

void mos6502_memw_raw(unsigned offset, unsigned val);
void mos6502_memw_back(unsigned val);
void mos6502_memw_zp(unsigned val);
void mos6502_memw_zpx(unsigned val);
void mos6502_memw_zpy(unsigned val);
void mos6502_memw_abs(unsigned val);
void mos6502_memw_absx(unsigned val);
void mos6502_memw_absy(unsigned val);
void mos6502_memw_indx(unsigned val);
void mos6502_memw_indy(unsigned val);

/* from mos6502_flags.c */
void update_nz(unsigned val);

/* from mos6502_stack.c */
void mos6502_prepare_irq(void);

#ifdef MOS6502_TEST
/* from mos6502_test.c */
unsigned memr_test(unsigned offset);
void memw_test(unsigned offset, unsigned val);
#endif

/*******************************************************************************
 * Public data
 *
 ******************************************************************************/
extern unsigned mos6502_a;
extern unsigned mos6502_x;
extern unsigned mos6502_y;
extern unsigned mos6502_sp;
extern uint32_t mos6502_pc;
extern unsigned mos6502_flags;
extern unsigned mos6502_opcode;
extern unsigned mos6502_addr;
extern unsigned mos6502_data;
extern unsigned mos6502_bp;

// in timer clock cycles => 100 cycles per us
extern unsigned mos6502_time;

extern unsigned irq_state;


#define MOS6502_CFLAG    (1 << 0)
#define MOS6502_ZFLAG    (1 << 1)
#define MOS6502_IFLAG    (1 << 2)
#define MOS6502_DFLAG    (1 << 3)
#define MOS6502_BFLAG    (1 << 4)
#define MOS6502_ONE_FLAG (1 << 5)
#define MOS6502_VFLAG    (1 << 6)
#define MOS6502_NFLAG    (1 << 7)

// Client RAM
extern uint8_t ram[MOS6502_RAM_SIZE];

// Client ROM
extern uint8_t rom[MOS6502_ROM_SIZE];

extern unsigned mos6502_running;



// (nothing interesting below)

void op_00(void);
void op_01(void);
void op_02(void);
void op_03(void);
void op_04(void);
void op_05(void);
void op_06(void);
void op_07(void);
void op_08(void);
void op_09(void);
void op_0a(void);
void op_0b(void);
void op_0c(void);
void op_0d(void);
void op_0e(void);
void op_0f(void);
void op_10(void);
void op_11(void);
void op_12(void);
void op_13(void);
void op_14(void);
void op_15(void);
void op_16(void);
void op_17(void);
void op_18(void);
void op_19(void);
void op_1a(void);
void op_1b(void);
void op_1c(void);
void op_1d(void);
void op_1e(void);
void op_1f(void);
void op_20(void);
void op_21(void);
void op_22(void);
void op_23(void);
void op_24(void);
void op_25(void);
void op_26(void);
void op_27(void);
void op_28(void);
void op_29(void);
void op_2a(void);
void op_2b(void);
void op_2c(void);
void op_2d(void);
void op_2e(void);
void op_2f(void);
void op_30(void);
void op_31(void);
void op_32(void);
void op_33(void);
void op_34(void);
void op_35(void);
void op_36(void);
void op_37(void);
void op_38(void);
void op_39(void);
void op_3a(void);
void op_3b(void);
void op_3c(void);
void op_3d(void);
void op_3e(void);
void op_3f(void);
void op_40(void);
void op_41(void);
void op_42(void);
void op_43(void);
void op_44(void);
void op_45(void);
void op_46(void);
void op_47(void);
void op_48(void);
void op_49(void);
void op_4a(void);
void op_4b(void);
void op_4c(void);
void op_4d(void);
void op_4e(void);
void op_4f(void);
void op_50(void);
void op_51(void);
void op_52(void);
void op_53(void);
void op_54(void);
void op_55(void);
void op_56(void);
void op_57(void);
void op_58(void);
void op_59(void);
void op_5a(void);
void op_5b(void);
void op_5c(void);
void op_5d(void);
void op_5e(void);
void op_5f(void);
void op_60(void);
void op_61(void);
void op_62(void);
void op_63(void);
void op_64(void);
void op_65(void);
void op_66(void);
void op_67(void);
void op_68(void);
void op_69(void);
void op_6a(void);
void op_6b(void);
void op_6c(void);
void op_6d(void);
void op_6e(void);
void op_6f(void);
void op_70(void);
void op_71(void);
void op_72(void);
void op_73(void);
void op_74(void);
void op_75(void);
void op_76(void);
void op_77(void);
void op_78(void);
void op_79(void);
void op_7a(void);
void op_7b(void);
void op_7c(void);
void op_7d(void);
void op_7e(void);
void op_7f(void);
void op_80(void);
void op_81(void);
void op_82(void);
void op_83(void);
void op_84(void);
void op_85(void);
void op_86(void);
void op_87(void);
void op_88(void);
void op_89(void);
void op_8a(void);
void op_8b(void);
void op_8c(void);
void op_8d(void);
void op_8e(void);
void op_8f(void);
void op_90(void);
void op_91(void);
void op_92(void);
void op_93(void);
void op_94(void);
void op_95(void);
void op_96(void);
void op_97(void);
void op_98(void);
void op_99(void);
void op_9a(void);
void op_9b(void);
void op_9c(void);
void op_9d(void);
void op_9e(void);
void op_9f(void);
void op_a0(void);
void op_a1(void);
void op_a2(void);
void op_a3(void);
void op_a4(void);
void op_a5(void);
void op_a6(void);
void op_a7(void);
void op_a8(void);
void op_a9(void);
void op_aa(void);
void op_ab(void);
void op_ac(void);
void op_ad(void);
void op_ae(void);
void op_af(void);
void op_b0(void);
void op_b1(void);
void op_b2(void);
void op_b3(void);
void op_b4(void);
void op_b5(void);
void op_b6(void);
void op_b7(void);
void op_b8(void);
void op_b9(void);
void op_ba(void);
void op_bb(void);
void op_bc(void);
void op_bd(void);
void op_be(void);
void op_bf(void);
void op_c0(void);
void op_c1(void);
void op_c2(void);
void op_c3(void);
void op_c4(void);
void op_c5(void);
void op_c6(void);
void op_c7(void);
void op_c8(void);
void op_c9(void);
void op_ca(void);
void op_cb(void);
void op_cc(void);
void op_cd(void);
void op_ce(void);
void op_cf(void);
void op_d0(void);
void op_d1(void);
void op_d2(void);
void op_d3(void);
void op_d4(void);
void op_d5(void);
void op_d6(void);
void op_d7(void);
void op_d8(void);
void op_d9(void);
void op_da(void);
void op_db(void);
void op_dc(void);
void op_dd(void);
void op_de(void);
void op_df(void);
void op_e0(void);
void op_e1(void);
void op_e2(void);
void op_e3(void);
void op_e4(void);
void op_e5(void);
void op_e6(void);
void op_e7(void);
void op_e8(void);
void op_e9(void);
void op_ea(void);
void op_eb(void);
void op_ec(void);
void op_ed(void);
void op_ee(void);
void op_ef(void);
void op_f0(void);
void op_f1(void);
void op_f2(void);
void op_f3(void);
void op_f4(void);
void op_f5(void);
void op_f6(void);
void op_f7(void);
void op_f8(void);
void op_f9(void);
void op_fa(void);
void op_fb(void);
void op_fc(void);
void op_fd(void);
void op_fe(void);
void op_ff(void);
void op_xx(void); // <= unimplemented ops

#endif
