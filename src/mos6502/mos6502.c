/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>

#include "mos6502.h"


/*******************************************************************************
 * Public data
 *
 ******************************************************************************/

unsigned mos6502_a;
unsigned mos6502_x;
unsigned mos6502_y;
unsigned mos6502_sp;

/* We use bits 31..16 for the 6502 PC */
uint32_t mos6502_pc;

/* Same format as mos6502_pc */
unsigned mos6502_bp;

/* current/previous opcode */
unsigned mos6502_opcode;

unsigned mos6502_addr;
unsigned mos6502_data;

unsigned mos6502_flags;

// in timer clock cycles
unsigned mos6502_time;

// this is != 0 if the CPU is running
unsigned mos6502_running;

typedef void (*opcode_function_t)(void);

static const opcode_function_t mos6502_jump_table[256] =
{
    op_xx, op_01, op_xx, op_xx, op_xx, op_05, op_06, op_xx, // 0
    op_08, op_09, op_0a, op_xx, op_xx, op_0d, op_0e, op_xx,
    op_10, op_11, op_xx, op_xx, op_xx, op_15, op_16, op_xx, // 1
    op_18, op_19, op_xx, op_xx, op_xx, op_1d, op_1e, op_xx,
    op_20, op_21, op_xx, op_xx, op_24, op_25, op_26, op_xx, // 2
    op_28, op_29, op_2a, op_xx, op_2c, op_2d, op_2e, op_xx,
    op_30, op_31, op_xx, op_xx, op_xx, op_35, op_36, op_xx, // 3
    op_38, op_39, op_xx, op_xx, op_xx, op_3d, op_3e, op_xx,
    op_40, op_41, op_xx, op_xx, op_xx, op_45, op_46, op_xx, // 4
    op_48, op_49, op_4a, op_xx, op_4c, op_4d, op_4e, op_xx,
    op_50, op_51, op_xx, op_xx, op_xx, op_55, op_56, op_xx, // 5
    op_58, op_59, op_xx, op_xx, op_xx, op_5d, op_5e, op_xx,
    op_60, op_61, op_xx, op_xx, op_xx, op_65, op_66, op_xx, // 6
    op_68, op_69, op_6a, op_xx, op_6c, op_6d, op_6e, op_xx,
    op_70, op_71, op_xx, op_xx, op_xx, op_75, op_76, op_xx, // 7
    op_78, op_79, op_xx, op_xx, op_xx, op_7d, op_7e, op_xx,
    op_xx, op_81, op_xx, op_xx, op_84, op_85, op_86, op_xx, // 8
    op_88, op_xx, op_8a, op_xx, op_8c, op_8d, op_8e, op_xx,
    op_90, op_91, op_xx, op_xx, op_94, op_95, op_96, op_xx, // 9
    op_98, op_99, op_9a, op_xx, op_xx, op_9d, op_xx, op_xx,
    op_a0, op_a1, op_a2, op_xx, op_a4, op_a5, op_a6, op_xx, // a
    op_a8, op_a9, op_aa, op_xx, op_ac, op_ad, op_ae, op_xx,
    op_b0, op_b1, op_xx, op_xx, op_b4, op_b5, op_b6, op_xx, // b
    op_b8, op_b9, op_ba, op_xx, op_bc, op_bd, op_be, op_xx,
    op_c0, op_c1, op_xx, op_xx, op_c4, op_c5, op_c6, op_xx, // c
    op_c8, op_c9, op_ca, op_xx, op_cc, op_cd, op_ce, op_xx,
    op_d0, op_d1, op_xx, op_xx, op_xx, op_d5, op_d6, op_xx, // d
    op_d8, op_d9, op_xx, op_xx, op_xx, op_dd, op_de, op_xx,
    op_e0, op_e1, op_xx, op_xx, op_e4, op_e5, op_e6, op_xx, // e
    op_e8, op_e9, op_ea, op_xx, op_ec, op_ed, op_ee, op_xx,
    op_f0, op_f1, op_xx, op_xx, op_xx, op_f5, op_f6, op_xx, // f
    op_f8, op_f9, op_xx, op_xx, op_xx, op_fd, op_fe, op_xx
};


static const uint8_t mos6502_cycle_table[256] =
{
    7, 6, 0, 8, 3, 3, 5, 5,    3, 2, 2, 2, 4, 4, 6, 6, // 0
    2, 5, 0, 8, 4, 4, 6, 6,    2, 4, 2, 7, 4, 4, 7, 7, // 1
    6, 6, 0, 8, 3, 3, 5, 5,    4, 2, 2, 2, 4, 4, 6, 6, // 2
    2, 5, 0, 8, 4, 4, 6, 6,    2, 4, 2, 7, 4, 4, 7, 7, // 3
    6, 6, 0, 8, 3, 3, 5, 5,    3, 2, 2, 2, 3, 4, 6, 6, // 4
    2, 5, 0, 8, 4, 4, 6, 6,    2, 4, 2, 7, 4, 4, 7, 7, // 5
    6, 6, 0, 8, 3, 3, 5, 5,    4, 2, 2, 2, 5, 4, 6, 6, // 6
    2, 5, 0, 8, 4, 4, 6, 6,    2, 4, 2, 7, 4, 4, 7, 7, // 7
    2, 6, 2, 6, 3, 3, 3, 3,    2, 2, 2, 2, 4, 4, 4, 4, // 8
    2, 6, 0, 5, 4, 4, 4, 4,    2, 5, 2, 5, 5, 5, 5, 5, // 9
    2, 6, 2, 6, 3, 3, 3, 3,    2, 2, 2, 2, 4, 4, 4, 4, // A
    2, 5, 0, 5, 4, 4, 4, 4,    2, 4, 2, 4, 4, 4, 4, 4, // B
    2, 6, 2, 8, 3, 3, 5, 5,    2, 2, 2, 2, 4, 4, 6, 6, // C
    2, 5, 0, 8, 4, 4, 6, 6,    2, 4, 2, 7, 4, 4, 7, 7, // D
    2, 6, 2, 8, 3, 3, 5, 5,    2, 2, 2, 2, 4, 4, 6, 6, // E
    2, 5, 0, 8, 4, 4, 6, 6,    2, 4, 2, 7, 4, 4, 7, 7  // F
};


// todo: replace with mutex
void mos6502_set_running(unsigned running)
{
    mos6502_running = running;
}

/*******************************************************************************
 * Execute a single CPU instruction
 *
 ******************************************************************************/
#pragma stackfunction 64
void mos6502_step(void)
{
    if (!(mos6502_flags & MOS6502_IFLAG))
    {
        if (irq_state)
        {
            mos6502_prepare_irq();
            // The time has been updated, next step will be a real instruction
            return;
        }
    }

    mos6502_opcode = mos6502_memr_opcode();

    // while an instruction is executed, mos6502_time contains
    // the time to start the next instruction already
    mos6502_time += TICKS_PER_US * mos6502_cycle_table[mos6502_opcode];
    mos6502_jump_table[mos6502_opcode]();
}


/*******************************************************************************
 * ADDR = (ADDR + X) & 0xff
 *
 ******************************************************************************/
void mos6502_zp_x(void)
{
    mos6502_addr = (mos6502_addr + mos6502_x) & 0xff;
}


/*******************************************************************************
 * ADDR = (ADDR + Y) & 0xff
 *
 ******************************************************************************/
void mos6502_zp_y(void)
{
    mos6502_addr = (mos6502_addr + mos6502_y) & 0xff;
}

