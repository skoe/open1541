/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include "mos6502.h"

static void do_cmp(unsigned reg, unsigned arg)
{
    signed val;

    // remove C, N, Z
    mos6502_flags &= ~(MOS6502_CFLAG | MOS6502_NFLAG | MOS6502_ZFLAG);

    val = (signed)reg - (signed)arg;
    if ((val & 0xff) == 0)
        mos6502_flags |= MOS6502_ZFLAG;
    if (val & 0x80)
        mos6502_flags |= MOS6502_NFLAG;
    if (val >= 0)
        mos6502_flags |= MOS6502_CFLAG;
}


void op_c0(void)
{ // CPY #$FF
    do_cmp(mos6502_y, mos6502_memr_arg8());
}


void op_c1(void)
{ // CMP ($FF,X)
    do_cmp(mos6502_a, mos6502_memr_indx());

}


void op_c4(void)
{ // CPY $FF
    do_cmp(mos6502_y, mos6502_memr_zp());
}


void op_c5(void)
{ // CMP $FF
    do_cmp(mos6502_a, mos6502_memr_zp());
}


void op_c9(void)
{ // CMP #$FF
    do_cmp(mos6502_a, mos6502_memr_arg8());
}


void op_cc(void)
{ // CPY $FFFF
    do_cmp(mos6502_y, mos6502_memr_abs());
}


void op_cd(void)
{ // CMP $FFFF
    do_cmp(mos6502_a, mos6502_memr_abs());
}


void op_d1(void)
{ // CMP ($FF),Y
    do_cmp(mos6502_a, mos6502_memr_indy());
}


void op_d5(void)
{ // CMP $FF,X
    do_cmp(mos6502_a, mos6502_memr_zpx());
}


void op_d9(void)
{ // CMP $FFFF,Y
    do_cmp(mos6502_a, mos6502_memr_absy());
}


void op_dd(void)
{ // CMP $FFFF,X
    do_cmp(mos6502_a, mos6502_memr_absx());
}


void op_e0(void)
{ // CPX #$FF
    do_cmp(mos6502_x, mos6502_memr_arg8());
}


void op_e4(void)
{ // CPX $FF
    do_cmp(mos6502_x, mos6502_memr_zp());
}


void op_ec(void)
{ // CPX $FFFF
    do_cmp(mos6502_x, mos6502_memr_abs());
}
