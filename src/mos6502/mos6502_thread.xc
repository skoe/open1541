/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

/*
 * Todo:
 * - Implement BCD mode
 * - Put SP into high byte, so wrapping needn't to be done manually
 * - VIA doesn't stop when 6502 is stopped (e.g. on breakpoints)
 */

/*
 *       _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
 * Phi  | |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_
 * T     1   2   3   4   5   6   7   8
 * Inst |-------|---------------|---------|
 *              ^
 * 1.    NOP    t               ^
 * 2.            LDA $xxxx      t
 *
 */

#include <xs1.h>

#include <config.h>
#include <uart.h>
#include <util.h>
#include <via.h>

#include "mos6502.h"

static void mos6502_check_channels(streaming chanend mos6502_ctrl_ce,
                                   streaming chanend mos6502_via1_ce,
                                   streaming chanend mos6502_via2_ce,
                                   streaming chanend mos6502_ui_ce);
static void mos6502_step_with_breakpoint(void);
static void mos6502_dump_state(void);
static void mos6502_ctrl_run(void);
static void mos6502_ctrl_stop(void);
static void mos6502_ctrl_step(void);

// If != 0 IRQ line is asserted. Contains bits of MOS6502_IRQ_LINE_*
unsigned irq_state;

static timer    tmr;

#define MOS6502_TIMING_TEST
#ifdef MOS6502_TIMING_TEST
static out port test_port = PORT_TEST1;
#endif

/*******************************************************************************
 * The CPU emulation thread.
 *
 ******************************************************************************/
void mos6502_thread(unsigned start_time,
                    streaming chanend mos6502_ctrl_ce,
                    streaming chanend mos6502_via1_ce,
                    streaming chanend mos6502_via2_ce,
                    streaming chanend mos6502_ui_ce)
{
    int      i;
    unsigned t_now;

    mos6502_time = start_time;
    mos6502_init();
    mos6502_mem_set_via_chanends(mos6502_via1_ce, mos6502_via2_ce);

    irq_state = 0;
    mos6502_set_running(0);

    for (;;)
    {
        mos6502_check_channels(mos6502_ctrl_ce, mos6502_via1_ce,
                               mos6502_via2_ce, mos6502_ui_ce);

        // keep 6502 is about x us before real time, this gives us more time
        // for I/O accesses
        tmr when timerafter(mos6502_time - 0 * TICKS_PER_US) :> void;

        mos6502_set_last_op_start(mos6502_time);
#ifdef MOS6502_TIMING_TEST
        test_port <: 1;
#endif
        mos6502_step_with_breakpoint();
#ifdef MOS6502_TIMING_TEST
        test_port <: 0;
#endif
    } // for
}


/*******************************************************************************
 * Execute a control command.
 *
 ******************************************************************************/
void mos6502_exec_ctrl_cmd(unsigned ctrl_cmd)
{
    switch(ctrl_cmd)
    {
    case MOS6502_CTRL_ASSERT_IRQ_VIA1:
        irq_state |= MOS6502_IRQ_LINE_VIA1;
        break;

    case MOS6502_CTRL_RELEASE_IRQ_VIA1:
        irq_state &= ~MOS6502_IRQ_LINE_VIA1;
        break;

    case MOS6502_CTRL_ASSERT_IRQ_VIA2:
        irq_state |= MOS6502_IRQ_LINE_VIA2;
        break;

    case MOS6502_CTRL_RELEASE_IRQ_VIA2:
        irq_state &= ~MOS6502_IRQ_LINE_VIA2;
        break;

    case MOS6502_CTRL_SETV:
        mos6502_flags |= MOS6502_VFLAG;
        break;

    case MOS6502_CTRL_DISABLE:
        break;

    case MOS6502_CTRL_ENABLE:
        break;

    case MOS6502_CTRL_RESET:
        uart_puts("sys reset\r\n");
        mos6502_reset();
        // todo: mos6502_via_ce <: VIA_ACCESS_CMD_RESET << VIA_ACCESS_CMD_SHIFT;
        break;

    case MOS6502_CTRL_RUN:
        mos6502_ctrl_run();
        break;

    case MOS6502_CTRL_STOP:
        mos6502_ctrl_stop();
        break;

    case MOS6502_CTRL_STEP:
        mos6502_ctrl_step();
        break;

    case MOS6502_CTRL_REGS:
        mos6502_dump_regs();
        if (mos6502_running)
            mos6502_time += 0x1000 * TICKS_PER_US;
        break;

    default:
        break;
    }
}


/*******************************************************************************
 * Check all channels and
 *
 ******************************************************************************/
static void mos6502_check_channels(streaming chanend mos6502_ctrl_ce,
                                   streaming chanend mos6502_via1_ce,
                                   streaming chanend mos6502_via2_ce,
                                   streaming chanend mos6502_ui_ce)
{
    unsigned ctrl_cmd;
    unsigned got_event;

    do
    {
        got_event = 1;
        select
        {
        case mos6502_ctrl_ce :> ctrl_cmd:
            mos6502_exec_ctrl_cmd(ctrl_cmd);
            break;

        case mos6502_via1_ce :> ctrl_cmd:
            mos6502_exec_ctrl_cmd(ctrl_cmd);
            break;

        case mos6502_via2_ce :> ctrl_cmd:
            mos6502_exec_ctrl_cmd(ctrl_cmd);
            break;

        case mos6502_ui_ce :> ctrl_cmd:
            mos6502_exec_ctrl_cmd(ctrl_cmd);
            break;

        default:
            got_event = 0;
            break;
        }
    }
    while (got_event);
}


/*******************************************************************************
 * Check the breakpoints and execute one instruction.
 *
 ******************************************************************************/
static void mos6502_step_with_breakpoint(void)
{
    if (mos6502_running)
    {
        if (mos6502_bp == mos6502_pc)
        {
            uart_puts("breakpoint\r\n");
            mos6502_dump_state();
            mos6502_set_running(0);
        }
    }

    // may have been stopped by a breakpoint
    if (mos6502_running)
    {
        // fetch opcode and possibly emulate the whole old-style instruction
        mos6502_step();
    }
    else
    {
        // we're not running, advance the timer nevertheless to make sure the
        // main loop doesn't hang
        mos6502_time += 4 * TICKS_PER_US;
    }

    //if (mos6502_pc >> 16 == 0x062a)
      //  uart_puthex_padded(2, mos6502_a);
}


/*******************************************************************************
 * Show the registers and disassemble the next instruction.
 *
 ******************************************************************************/
static void mos6502_dump_state(void)
{
    mos6502_dump_regs();
    mos6502_dis(mos6502_pc, mos6502_pc);
}


/*******************************************************************************
 * Start the CPU.
 *
 ******************************************************************************/
static void mos6502_ctrl_run(void)
{
    if (!mos6502_running)
    {
        mos6502_set_running(1);
        // don't stop again at the same breakpoint...
        mos6502_step();
    }
}


/*******************************************************************************
 * Stop the CPU and dump the current state. Print an error message if it is not
 * running.
 *
 ******************************************************************************/
static void mos6502_ctrl_stop(void)
{
    if (mos6502_running)
    {
        mos6502_set_running(0);
        mos6502_dump_state();
    }
    else
    {
        uart_puts("Not running\r\n");
    }
}


/*******************************************************************************
 * Do a single CPU step and dump the current state. Du nothing if the CPU
 * is still running.
 *
 ******************************************************************************/
static void mos6502_ctrl_step(void)
{
    // ignore a single step if we're running anyway
    if (!mos6502_running)
    {
        mos6502_step();
        mos6502_dump_state();
    }
}

