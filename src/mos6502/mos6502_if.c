/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008, 2009 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <stdint.h>
#include <string.h>
#include <xs1.h>
#include <uart.h>
#include "mos6502.h"

static unsigned mos6502_last_op_start;


/*******************************************************************************
 * Initialize the 6502 processor emulation.
 *
 * t_start      Timer value to start the next (first) emulated clock cycle
 * 
 ******************************************************************************/
void mos6502_init(void)
{
    mos6502_bp = ~0;
    mos6502_reset();
}


/*******************************************************************************
 * For debugging
 *
 ******************************************************************************/
void mos6502_set_last_op_start(unsigned t)
{
    mos6502_last_op_start = t;
}


/*******************************************************************************
 * For debugging
 *
 ******************************************************************************/
unsigned mos6502_get_last_op_start(void)
{
    return mos6502_last_op_start;
}


/*******************************************************************************
 * Show the registers on UART.
 *
 ******************************************************************************/
void mos6502_dump_regs(void)
{
    uart_puts("      TIME  PC  AC XR YR SP NV#BDIZC\r\n");
    //         1234567890 1234 11 22 33 44 ********
    uart_putdec_padded(10, mos6502_time / TICKS_PER_US);
    uart_putc(' ');
    uart_puthex_padded_sp(4, mos6502_pc);
    uart_puthex_padded_sp(2, mos6502_a);
    uart_puthex_padded_sp(2, mos6502_x);
    uart_puthex_padded_sp(2, mos6502_y);
    uart_puthex_padded_sp(2, mos6502_sp);
    uart_putc(mos6502_flags & MOS6502_NFLAG ? 'N' : '-');
    uart_putc(mos6502_flags & MOS6502_VFLAG ? 'V' : '-');
    uart_puts("---");
    uart_putc(mos6502_flags & MOS6502_IFLAG ? 'I' : '-');
    uart_putc(mos6502_flags & MOS6502_ZFLAG ? 'Z' : '-');
    uart_putc(mos6502_flags & MOS6502_CFLAG ? 'C' : '-');
    if (irq_state)
        uart_puts(" IRQ asserted");

    uart_putcrlf();
}

/*******************************************************************************
 * Print a hexdump of client memory from <start> to <stop> (excluded) to UART.
 *
 ******************************************************************************/
void mos6502_dump_mem(unsigned start, unsigned stop)
{
    unsigned c;
    char text[17];
    unsigned llen;

    /* exclude "stop" from output */
    while (start < stop)
    {
        memset(text, 0, sizeof(text));
        llen = 16;
        uart_puthex_padded(4, start);
        uart_puts(": ");
        while (llen && start < stop)
        {
            c = mos6502_memr_raw(start);
            uart_puthex_padded_sp(2, c);

            if (c < ' ' || c > 126)
                c = '.';
            text[16 - llen] = c;

            if ((--llen & 3) == 0)
                uart_putc(' ');
            start++;
        }
        uart_putc(' ');
        uart_puts(text);
        uart_putcrlf();
    }
}

/*******************************************************************************
 * Fill <from>  to <to> (included) with <val>.
 *
 ******************************************************************************/
void mos6502_fill_mem(uint16_t from, uint16_t to, uint8_t val)
{
    while (from <= to)
    {
        mos6502_memw_raw(from++, val);
    }
}

/*******************************************************************************
 * Print a list of breakpoints set.
 *
 ******************************************************************************/
void mos6502_show_breakpoints(void)
{
    uart_puthex_padded(4, mos6502_bp);
}

/*******************************************************************************
 * Set a breakpoint at addr. If there is a breakpoint at this address already,
 * let the call succeed without setting another one.
 *
 ******************************************************************************/
void mos6502_set_breakpoint(unsigned addr)
{
    mos6502_bp = addr << 16;
}


/*******************************************************************************
 * Remove a breakpoint.
 *
 ******************************************************************************/
void mos6502_rm_breakpoint(void)
{
	mos6502_bp = ~0;
}


/*******************************************************************************
 * Initialize all registers and read program counter from reset vector.
 * This is more than actually needed, but makes test more reproducable.
 *
 ******************************************************************************/
void mos6502_reset(void)
{
    // todo: Stop emulation while doing this
    mos6502_a  = 0;
    mos6502_x  = 0;
    mos6502_y  = 0;
    mos6502_sp = 0xff;
    mos6502_flags = 0;

    // Read start address from reset vector
    mos6502_pc = (mos6502_memr_raw(0xfffc) +
                 (mos6502_memr_raw(0xfffd) << 8));
}
