
#ifndef MOS6502_ASM_H
#define MOS6502_ASM_H

#define BLA 3

#define SECTION_DATA   .section .dp.data, "awd", @progbits
#define SECTION_RODATA .section .cp.rodata, "ac", @progbits
#define SECTION_TEXT   .text

#define r_rv    r1
#define r_addr  r2
#define r_val   r3

#define r_a     r4
#define r_x     r5
#define r_y     r6
#define r_sp    r7
#define r_pc    r8
#define r_flags r9
#define r_time  r10


#define MOS6502_CFLAG    (1 << 0)
#define MOS6502_ZFLAG    (1 << 1)
#define MOS6502_IFLAG    (1 << 2)
#define MOS6502_DFLAG    (1 << 3)
#define MOS6502_BFLAG    (1 << 4)
#define MOS6502_ONE_FLAG (1 << 5)
#define MOS6502_VFLAG    (1 << 6)
#define MOS6502_NFLAG    (1 << 7)

#define MOS6502_ROM_ADDR 0xc000

#define TICKS_PER_US 100

#endif
