

#include "mos6502_asm.h"


SECTION_RODATA
        .align 4

SECTION_TEXT
        .align 2


/*******************************************************************************
 * Read a byte from memory.
 *
 * input:   r_addr  address - bits 16..31 must be 0
 *
 * return:  r_rv    result value
 *
 * changes: r0
 *
 ******************************************************************************/
        .globl mos6502_mread
        .globl mos6502_mread_ret1
mos6502_mread:
        entsp   1
mos6502_mread_ret1:
        // ADR  5432 1098 7654 3210
        // RAM  0xx0 0xxx xxxx xxxx
        // VIA1 0001 10xx xxxx xxxx
        // VIA2 0001 11xx xxxx xxxx
        // ROM  11xx xxxx xxxx xxxx
        shr     r0, r_addr, 8
        shr     r0, r0, 3
        bru     r0
        BRFU_u6 mos6502_mread_ram   // 0x0000
        BRFU_u6 mos6502_mread_none  // 0x0800
        BRFU_u6 mos6502_mread_none  // 0x1000
        BRFU_u6 mos6502_mread_via   // 0x1800
        BRFU_u6 mos6502_mread_ram   // 0x2000
        BRFU_u6 mos6502_mread_none  // 0x2800
        BRFU_u6 mos6502_mread_none  // 0x3000
        BRFU_u6 mos6502_mread_none  // 0x3800
        BRFU_u6 mos6502_mread_ram   // 0x4000
        BRFU_u6 mos6502_mread_none  // 0x4800
        BRFU_u6 mos6502_mread_none  // 0x5000
        BRFU_u6 mos6502_mread_none  // 0x5800
        BRFU_u6 mos6502_mread_ram   // 0x6000
        BRFU_u6 mos6502_mread_none  // 0x6800
        BRFU_u6 mos6502_mread_none  // 0x7000
        BRFU_u6 mos6502_mread_none  // 0x7800
        BRFU_u6 mos6502_mread_none  // 0x8000
        BRFU_u6 mos6502_mread_none  // 0x8800
        BRFU_u6 mos6502_mread_none  // 0x9000
        BRFU_u6 mos6502_mread_none  // 0x9800
        BRFU_u6 mos6502_mread_none  // 0xa000
        BRFU_u6 mos6502_mread_none  // 0xa800
        BRFU_u6 mos6502_mread_none  // 0xb000
        BRFU_u6 mos6502_mread_none  // 0xb800
        BRFU_u6 mos6502_mread_rom   // 0xc000
        BRFU_u6 mos6502_mread_rom   // 0xc800
        BRFU_u6 mos6502_mread_rom   // 0xd000
        BRFU_u6 mos6502_mread_rom   // 0xd800
        BRFU_u6 mos6502_mread_rom   // 0xe000
        BRFU_u6 mos6502_mread_rom   // 0xe800
        BRFU_u6 mos6502_mread_rom   // 0xf000
        // BRFU_u6 mos6502_mread_rom   // 0xf800 - fall through

mos6502_mread_rom:
        // mask to 16k
        ldc     r0, 16 * 1024 - 1
        and     r0, r_addr, r0
        ldaw    r_rv, dp[rom]
        ld8u    r_rv, r_rv[r0]
        retsp   1

mos6502_mread_ram:
        // mask to 2k
        ldc     r0, 2 * 1024 - 1
        and     r0, r_addr, r0
        ldaw    r_rv, dp[ram]
        ld8u    r_rv, r_rv[r0]
mos6502_mread_none:
mread_via_end:
        retsp   1

mos6502_mread_via:
        // 0xCC00AAAAVV, CC = command, AAAA = address, VV = value
        shl     r11, r_addr, 8      // addr << VIA_CHANNEL_REG_SHIFT - todo: mask address here?

        ldc     r0, 0x04            // VIA2_BASE - VIA1_BASE = 0x0400
        shl     r0, r0, 8           // 0x04 => 0x0400
        and     r0, r_addr, r0
        bt      r0, mread_via2
        ldw     r0, dp[via1_ce]
        out     res[r0], r11
        out     res[r0], r_time
        bu	mread_via_wait

mread_via2:
        ldw     r0, dp[via2_ce]
        out     res[r0], r11
        out     res[r0], r_time

mread_via_wait:
	// receive response from via
	in	r_rv, res[r0]
	// if it is < 0x100 => response gotten, end
	ldc	r11, 0x100
	lsu	r11, r_rv, r11
	bt	r11, mread_via_end
	// otherwise: this is a control command
	bl	exec_ctrl_cmd_wrapper
	bu 	mread_via_wait

exec_ctrl_cmd_wrapper:
	entsp 	6
	stw	r0, sp[1]
	stw	r1, sp[2]
	stw	r2, sp[3]
	stw	r3, sp[4]
	stw	r11, sp[5]
	mov	r0, r_rv
	bl	mos6502_exec_ctrl_cmd
	ldw	r0, sp[1]
	ldw	r1, sp[2]
	ldw	r2, sp[3]
	ldw	r3, sp[4]
	ldw	r11, sp[5]	
	retsp 	6

/*******************************************************************************
 * Write a byte to memory.
 *
 * input:   r_addr  address - bits 16..31 must be 0
 *          r_val   value
 * changes: r0, r11
 *
 ******************************************************************************/
        .globl mos6502_mwrite
        .globl mos6502_mwrite2
mos6502_mwrite:
        entsp   1
mos6502_mwrite_ret1:
        // ADR  5432 1098 7654 3210
        // RAM  0xx0 0xxx xxxx xxxx
        // VIA1 0001 10xx xxxx xxxx
        // VIA2 0001 11xx xxxx xxxx
        // ROM  11xx xxxx xxxx xxxx
        shr     r0, r_addr, 8
        shr     r0, r0, 3
        bru     r0
        BRFU_u6 mos6502_mwrite_ram_ret1     // 0x0000
        BRFU_u6 mos6502_mwrite_none         // 0x0800
        BRFU_u6 mos6502_mwrite_none         // 0x1000
        BRFU_u6 mos6502_mwrite_via          // 0x1800
        BRFU_u6 mos6502_mwrite_ram_ret1     // 0x2000
        BRFU_u6 mos6502_mwrite_none         // 0x2800
        BRFU_u6 mos6502_mwrite_none         // 0x3000
        BRFU_u6 mos6502_mwrite_none         // 0x3800
        BRFU_u6 mos6502_mwrite_ram_ret1     // 0x4000
        BRFU_u6 mos6502_mwrite_none         // 0x4800
        BRFU_u6 mos6502_mwrite_none         // 0x5000
        BRFU_u6 mos6502_mwrite_none         // 0x5800
        BRFU_u6 mos6502_mwrite_ram_ret1     // 0x6000
        BRFU_u6 mos6502_mwrite_none         // 0x6800
        BRFU_u6 mos6502_mwrite_none         // 0x7000
        BRFU_u6 mos6502_mwrite_none         // 0x7800
        BRFU_u6 mos6502_mwrite_none         // 0x8000
        BRFU_u6 mos6502_mwrite_none         // 0x8800
        BRFU_u6 mos6502_mwrite_none         // 0x9000
        BRFU_u6 mos6502_mwrite_none         // 0x9800
        BRFU_u6 mos6502_mwrite_none         // 0xa000
        BRFU_u6 mos6502_mwrite_none         // 0xa800
        BRFU_u6 mos6502_mwrite_none         // 0xb000
        BRFU_u6 mos6502_mwrite_none         // 0xb800
        BRFU_u6 mos6502_mwrite_none         // 0xc000
        BRFU_u6 mos6502_mwrite_none         // 0xc800
        BRFU_u6 mos6502_mwrite_none         // 0xd000
        BRFU_u6 mos6502_mwrite_none         // 0xd800
        BRFU_u6 mos6502_mwrite_none         // 0xe000
        BRFU_u6 mos6502_mwrite_none         // 0xe800
        BRFU_u6 mos6502_mwrite_none         // 0xf000
        // BRFU_u6 mos6502_mwrite_none      // 0xf800 - fall through

mos6502_mwrite_none:
        retsp   1

mos6502_mwrite_ram:
        entsp   1
mos6502_mwrite_ram_ret1:
        // mask to 2k
        ldc     r0, 2 * 1024 - 1
        and     r0, r_addr, r0
        ldaw    r11, dp[ram]
        st8     r_val, r11[r0]
        retsp   1

mos6502_mwrite_via:
        // 0xCC00AAAAVV, CC = command, AAAA = address, VV = value
        ldc     r11, 1              // VIA_CHANNEL_CMD_WRITE << VIA_CHANNEL_CMD_SHIFT
        shl     r11, r11, 24
        shl     r0, r_addr, 8       // addr << VIA_CHANNEL_REG_SHIFT - todo: mask address here?
        or      r11, r11, r0
        or      r11, r11, r_val

        ldc     r0, 0x04            // VIA2_BASE - VIA1_BASE = 0x0400
        shl     r0, r0, 8           // 0x04 => 0x0400
        and     r0, r_addr, r0
        bt      r0, mos6502_mwrite_via2
        ldw     r0, dp[via1_ce]
        out     res[r0], r11
        out     res[r0], r_time
        retsp   1
mos6502_mwrite_via2:
        ldw     r0, dp[via2_ce]
        out     res[r0], r11
        out     res[r0], r_time
        retsp   1


/*******************************************************************************
 * Fetch an argument byte and increment PC.
 *
 * return:  r_rv    result value
 *
 * changes: r0, r_addr
 *
 ******************************************************************************/
        .globl mos6502_mread_arg8
mos6502_mread_arg8:
        entsp   1
        mov     r_addr, r_pc
        add     r_pc, r_pc, 1
        zext    r_pc, 16
        bu      mos6502_mread_ret1


/*******************************************************************************
 * Fetch two argument bytes and add 2 to PC.
 *
 * return:  r_rv    result value
 *
 * changes: r0, r11, r_addr
 *
 ******************************************************************************/
        .globl mos6502_mread_arg16
mos6502_mread_arg16:
        entsp   1

        mov     r_addr, r_pc
        add     r_pc, r_pc, 1
        zext    r_pc, 16
        bl      mos6502_mread
        mov     r11, r_rv

        mov     r_addr, r_pc
        add     r_pc, r_pc, 1
        zext    r_pc, 16
        bl      mos6502_mread
        shl	    r_rv, r_rv, 8
        or      r_rv, r_rv, r11
        retsp   1


/*******************************************************************************
 * Adressing mode read ZP. Fetch one argument byte and add 1 to PC.
 * Read the byte from the resulting address.
 *
 * return:  r_rv    result value
 *          r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mread_zp
mos6502_mread_zp:
        entsp   1
        bl      mos6502_mread_arg8
        mov     r_addr, r_rv
        bu      mos6502_mread_ram


/*******************************************************************************
 * Adressing mode write ZP. Fetch one argument byte and add 1 to PC.
 * Write the byte to the resulting address.
 *
 * input:   r_val   value
 * return:  r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mwrite_zp
        .globl mos6502_mwrite_zp_ret1
mos6502_mwrite_zp:
        entsp   1
mos6502_mwrite_zp_ret1:
        bl      mos6502_mread_arg8
        mov     r_addr, r_rv
        bl      mos6502_mwrite_ram
        bu      leave_asm_function_ret1


/*******************************************************************************
 * Adressing mode read ZP,X. Fetch one argument byte and add 1 to PC, add X
 * to the result. Read the byte at the resulting address.
 *
 * return:  r_rv    result value
 *          r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mread_zpx
mos6502_mread_zpx:
        entsp   1
        bl      mos6502_mread_arg8
        add     r_addr, r_rv, r_x
        zext    r_addr, 8
        bu      mos6502_mread_ram


/*******************************************************************************
 * Adressing mode write ZP,X. Fetch one argument bytes and add 1 to PC, add X
 * to the result. Write the byte to the resulting address.
 *
 * input:   r_val   value
 * return:  r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mwrite_zpx
        .globl mos6502_mwrite_zpx_ret1
mos6502_mwrite_zpx:
        entsp   1
mos6502_mwrite_zpx_ret1:
        bl      mos6502_mread_arg8
        add     r_addr, r_rv, r_x
        zext    r_addr, 8
        bl      mos6502_mwrite_ram
        bu      leave_asm_function_ret1


/*******************************************************************************
 * Adressing mode read ZP,Y. Fetch one argument byte and add 1 to PC, add Y
 * to the result. Read the byte at the resulting address.
 *
 * return:  r_rv    result value
 *          r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mread_zpy
mos6502_mread_zpy:
        entsp   1
        bl      mos6502_mread_arg8
        add     r_addr, r_rv, r_y
        zext    r_addr, 8
        bu      mos6502_mread_ram


/*******************************************************************************
 * Adressing mode write ZP,Y. Fetch one argument bytes and add 1 to PC, add Y
 * to the result. Write the byte to the resulting address.
 *
 * input:   r_val   value
 * return:  r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mwrite_zpy
        .globl mos6502_mwrite_zpy_ret1
mos6502_mwrite_zpy:
        entsp   1
mos6502_mwrite_zpy_ret1:
        bl      mos6502_mread_arg8
        add     r_addr, r_rv, r_y
        zext    r_addr, 8
        bl      mos6502_mwrite_ram
        bu      leave_asm_function_ret1


/*******************************************************************************
 * Adressing mode read ABS. Fetch two argument bytes and add 2 to PC.
 * Read the byte from the resulting address.
 *
 * return:  r_rv    result value
 *          r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mread_abs
mos6502_mread_abs:
        entsp   1
        bl      mos6502_mread_arg16
        mov     r_addr, r_rv
        bu      mos6502_mread_ret1


/*******************************************************************************
 * Adressing mode write ABS. Fetch two argument bytes and add 2 to PC.
 * Write the byte to the resulting address.
 *
 * input:   r_val   value
 * return:  r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mwrite_abs
        .globl mos6502_mwrite_abs_ret1
mos6502_mwrite_abs:
        entsp   1
mos6502_mwrite_abs_ret1:
        bl      mos6502_mread_arg16
        mov     r_addr, r_rv
        bl      mos6502_mwrite
        bu      leave_asm_function_ret1

/*******************************************************************************
 * Adressing mode read ABS,X. Fetch two argument bytes and add 2 to PC, add X
 * to the result. Read the byte at the resulting address.
 *
 * return:  r_rv    result value
 *          r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mread_absx
mos6502_mread_absx:
        entsp   1
        bl      mos6502_mread_arg16
        shr     r11, r_rv, 8        // remember high-byte of base address
        add     r_addr, r_rv, r_x
        zext    r_addr, 16
        shr     r0, r_addr, 8       // get high byte of effective address
        eq      r0, r0, r11         // same page?
        bt      r0, mread_absx_nowrap
        ldc     r0, TICKS_PER_US
        add     r_time, r_time, r0  // add a cycle
mread_absx_nowrap:
        bu      mos6502_mread_ret1


/*******************************************************************************
 * Adressing mode write ABS,X. Fetch two argument bytes and add 2 to PC, add X
 * to the result. Write the byte to the resulting address.
 *
 * input:   r_val   value
 * return:  r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mwrite_absx
        .globl mos6502_mwrite_absx_ret1
mos6502_mwrite_absx:
        entsp   1
mos6502_mwrite_absx_ret1:
        bl      mos6502_mread_arg16
        add     r_addr, r_rv, r_x
        zext    r_addr, 16
        bl      mos6502_mwrite
        bu      leave_asm_function_ret1


/*******************************************************************************
 * Adressing mode read ABS,Y. Fetch two argument bytes and add 2 to PC, add Y
 * to the result. Read the byte at the resulting address.
 *
 * return:  r_rv    result value
 *          r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mread_absy
mos6502_mread_absy:
        entsp   1
        bl      mos6502_mread_arg16
        shr     r11, r_rv, 8        // remember high-byte of base address
        add     r_addr, r_rv, r_y
        zext    r_addr, 16
        shr     r0, r_addr, 8       // get high byte of effective address
        eq      r0, r0, r11         // same page?
        bt      r0, mread_absy_nowrap
        ldc     r0, TICKS_PER_US
        add     r_time, r_time, r0
mread_absy_nowrap:
        bu      mos6502_mread_ret1


/*******************************************************************************
 * Adressing mode write ABS,X. Fetch two argument bytes and add 2 to PC, add X
 * to the result. Write the byte to the resulting address.
 *
 * input:   r_val   value
 * return:  r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mwrite_absy
        .globl mos6502_mwrite_absy_ret1
mos6502_mwrite_absy:
        entsp   1
mos6502_mwrite_absy_ret1:
        bl      mos6502_mread_arg16
        add     r_addr, r_rv, r_y
        zext    r_addr, 16
        bl      mos6502_mwrite
        bu      leave_asm_function_ret1


/*******************************************************************************
 * Adressing mode read (IND,X). Fetch one argument byte and add 1 to PC, add
 * X to this value, take the lower 8 bits. Read an address from this location.
 * Read the byte at the resulting address.
 *
 * return:  r_rv    result value
 *          r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mread_indx
mos6502_mread_indx:
        entsp   1
        // read argument and add x
        bl      mos6502_mread_arg8
        add     r_addr, r_rv, r_x
        zext    r_addr, 8
        // read 16 bit address
        bl      mos6502_mread
        mov     r11, r_rv
        add     r_addr, r_addr, 1
        zext    r_addr, 16
        bl      mos6502_mread
        shl     r_rv, r_rv, 8
        or      r_addr, r11, r_rv
        // read final value
        bu      mos6502_mread_ret1


/*******************************************************************************
 * Adressing mode write (IND,X). Fetch one argument byte and add 1 to PC, add
 * X to this value, take the lower 8 bits. Read an address from this location.
 * Write the byte to the resulting address.
 *
 * input:   r_val   value
 * return:  r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mwrite_indx
        .globl mos6502_mwrite_indx_ret1
mos6502_mwrite_indx:
        entsp   1
mos6502_mwrite_indx_ret1:
        // read argument and add x
        bl      mos6502_mread_arg8
        add     r_addr, r_rv, r_x
        zext    r_addr, 8
        // read 16 bit address
        bl      mos6502_mread
        mov     r11, r_rv
        add     r_addr, r_addr, 1
        zext    r_addr, 16
        bl      mos6502_mread
        shl     r_rv, r_rv, 8
        or      r_addr, r11, r_rv
        // write value
        bl      mos6502_mwrite
        bu      leave_asm_function_ret1


/*******************************************************************************
 * Adressing mode read (IND),Y. Fetch one argument byte and add 1 to PC, read
 * an address from this location. add Y to the result.
 * Read the byte at the resulting address.
 * Add 1 clock cycle if page boundary was crossed.
 *
 * return:  r_rv    result value
 *          r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mread_indy
mos6502_mread_indy:
        entsp   1
        bl      mos6502_mread_arg8
        mov     r_addr, r_rv
        // read 16 bit address
        bl      mos6502_mread
        mov     r11, r_rv
        add     r_addr, r_addr, 1
        zext    r_addr, 16
        bl      mos6502_mread
        shl     r0, r_rv, 8             // r_rv contains high byte
        or      r_addr, r11, r0
        // add y index
        add     r_addr, r_addr, r_y
        zext    r_addr, 16
        // check if high byte changed
        shr     r0, r_addr, 8           // get high byte of effective address
        eq      r0, r0, r_rv            // same page?
        bt      r0, mread_indy_nowrap
        ldc     r0, TICKS_PER_US
        add     r_time, r_time, r0      // add a cycle
mread_indy_nowrap:
        // read final value
        bu      mos6502_mread_ret1


/*******************************************************************************
 * Adressing mode write (IND),Y. Fetch one argument byte and add 1 to PC, read
 * an address from this location. add Y to the result.
 * Write the byte to the resulting address.
 *
 * return:  r_rv    result value
 *          r_addr  address used
 * changes: r0, r11, r_rv
 *
 ******************************************************************************/
        .globl mos6502_mwrite_indy
        .globl mos6502_mwrite_indy_ret1
mos6502_mwrite_indy:
        entsp   1
mos6502_mwrite_indy_ret1:
        bl      mos6502_mread_arg8
        mov     r_addr, r_rv
        // read 16 bit address
        bl      mos6502_mread
        mov     r11, r_rv
        add     r_addr, r_addr, 1
        zext    r_addr, 16
        bl      mos6502_mread
        shl     r0, r_rv, 8
        or      r_addr, r11, r0
        // add y index
        add     r_addr, r_addr, r_y
        zext    r_addr, 16
        // write value
        bl      mos6502_mwrite
        bu      leave_asm_function_ret1

