/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */
#include <stdio.h>
#include <xs1.h>

#include <uart.h>
#include <via.h>
#include "mos6502.h"

/*******************************************************************************
 * Public data
 *
 ******************************************************************************/

/*
 * Important:
 * These functions must make sure that the upper bits of offset and value
 * must be 0 or will be 0. Other functions rely on it! We do not use smaller
 * data types to get smaller code, because no zero expansions are needed
 * everywhere.
 */
typedef unsigned (*memr_function_t)(unsigned addr);
typedef void     (*memw_function_t)(unsigned addr, unsigned val);

static unsigned memr_ram(unsigned addr);
static unsigned memr_rom(unsigned addr);
static unsigned memr_none(unsigned addr);
static unsigned memr_via(unsigned addr);

static void memw_ram(unsigned addr, unsigned val);
static void memw_none(unsigned addr, unsigned val);
static void memw_via(unsigned addr, unsigned val);


/*******************************************************************************
 * Private data
 *
 ******************************************************************************/

/*******************************************************************************
 *
 ******************************************************************************/
static const memr_function_t memr_table[128] =
{
    memr_ram,  memr_none, memr_none, memr_via,  // 0x0000
    memr_none, memr_none, memr_none, memr_none, // 0x2000
    memr_none, memr_none, memr_none, memr_none, // 0x4000
    memr_none, memr_none, memr_none, memr_none, // 0x6000
    memr_none, memr_none, memr_none, memr_none, // 0x8000
    memr_none, memr_none, memr_none, memr_none, // 0xa000
    memr_rom,  memr_rom,  memr_rom,  memr_rom,  // 0xc000
    memr_rom,  memr_rom,  memr_rom,  memr_rom,  // 0xe000
};

static const memw_function_t memw_table[128] =
{
    memw_ram,  memw_none, memw_none, memw_via,  // 0x0000
    memw_none, memw_none, memw_none, memw_none, // 0x2000
    memw_none, memw_none, memw_none, memw_none, // 0x4000
    memw_none, memw_none, memw_none, memw_none, // 0x6000
    memw_none, memw_none, memw_none, memw_none, // 0x8000
    memw_none, memw_none, memw_none, memw_none, // 0xa000
    memw_none, memw_none, memw_none, memw_none, // 0xc000
    memw_none, memw_none, memw_none, memw_none, // 0xe000
};

// Client RAM
uint8_t ram[MOS6502_RAM_SIZE];

// Client ROM
uint8_t rom[MOS6502_ROM_SIZE];

// Address which has been read by the last call to mos6502_memr_<addrmode>
// function.
static unsigned prev_addr;

// channel ends for via access
unsigned via1_ce;
unsigned via2_ce;


/*******************************************************************************
 * Set the channel end for via access
 *
 ******************************************************************************/
void mos6502_mem_set_via_chanends(unsigned mos6502_via1_ce,
                                  unsigned mos6502_via2_ce)
{
    via1_ce = mos6502_via1_ce;
    via2_ce = mos6502_via2_ce;
}


/*******************************************************************************
 * Read a byte from memory.
 *
 ******************************************************************************/
#pragma stackfunction 64
unsigned mos6502_memr_raw(unsigned addr)
{
    return memr_table[addr >> 11](addr);
}


/*******************************************************************************
 * Fetch an instruction and increment PC.
 *
 ******************************************************************************/
unsigned mos6502_memr_opcode(void)
{
    unsigned val;

    val = memr_table[mos6502_pc >> 11](mos6502_pc);
    mos6502_pc++;
    mos6502_pc &= 0xffff;

    return val;
}


/*******************************************************************************
 * Fetch an argument byte and increment PC.
 *
 ******************************************************************************/
unsigned mos6502_memr_arg8(void)
{
    unsigned val;

    val = memr_table[mos6502_pc >> 11](mos6502_pc);
    mos6502_pc++;
    mos6502_pc &= 0xffff;

    return val;
}


/*******************************************************************************
 * Fetch two argument bytes and add 2 to PC.
 *
 ******************************************************************************/
unsigned mos6502_memr_arg16(void)
{
    unsigned val;

    val  = memr_table[mos6502_pc >> 11](mos6502_pc);
    mos6502_pc++;
    mos6502_pc &= 0xffff;

    val += memr_table[mos6502_pc >> 11](mos6502_pc) << 8;
    mos6502_pc++;
    mos6502_pc &= 0xffff;

    return val;
}


/*******************************************************************************
 * Adressing mode read ZP. Fetch one argument byte and add 1 to PC.
 * Read the byte from the resulting address.
 *
 ******************************************************************************/
unsigned mos6502_memr_zp(void)
{
    unsigned addr;

    addr = mos6502_memr_arg8();
    prev_addr = addr;
    return memr_table[0](addr);
}


/*******************************************************************************
 * Adressing mode read ZP,X. Fetch one argument byte and add 1 to PC, add X
 * to the result. Read the byte at the resulting address.
 *
 ******************************************************************************/
unsigned mos6502_memr_zpx(void)
{
    unsigned abs, addr;

    abs = mos6502_memr_arg8();
    addr = (abs + mos6502_x) & 0xff;
    prev_addr = addr;

    return memr_table[0](addr);
}


/*******************************************************************************
 * Adressing mode read ZP,Y. Fetch one argument byte and add 1 to PC, add Y
 * to the result. Read the byte at the resulting address.
 *
 ******************************************************************************/
unsigned mos6502_memr_zpy(void)
{
    unsigned abs, addr;

    abs = mos6502_memr_arg8();
    addr = (abs + mos6502_y) & 0xff;
    prev_addr = addr;

    return memr_table[0](addr);
}


/*******************************************************************************
 * Adressing mode read ABS. Fetch two argument bytes and add 2 to PC.
 * Read the byte at the resulting address.
 *
 ******************************************************************************/
unsigned mos6502_memr_abs(void)
{
    unsigned addr;

    addr = mos6502_memr_arg16(); // t += 1
    prev_addr = addr;

    return memr_table[addr >> 11](addr);
}


/*******************************************************************************
 * Adressing mode read ABS,X. Fetch two argument bytes and add 2 to PC, add X 
 * to the result. Read the byte at the resulting address.
 * Add 1 additional clock cycle more if page boundary was crossed.
 *
 ******************************************************************************/
unsigned mos6502_memr_absx(void)
{
    unsigned abs, addr;

    abs = mos6502_memr_arg16(); // t += 1
    addr = (abs + mos6502_x) & 0xffff;
    prev_addr = addr;

    if ((addr >> 8) != (abs >> 8))
        mos6502_time += TICKS_PER_US;

    return memr_table[addr >> 11](addr);
}


/*******************************************************************************
 * Adressing mode read ABS,X. Fetch two argument bytes and add 2 to PC, add X 
 * to the result. Read the byte at the resulting address.
 *
 ******************************************************************************/
unsigned mos6502_memr_absx3(void)
{
    unsigned addr;

    addr = mos6502_memr_arg16(); // t += 1
    addr = (addr + mos6502_x) & 0xffff;
    prev_addr = addr;

    return memr_table[addr >> 11](addr);
}


/*******************************************************************************
 * Adressing mode read ABS,Y. Fetch two argument bytes and add 2 to PC, add Y
 * to the result. Read the byte at the resulting address.
 * Add 1 clock cycle if page boundary was crossed.
 *
 ******************************************************************************/
unsigned mos6502_memr_absy(void)
{
    unsigned abs, addr;

    abs = mos6502_memr_arg16();
    addr = (abs + mos6502_y) & 0xffff;
    prev_addr = addr;

    if ((addr >> 8) != (abs >> 8))
        mos6502_time += TICKS_PER_US;

    return memr_table[addr >> 11](addr);
}


/*******************************************************************************
 * Adressing mode read (IND,X). Fetch one argument byte and add 1 to PC, add
 * X to this value, take the lower 8 bits. Read an address from this location.
 * Read the byte at the resulting address.
 *
 ******************************************************************************/
unsigned mos6502_memr_indx(void)
{
    unsigned addr, ind;

    ind = mos6502_memr_arg8();
    ind = (ind + mos6502_x) & 0xff;

    // simplification: we should wrap here, too
    addr  = memr_table[0](ind++);
    addr |= memr_table[0](ind) << 8;
    prev_addr = addr;

    return memr_table[addr >> 11](addr);
}


/*******************************************************************************
 * Adressing mode read (IND),Y. Fetch one argument byte and add 1 to PC, read
 * an address from this location. add Y to the result.
 * Read the byte at the resulting address.
 * Add 1 clock cycle if page boundary was crossed.
 *
 ******************************************************************************/
unsigned mos6502_memr_indy(void)
{
    unsigned abs, ind, addr;

    abs = mos6502_memr_arg8();

    // simplification: we should wrap here, too
    ind  = memr_table[0](abs++);
    ind |= memr_table[0](abs) << 8;

    addr = (ind + mos6502_y) & 0xffff;
    prev_addr = addr;

    if ((addr >> 8) != (ind >> 8))
        mos6502_time += TICKS_PER_US;

    return memr_table[addr >> 11](addr);
}


/*******************************************************************************
 * Write to memory directly.
 *
 ******************************************************************************/
#pragma stackfunction 80
void mos6502_memw_raw(unsigned addr, unsigned val)
{
    memw_table[addr >> 11](addr, val);
}


/*******************************************************************************
 * Write to value back to memory which has been read last using one of the
 * address mode functions.
 *
 ******************************************************************************/
void mos6502_memw_back(unsigned val)
{
    memw_table[prev_addr >> 11](prev_addr, val);
}


/*******************************************************************************
 * Adressing mode write ZP. Fetch one argument byte and add 1 to PC. 
 * Write the byte to the resulting address.
 *
 ******************************************************************************/
void mos6502_memw_zp(unsigned val)
{
    unsigned addr;

    addr = mos6502_memr_arg8(); // t += 1
    memw_table[0](addr, val);
}


/*******************************************************************************
 * Adressing mode write ZP,X. Fetch one argument bytes and add 1 to PC, add X
 * to the result. Write the byte to the resulting address.
 *
 ******************************************************************************/
void mos6502_memw_zpx(unsigned val)
{
    unsigned abs, ind;

    abs = mos6502_memr_arg8();
    ind = (abs + mos6502_x) & 0xff;

    memw_table[ind >> 11](ind, val);
}


/*******************************************************************************
 * Adressing mode write ZP,Y. Fetch one argument bytes and add 1 to PC, add Y
 * to the result. Write the byte to the resulting address.
 *
 ******************************************************************************/
void mos6502_memw_zpy(unsigned val)
{
    unsigned abs, ind;

    abs = mos6502_memr_arg8();
    ind = (abs + mos6502_y) & 0xff;

    memw_table[ind >> 11](ind, val);
}


/*******************************************************************************
 * Adressing mode write ABS. Fetch two argument bytes and add 2 to PC. 
 * Write the byte to the resulting address.
 *
 ******************************************************************************/
void mos6502_memw_abs(unsigned val)
{
    unsigned abs;

    abs = mos6502_memr_arg16(); // t += 1

    memw_table[abs >> 11](abs, val);
}


/*******************************************************************************
 * Adressing mode write ABS,X. Fetch two argument bytes and add 2 to PC, add X
 * to the result. Write the byte to the resulting address.
 *
 ******************************************************************************/
void mos6502_memw_absx(unsigned val)
{
    unsigned abs, ind;

    abs = mos6502_memr_arg16(); // t += 1
    ind = (abs + mos6502_x) & 0xffff;

    memw_table[ind >> 11](ind, val);
}


/*******************************************************************************
 * Adressing mode write ABS,Y. Fetch two argument bytes and add 2 to PC, add Y
 * to the result. Write the byte to the resulting address.
 *
 ******************************************************************************/
void mos6502_memw_absy(unsigned val)
{
    unsigned abs, ind;

    abs = mos6502_memr_arg16(); // t += 1
    ind = (abs + mos6502_y) & 0xffff;

    memw_table[ind >> 11](ind, val);
}


/*******************************************************************************
 * Adressing mode write (IND,X). Fetch one argument byte and add 1 to PC, add
 * X to this value, take the lower 8 bits. Read an address from this location.
 * Write the byte to the resulting address.
 *
 ******************************************************************************/
void mos6502_memw_indx(unsigned val)
{
    unsigned addr, ind;

    addr = mos6502_memr_arg8();
    addr = (addr + mos6502_x) & 0xff;

    // simplification: we should wrap here, too
    ind  = memr_table[0](addr++);
    ind |= memr_table[0](addr) << 8;

    memw_table[ind >> 11](ind, val);
}


/*******************************************************************************
 * Adressing mode write (IND),Y. Fetch one argument byte and add 1 to PC, read
 * an address from this location. add Y to the result.
 * Write the byte to the resulting address.
 *
 ******************************************************************************/
void mos6502_memw_indy(unsigned val)
{
    unsigned abs, ind, indy;

    abs = mos6502_memr_arg8();

    // simplification: we should wrap here, too
    ind  = memr_table[0](abs++);
    ind |= memr_table[0](abs) << 8;

    indy = (ind + mos6502_y) & 0xffff;

    memw_table[indy >> 11](indy, val);
}



/*******************************************************************************
 * Read from 2kByte RAM. Only the number of bits needed to address 2 kBytes
 * are evaluated here.
 *
 ******************************************************************************/
static unsigned memr_ram(unsigned addr)
{
    return ram[addr & (MOS6502_RAM_SIZE - 1)];
}


/*******************************************************************************
 * Send a read command to the VIA thread. The command contains the address to
 * be read. Wait until a response arrives which contains the result.
 * Before the real response arrives, other events from the VIAs may roll in,
 * these are also executed from here.
 *
 ******************************************************************************/
static unsigned memr_via(unsigned addr)
{
    unsigned val;
    unsigned ce;

    val = addr << VIA_CHANNEL_REG_SHIFT;

    ce = (addr < VIA2_BASE) ? via1_ce : via2_ce;

    asm
    (
            "out res[%0], %1 \n"
            "out res[%0], %2 \n"
            : /* no output */
            : "r"(ce), "r"(val), "r"(mos6502_time)
    );

    for (;;)
    {
        asm
        (
            "in  %0, res[%1] \n"
            : "=r"(val)
            : "r"(ce)
        );
        if (val > 0xff)
            mos6502_exec_ctrl_cmd(val);
        else
            break;
    }

    return val;
}


static unsigned memr_rom(unsigned addr)
{
    return rom[addr & (MOS6502_ROM_SIZE - 1)];
}


static unsigned memr_none(unsigned addr)
{
    return 0xff;
}


static void memw_ram(unsigned addr, unsigned val)
{
    ram[addr & (MOS6502_RAM_SIZE - 1)] = val;
}


static void memw_none(unsigned addr, unsigned val)
{
}


static void memw_via(unsigned addr, unsigned val)
{
    unsigned ce;

    val = (VIA_CHANNEL_CMD_WRITE << VIA_CHANNEL_CMD_SHIFT) |
            (addr << VIA_CHANNEL_REG_SHIFT) |
            ((val & 0xff) << VIA_CHANNEL_VAL_SHIFT);

    ce = (addr < VIA2_BASE) ? via1_ce : via2_ce;

    asm
    (
            "out res[%0], %1 \n"
            "out res[%0], %2 \n"
            : /* no output */
            : "r"(ce), "r"(val), "r"(mos6502_time)
    );
}
