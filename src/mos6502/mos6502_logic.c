/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include "mos6502.h"


/*******************************************************************************
 * Do ASL, write back the value to memory and update flags
 *
 ******************************************************************************/
static void do_asl_write_back(unsigned val)
{
    if (val & 0x80)
        mos6502_flags |= MOS6502_CFLAG;
    else
        mos6502_flags &= ~MOS6502_CFLAG;

    val <<= 1;
    update_nz(val);
    mos6502_memw_back(val);
}


/*******************************************************************************
 * Do LSR, write back the value to memory and update flags
 *
 ******************************************************************************/
static void do_lsr_write_back(unsigned val)
{
    if (val & 1)
        mos6502_flags |= MOS6502_CFLAG;
    else
        mos6502_flags &= ~MOS6502_CFLAG;

    val >>= 1;
    update_nz(val);
    mos6502_memw_back(val);
}


/*******************************************************************************
 * Do ROL, write back the value to memory and update flags
 *
 ******************************************************************************/
static void do_rol_write_back(unsigned val)
{
    val <<= 1;

    if (mos6502_flags & MOS6502_CFLAG)
        val |= 1;

    if (val & 0x100)
        mos6502_flags |= MOS6502_CFLAG;
    else
        mos6502_flags &= ~MOS6502_CFLAG;

    update_nz(val);
    mos6502_memw_back(val);
}


/*******************************************************************************
 * Do ROR, write back the value to memory and update flags
 *
 ******************************************************************************/
static void do_ror_write_back(unsigned val)
{
    if (mos6502_flags & MOS6502_CFLAG)
        val |= 0x100;

    if (val & 1)
        mos6502_flags |= MOS6502_CFLAG;
    else
        mos6502_flags &= ~MOS6502_CFLAG;

    val >>= 1;
    update_nz(val);
    mos6502_memw_back(val);
}


void op_01(void)
{ // ORA ($FF,X)
    mos6502_a |= mos6502_memr_indx();
    update_nz(mos6502_a);
}


void op_05(void)
{ // ORA $FF
    mos6502_a |= mos6502_memr_zp();
    update_nz(mos6502_a);
}


void op_06(void)
{ // ASL $FF
    do_asl_write_back(mos6502_memr_zp());
}


void op_09(void)
{ // ORA #$FF
    mos6502_a |= mos6502_memr_arg8();
    update_nz(mos6502_a);
}


void op_0a(void)
{ // ASL A
    if (mos6502_a & 0x80)
        mos6502_flags |= MOS6502_CFLAG;
    else
        mos6502_flags &= ~MOS6502_CFLAG;

    mos6502_a <<= 1;
    mos6502_a &= 0xff;
    update_nz(mos6502_a);
}


void op_0d(void)
{ // ORA $FFFF
    mos6502_a |= mos6502_memr_abs();
    update_nz(mos6502_a);
}


void op_0e(void)
{ // ASL $FFFF
    do_asl_write_back(mos6502_memr_abs());
}


void op_11(void)
{ // ORA ($FF),Y
    mos6502_a |= mos6502_memr_indy();
    update_nz(mos6502_a);
}


void op_15(void)
{ // ORA $FF,X
    mos6502_a |= mos6502_memr_zpx();
    update_nz(mos6502_a);
}


void op_16(void)
{ // ASL $FF,X
    do_asl_write_back(mos6502_memr_zpx());
}


void op_19(void)
{ // ORA $FFFF,Y
    mos6502_a |= mos6502_memr_absy();
    update_nz(mos6502_a);
}


void op_1d(void)
{ // ORA $FFFF,X
    mos6502_a |= mos6502_memr_absx();
    update_nz(mos6502_a);
}


void op_1e(void)
{ // ASL $FFFF,X
    do_asl_write_back(mos6502_memr_absx3());
}


void op_21(void)
{ // AND ($FF,X)
    mos6502_a &= mos6502_memr_indx();
    update_nz(mos6502_a);
}


void op_25(void)
{ // AND $FF
    mos6502_a &= mos6502_memr_zp();
    update_nz(mos6502_a);
}


void op_26(void)
{ // ROL $FF
    do_rol_write_back(mos6502_memr_zp());
}


void op_29(void)
{ // AND #$FF
    mos6502_a &= mos6502_memr_arg8();
    update_nz(mos6502_a);
}


void op_2a(void)
{ // ROL A
    mos6502_a <<= 1;

    if (mos6502_flags & MOS6502_CFLAG)
        mos6502_a |= 1;

    if (mos6502_a & 0x100)
        mos6502_flags |= MOS6502_CFLAG;
    else
        mos6502_flags &= ~MOS6502_CFLAG;

    mos6502_a &= 0xff;
    update_nz(mos6502_a);
}


void op_2d(void)
{ // AND $FFFF
    mos6502_a &= mos6502_memr_abs();
    update_nz(mos6502_a);
}


void op_2e(void)
{ // ROL $FFFF
    do_rol_write_back(mos6502_memr_abs());
}


void op_31(void)
{ // AND ($FF),Y
    mos6502_a &= mos6502_memr_indy();
    update_nz(mos6502_a);
}


void op_35(void)
{ // AND $FF,X
    mos6502_a &= mos6502_memr_zpx();
    update_nz(mos6502_a);
}


void op_36(void)
{ // ROL $FF,X
    do_rol_write_back(mos6502_memr_zpx());
}


void op_39(void)
{ // AND $FFFF,Y
    mos6502_a &= mos6502_memr_absy();
    update_nz(mos6502_a);
}


void op_3d(void)
{ // AND $FFFF,X
    mos6502_a &= mos6502_memr_absx();
    update_nz(mos6502_a);
}


void op_3e(void)
{ // ROL $FFFF,X
    do_rol_write_back(mos6502_memr_absx3());
}


void op_41(void)
{ // EOR ($FF,X)
    mos6502_a ^= mos6502_memr_indx();
    update_nz(mos6502_a);
}


void op_45(void)
{ // EOR $FF
    mos6502_a ^= mos6502_memr_zp();
    update_nz(mos6502_a);
}


void op_46(void)
{ // LSR $FF
    do_lsr_write_back(mos6502_memr_zp());
}


void op_49(void)
{ // EOR #$FF
    mos6502_a ^= mos6502_memr_arg8();
    update_nz(mos6502_a);
}


void op_4a(void)
{ // LSR A
    if (mos6502_a & 1)
        mos6502_flags |= MOS6502_CFLAG;
    else
        mos6502_flags &= ~MOS6502_CFLAG;

    mos6502_a >>= 1;
    update_nz(mos6502_a);
}


void op_4d(void)
{ // EOR $FFFF
    mos6502_a ^= mos6502_memr_abs();
    update_nz(mos6502_a);
}


void op_4e(void)
{ // LSR $FFFF
    do_lsr_write_back(mos6502_memr_abs());
}


void op_51(void)
{ // EOR ($FF),Y
    mos6502_a ^= mos6502_memr_indy();
    update_nz(mos6502_a);
}


void op_55(void)
{ // EOR $FF,X
    mos6502_a ^= mos6502_memr_zpx();
    update_nz(mos6502_a);
}


void op_56(void)
{ // LSR $FF,X
    do_lsr_write_back(mos6502_memr_zpx());
}


void op_59(void)
{ // EOR $FFFF,Y
    mos6502_a ^= mos6502_memr_absy();
    update_nz(mos6502_a);
}

void op_5d(void)
{ // EOR $FFFF,X
    mos6502_a ^= mos6502_memr_absx();
    update_nz(mos6502_a);
}


void op_5e(void)
{ // LSR $FFFF,X
    do_lsr_write_back(mos6502_memr_absx3());
}


void op_66(void)
{ // ROR $FF
    do_ror_write_back(mos6502_memr_zp());
}


void op_6a(void)
{ // ROR A
    if (mos6502_flags & MOS6502_CFLAG)
        mos6502_a |= 0x100;

    if (mos6502_a & 1)
        mos6502_flags |= MOS6502_CFLAG;
    else
        mos6502_flags &= ~MOS6502_CFLAG;

    mos6502_a >>= 1;
    update_nz(mos6502_a);
}


void op_6e(void)
{ // ROR $FFFF
    do_ror_write_back(mos6502_memr_abs());
}


void op_76(void)
{ // ROR $FF,X
    do_ror_write_back(mos6502_memr_zpx());
}


void op_7e(void)
{ // ROR $FFFF,X
    do_ror_write_back(mos6502_memr_absx3());
}
