/*
 * drive_util.xc - Utility functions for disk drive emulation
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>

#include <string.h>
#include <stdint.h>
#include <uart.h>

#include "drive.h"

/*******************************************************************************
 ******************************************************************************/

/*
 * Raw data of the current sector just as seen by the R/W head. We use double
 * buffering: While one sector is being read or written by the drive, the other
 * sector is written and/or loaded from the disk image.
 * They are called foreground (fg) and background (bg) sector.
 */
uint8_t drive_raw_sector[2][SECTOR_LEN];

/* Conversion BIN => GCR */
static const uint8_t bin_to_gcr[16] =
{
    0x0a, 0x0b, 0x12, 0x13,
    0x0e, 0x0f, 0x16, 0x17,
    0x09, 0x19, 0x1a, 0x1b,
    0x0d, 0x1d, 0x1e, 0x15
};

/* These are the standard speed zones */
static const uint8_t track_to_speedzone[MAX_NUM_TRACKS] =
{
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3,            // 17 * 3 (21 sectors)
    2, 2, 2, 2, 2, 2, 2,            //  7 * 2 (19 sectors)
    1, 1, 1, 1, 1, 1,               //  6 * 1 (18 sectors)
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0    // 10 * 0 (17 sectors)
};

/* Sectors per track depends from speed zone */
static const uint8_t speedzone_to_sectors[4] =
{
    17, 18, 19, 21
};


/*
 * Timer Hz / (bitrate / 8)
 */
static const uint16_t speedzone_to_ticks_per_byte[4] =
{
    XS1_TIMER_HZ / (250000 / 8),
    XS1_TIMER_HZ / (266667 / 8),
    XS1_TIMER_HZ / (285714 / 8),
    XS1_TIMER_HZ / (307692 / 8)
};


/* Index of track data in a d64 image in 256 byte units. */
static uint16_t d64_track_offsets[MAX_NUM_TRACKS] =
{
    0x0000, 0x0015, 0x002a, 0x003f, 0x0054,
    0x0069, 0x007e, 0x0093, 0x00a8, 0x00bd,
    0x00d2, 0x00e7, 0x00fc, 0x0111, 0x0126,
    0x013b, 0x0150, 0x0165, 0x0178, 0x018b,
    0x019e, 0x01b1, 0x01c4, 0x01d7, 0x01ea,
    0x01fc, 0x020e, 0x0220, 0x0232, 0x0244,
    0x0256, 0x0267, 0x0278, 0x0289, 0x029a,
    0x02ab, 0x02bc, 0x02cd, 0x02de, 0x02ef
};



/*******************************************************************************
 * Convert 4 bytes of binary data from src to 5 bytes of GCR data to dst.
 *
 ******************************************************************************/
void drive_gcr_encode(uint8_t dst[], const uint8_t src[])
{
    unsigned o, i;
    // aaaa bbbb => oo ooop pppp
    i  = src[0];
    o  = bin_to_gcr[i >> 4] << 5;
    o |= bin_to_gcr[i & 0xf];
    dst[0] = o >> 2;
    o <<= 10;
    // cccc dddd => ppqq qqqr rrrr
    i  = src[1];
    o |= bin_to_gcr[i >> 4] << 5;
    o |= bin_to_gcr[i & 0xf];
    dst[1] = o >> 4;
    o <<= 10;
    // eeee ffff => rr rrss ssst tttt
    i  = src[2];
    o |= bin_to_gcr[i >> 4] << 5;
    o |= bin_to_gcr[i & 0xf];
    dst[2] = o >> 6;
    o <<= 10;
    // gggg hhhh => sttt ttuu uuuv vvvv
    i  = src[3];
    o |= bin_to_gcr[i >> 4] << 5;
    o |= bin_to_gcr[i & 0xf];
    dst[3] = o >> 8;
    dst[4] = o;
#if 0
    uart_puts("enc ");
    for (i = 0; i < 4; ++i)
        uart_puthex_padded(2, src[i]);
    uart_puts(" > ");
    for (i = 0; i < 5; ++i)
        uart_puthex_padded(2, dst[i]);
    uart_putcrlf();
#endif
}


/*******************************************************************************
 * Calculate and return an EOR checksum of the given data.
 * todo: Inline this?
 *
 ******************************************************************************/
unsigned drive_checksum(const uint8_t data[], unsigned len)
{
    unsigned i, sum;
    sum = 0;
    for (i = 0; i < len; ++i)
    {
        sum ^= data[i];
    }
    return sum;
}


/*******************************************************************************
 * Convert a binary sector of 256 bytes to a GCR encoded sector.
 *
 * n_sector     index of the sector buffer to write to (0|1)
 * src          binary sector data, 256 bytes
 * track        track number, 0-based
 * sector       sector
 *
 ******************************************************************************/
void drive_convert_to_raw_sector(unsigned n_sector,
                                 const uint8_t src[],
                                 unsigned track, unsigned sector)
{
    uint8_t* dst;
    unsigned i;
    uint8_t bin[8];

    dst = drive_raw_sector[n_sector];

    // create header sync
    memset(dst, 0xff, SECTOR_LEN_HEADER_SYNC);
    dst += SECTOR_LEN_HEADER_SYNC;

    // create header
    bin[0] = 0x08; // header
    bin[2] = sector;
    bin[3] = track + 1;
    bin[4] = 0x58; // id 2
    bin[5] = 0x5a; // id 1
    bin[6] = 0x0f; // off byte
    bin[7] = 0x0f; // off byte
    bin[1] = drive_checksum(bin + 2, 4);
    drive_gcr_encode(dst, bin);
    dst += 5;
    drive_gcr_encode(dst, bin + 4);
    dst += 5;

    // create header gap
    memset(dst, 0x55, SECTOR_LEN_HEADER_GAP);
    dst += SECTOR_LEN_HEADER_GAP;

    // create data sync
    memset(dst, 0xff, SECTOR_LEN_DATA_SYNC);
    dst += SECTOR_LEN_DATA_SYNC;

    // first 3 bytes of src data come to this GCR block
    bin[0] = 0x07;    // $00 - data block ID
    bin[1] = src[0];
    bin[2] = src[1];
    bin[3] = src[2];
    drive_gcr_encode(dst, bin);
    dst += 5;

    // 253 bytes left, convert next 63 * 4 = 252 bytes => up to byte 0xff
    for (i = 3; i < 0xff;)
    {
        bin[0] = src[i++];
        bin[1] = src[i++];
        bin[2] = src[i++];
        bin[3] = src[i++];
        drive_gcr_encode(dst, bin);
        dst += 5;
    }

    // last data byte goes to this GCR-block
    bin[0] = src[i];
    bin[1] = drive_checksum(src, 256);
    bin[2] = 0;
    bin[3] = 0;
    drive_gcr_encode(dst, bin);
    dst += 5;

    // create tail gap
    memset(dst, 0xff, SECTOR_LEN_TAIL_GAP);
    dst += SECTOR_LEN_TAIL_GAP;
}


/*******************************************************************************
 * Fill a buffer with random data.
 *
 * n_sector     index of the sector buffer to write to (0|1)
 *
 ******************************************************************************/
void drive_fill_buffer_random(unsigned n_sector)
{
    static unsigned m_z = 8;
    static unsigned m_w = 0xe4; // todo: use better seeds
    unsigned i;
    
    for (i = 0; i < SECTOR_LEN; ++i)
    {
        // taken from stock
        m_z = 36969 * (m_z & 65535) + (m_z >> 16);
        m_w = 18000 * (m_w & 65535) + (m_w >> 16);
        // return (m_z << 16) + m_w;  /* 32-bit result */
        
        // use "&" to get less 1s than 0s - more realistic? :)
        drive_raw_sector[n_sector][i] = m_z & m_w; 
    }
}


/*******************************************************************************
 * Return a byte from the raw sector buffer.
 *
 * This function does not do a range check. It's just there to circumvent
 * XC's parallel usage rules.
 ******************************************************************************/
unsigned drive_get_raw_byte(unsigned buffer, unsigned offset)
{
    return drive_raw_sector[buffer][offset];
}

/*******************************************************************************
 * Get the number of sectors for the given track.
 *
 *
 * track is 0-based, no half tracks here
 *
 ******************************************************************************/
unsigned drive_get_num_sectors(unsigned track)
{
    return speedzone_to_sectors[track_to_speedzone[track]];
}


/*******************************************************************************
 * Get the number of timer ticks per R/W byte for the given track.
 *
 * track is 0-based, no half tracks here
 *
 ******************************************************************************/
unsigned drive_get_ticks_per_byte(unsigned track)
{
    return speedzone_to_ticks_per_byte[track_to_speedzone[track]];
}


/*******************************************************************************
 * Return the offset in bytes a the given track in a d64 image.
 *
 * track is 0-based, no half tracks here
 *
 ******************************************************************************/
unsigned drive_get_d64_track_offset(unsigned track)
{
    return d64_track_offsets[track] << 8;
}

