/*
 * drive_thread.xc - Disk drive emulation
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>
#include <stdint.h>

#include "uart.h"
#include "drive.h"
#include "via.h"
#include "mos6502.h" // todo: remove me

static void drive_init_empty_disk(void);
static void check_drive_ctrl_msg(streaming chanend drive_ui_ce, unsigned msg);
static void check_sector_state(streaming chanend drive_ui_ce);
static void drive_next_byte(streaming chanend drive_ctrl_ce,
                            streaming chanend drive_data_ce);


/*******************************************************************************
 ******************************************************************************/
// State of the sector buffers:
// Set:   fg buffer is currently visible to the R/W head,
// Clear: fg buffer is inactive and must be replaced by a new buffer
#define BUFFER_STATE_FG_ACTIVE      0x01

// Set:   bg buffer is currently being loaded with new data
// Clear: bg buffer is not being loaded
#define BUFFER_STATE_BG_LOADING     0x02

// Set:   bg buffer is waiting to become active
// Clear: bg buffer is not ready
#define BUFFER_STATE_BG_READY       0x04

// Nothing: both buffers are empty
#define BUFFER_STATE_IDLE           0x00

static unsigned buffer_state;

// Current halftrack DRIVE_MIN_HALFTRACK..DRIVE_MAX_HALFTRACK of the R/W head
static unsigned current_halftrack;

// Number of the track the foreground buffer comes from (0-based)
static unsigned fg_track;

// Number of the sector in the foreground buffer (e.g. 0..20)
static unsigned fg_sector;

// Number of the sector being prepared in the background buffer (e.g. 0..20)
static unsigned bg_sector;

// Index of the active sector buffer in drive_raw_sector (0|1)
static uint8_t fg_buffer;

// length of the current raw track
static unsigned current_num_sectors;

// This is the position of the R/W head, shows the next position to read or write.
// it is less than current_track_len
static unsigned current_head_pos;

// time between two bytes captured by the R/W logic in timer ticks
static unsigned byte_interval;

// count the number of 0xff bytes to find sync (should count bits normally...)
static unsigned sync_counter;

// state of the spindle motor
static unsigned motor_on;

// state of the control register (seen at $1c00)
static unsigned drive_ctrl_reg;

static timer tmr;

/*******************************************************************************
 * Drive emulation thread.
 *
 ******************************************************************************/
void drive_thread(streaming chanend drive_ctrl_ce,
                  streaming chanend drive_data_ce,
                  streaming chanend drive_ui_ce)
{
    unsigned t_next_byte;
    unsigned msg;

    drive_init_empty_disk();

    for (;;)
    {
        select
        {
        case drive_ctrl_ce :> msg:
            check_drive_ctrl_msg(drive_ui_ce, msg);
            break;

        case drive_data_ce :> msg:
            break;

        case drive_ui_ce :> msg:
            buffer_state &= ~BUFFER_STATE_BG_LOADING;
            buffer_state |= BUFFER_STATE_BG_READY;
            break;

        case tmr when timerafter(t_next_byte) :> void:
            if (motor_on)
            {
                drive_next_byte(drive_ctrl_ce, drive_data_ce);
                check_sector_state(drive_ui_ce);
            }
            t_next_byte += byte_interval;
            break;
        }
    }
}


/*******************************************************************************
 * Initialize some stuff to start with an empty disk track.
 *
 ******************************************************************************/
static void drive_init_empty_disk(void)
{
    current_halftrack = (18 - 1) * 2; // 0-based
    drive_ctrl_reg = VIA_DRIVE_CTRL_NSYNC_FOUND | VIA_DRIVE_CTRL_WPROT;
    current_num_sectors = drive_get_num_sectors(18 - 1);
    byte_interval = drive_get_ticks_per_byte(18 - 1);
    current_head_pos = 0;
    fg_buffer = 0;
    fg_sector = 0;
    fg_track  = 0;
    buffer_state = BUFFER_STATE_IDLE;
}


/*******************************************************************************
 * Execute a drive control command.
 * The message has the same format as VIA register $1C00.
 *
 * drive_ui_ce is used to ask the UI thread to load another track, if needed.
 *
 ******************************************************************************/
static void check_drive_ctrl_msg(streaming chanend drive_ui_ce, unsigned msg)
{
    static unsigned old_stepper_val;
    unsigned stepper_val, tmp;

    stepper_val = msg & 0x03;
    if (stepper_val != old_stepper_val)
    {
        if (((old_stepper_val + 1) & 3) == stepper_val)
        {
            if (current_halftrack < DRIVE_MAX_HALFTRACK)
                ++current_halftrack;
        }
        else if (((old_stepper_val - 1) & 3) == stepper_val)
        {
            if (current_halftrack > DRIVE_MIN_HALFTRACK)
                --current_halftrack;
        }
        if ((current_halftrack & 1) == 0)
        {
            check_sector_state(drive_ui_ce);
        }

        old_stepper_val = stepper_val;
    }

    tmp = motor_on;
    motor_on = msg & 0x04;
    if (tmp != motor_on)
    {
        uart_putc('M');
        uart_putc(motor_on ? '1' : '0');
        uart_putcrlf();
    }
}


/*******************************************************************************
 * Check if new sector data has to be loaded from the disk image. If yes, ask
 * the UI thread to do this and initialize everything for the new track.
 *
 * drive_ui_ce is used to ask the UI thread to load a sector, if needed.
 *
 ******************************************************************************/
static void check_sector_state(streaming chanend drive_ui_ce)
{
    unsigned track, bg_buffer, msg;

    track = current_halftrack / 2;

    if (track != fg_track)
    {
        current_num_sectors = drive_get_num_sectors(track);
        byte_interval = drive_get_ticks_per_byte(track);
        fg_track = track;
        fg_sector = current_num_sectors - 1;
        buffer_state = BUFFER_STATE_IDLE;
        current_head_pos = 0;
    }

    if (buffer_state == BUFFER_STATE_BG_READY /* and FG inactive */)
    {
        // swap buffers
        buffer_state  = BUFFER_STATE_FG_ACTIVE; /* and BG idle */
        fg_buffer = !fg_buffer;
        fg_sector = bg_sector;
        current_head_pos = 0;
    }

    // nothing happens in the background buffer?
    if (!(buffer_state & (BUFFER_STATE_BG_READY | BUFFER_STATE_BG_LOADING)))
    {
        // sector number to load:
        bg_sector = fg_sector + 1;
        if (bg_sector >= current_num_sectors)
            bg_sector = 0;

        drive_ui_ce <: DRIVE_UI_LOAD_D64_SECTOR_REQ;
        bg_buffer = !fg_buffer;
        msg = (bg_buffer << 16) | (fg_track << 8) | (bg_sector);
        drive_ui_ce <: msg;
        buffer_state |= BUFFER_STATE_BG_LOADING;
    }
}


/*******************************************************************************
 * Read or write the next byte from/to disk and send it to the VIA thread.
 * Count 0xff bits to find a synchronization mark (should count 1 bits...).
 * Do also send the sync state to VIA.
 *
 * Todo: Currently we don't send data bytes as soon as we realized that this is
 *       a sync signal. Otherwise it looks like software would get $ff bytes
 *       which actually still belong to the sync mark. Is this correct?
 *
 ******************************************************************************/
static void drive_next_byte(streaming chanend drive_ctrl_ce,
                            streaming chanend drive_data_ce)
{
    unsigned val;

    if (buffer_state & BUFFER_STATE_FG_ACTIVE)
    {
        val = drive_get_raw_byte(fg_buffer, current_head_pos++);
        if (current_head_pos >= SECTOR_LEN)
        {
            buffer_state &= ~BUFFER_STATE_FG_ACTIVE;
            current_head_pos = 0;
        }

        if (val == 0xff)
        {
            ++sync_counter;
            if (sync_counter >= 2)
                drive_ctrl_reg &= ~VIA_DRIVE_CTRL_NSYNC_FOUND;
            else
                drive_data_ce <: val;
        }
        else
        {
            sync_counter = 0;
            drive_ctrl_reg |= VIA_DRIVE_CTRL_NSYNC_FOUND;
            drive_data_ce <: val;
        }
    }
    else
    {
        // if we don't have a valid sector yet, return nonsense
        drive_data_ce <: 0x0f;
        drive_ctrl_reg |= VIA_DRIVE_CTRL_NSYNC_FOUND;
    }

    // todo: send this only when it has changed
    drive_ctrl_ce <: drive_ctrl_reg;
}
