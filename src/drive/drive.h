/*
 * (c) 2008-2010 Thomas Giesel <skoe@directbox.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef DRIVE_H
#define DRIVE_H

#include <stdint.h>

#define DRIVE_MAX_HALFTRACK 79
#define DRIVE_MIN_HALFTRACK 0

/*
 * Maximal number of tracks actually in use. This is also used a maximal
 * number of tracks in a d64 image.
 */
#define MAX_NUM_TRACKS 40

// number of bytes in a physical (GCR) sector
#define SECTOR_LEN_HEADER_SYNC    5
#define SECTOR_LEN_HEADER_INFO   10
#define SECTOR_LEN_HEADER_GAP     9
#define SECTOR_LEN_DATA_SYNC      5
#define SECTOR_LEN_DATA_BLOCK   325
#define SECTOR_LEN_TAIL_GAP       8 // 4..12

#define SECTOR_LEN_HEADER       (SECTOR_LEN_HEADER_SYNC + SECTOR_LEN_HEADER_INFO + SECTOR_LEN_HEADER_GAP)
#define SECTOR_LEN_DATA         (SECTOR_LEN_DATA_SYNC + SECTOR_LEN_DATA_BLOCK)
#define SECTOR_LEN              (SECTOR_LEN_HEADER + SECTOR_LEN_DATA + SECTOR_LEN_TAIL_GAP)


/*******************************************************************************
 ******************************************************************************/
typedef enum drive_req_type_e
{
    /*
     * Drive sends this to UI to load a track from the current d64 file
     * Keep this message short (<= 4 bytes) to make sure the transmitting
     * thread doesn't block.
     */
    DRIVE_UI_LOAD_D64_SECTOR_REQ
} drive_req_type_t;


/*******************************************************************************
 ******************************************************************************/
#ifdef __XC__
void drive_thread(streaming chanend drive_ctrl_ce,
                  streaming chanend drive_data_ce,
                  streaming chanend drive_ui_ce);
#endif

void drive_gcr_encode(uint8_t dst[], const uint8_t src[]);
unsigned drive_checksum(const uint8_t data[], unsigned len);
void drive_convert_to_raw_sector(unsigned n_sector,
                                 const uint8_t src[],
                                 unsigned track, unsigned sector);
void drive_fill_buffer_random(unsigned n_sector);
unsigned drive_get_raw_byte(unsigned buffer, unsigned offset);
unsigned drive_get_num_sectors(unsigned track);
unsigned drive_get_ticks_per_byte(unsigned track);
unsigned drive_get_d64_track_offset(unsigned track);

/*******************************************************************************
 ******************************************************************************/
extern uint8_t drive_raw_sector[2][SECTOR_LEN];

#endif
