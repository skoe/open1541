/*
 * (c) 2008 Thomas Giesel <skoe@directbox.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>

void msleep(unsigned ms);
void usleep(unsigned us);
void nanosleep(unsigned ns);
void util_reboot(void);

int util_hdigit2i(int ch);

void util_strip_string(char str[]);

#ifndef __XC__
const char* util_parse_hex(const char* params, unsigned* v);
#endif

#define out_uint(res,val) asm( "out res[%0], %1 \n" :: "r"(res), "r"(val) )

#define TIME_SAME_OR_AFTER(a,b) ((int32_t)(a) - (int32_t)(b) >= 0)
#define TIME_BEFORE(a,b)        ((int32_t)(a) - (int32_t)(b) < 0)

#endif
