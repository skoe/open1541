/*
 * util_xmos.c - utility functions
 *
 * (c) 2008-2010 Thomas Giesel <skoe@directbox.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <xs1.h>
#include "util.h"


/*******************************************************************************
 * Pause for at least the given number of milliseconds.
 *
 ******************************************************************************/
void msleep(unsigned ms)
{
    unsigned i;
    for (i = 0; i < ms; ++i)
        usleep(1000);
}


/*******************************************************************************
 * Pause for at least the given number of microseconds.
 *
 ******************************************************************************/
void usleep(unsigned us)
{
    nanosleep(1000 * us);
}


/*******************************************************************************
 * Pause for at least the given number of nanoseconds.
 *
 ******************************************************************************/
void nanosleep(unsigned ns)
{
    timer    t;
    unsigned time;

    if (ns > 100)
    {
        t :> time;
        time += ns / (1000000000 / XS1_TIMER_HZ);
        t when timerafter(time) :> void;
    }
}


/*******************************************************************************
 * Reboot the system.
 *
 ******************************************************************************/
void util_reboot(void)
{
    unsigned x;

    read_sswitch_reg(get_core_id(), 6, x);
    write_sswitch_reg(get_core_id(), 6, x);
}
