/*
 * via1_jmp.c
 *
 *  Created on: 07.06.2010
 *      Author: tgiesel
 */

#include "via.h"


/*******************************************************************************
 * Local data
 *
 ******************************************************************************/


/* jump tables for VIA write/read */

static viaw_function_t via1_memw_table[VIA_REG_SIZE] =
{
    via1w_prb,  via1w_pra,  via1w_ddrb, via1w_ddra,
    via1w_t1ll, via1w_t1ch, via1w_t1ll, via1w_t1lh,
    via1w_8,    via1w_9,    via1w_a,    via1w_acr,
    via1w_pcr,  via1w_ifr,  via1w_ier,  via1w_f,
};

static viar_function_t via1_memr_table[VIA_REG_SIZE] =
{
    via1r_prb,  via1r_pra,  via1r_ddrb, via1r_ddra,
    via1r_t1cl, via1r_t1ch, via1r_t1ll, via1r_t1lh,
    via1r_8,    via1r_9,    via1r_a,    via1r_acr,
    via1r_pcr,  via1r_ifr,  via1r_ier,  via1r_f,
};



/*******************************************************************************
 * Read a byte from a VIA register.
 *
 ******************************************************************************/
#pragma stackfunction 64
unsigned via1_memr(unsigned addr)
{
    return via1_memr_table[addr & VIA_REG_MASK]();
}


/*******************************************************************************
 * Write a byte to a VIA register.
 *
 ******************************************************************************/
#pragma stackfunction 64
void via1_memw(unsigned addr, unsigned val)
{
    via1_memw_table[addr & VIA_REG_MASK](val);
}
