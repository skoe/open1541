/*
 * VIA (MOS 6522) emulation
 *
 * (c) 2008, 2009 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#ifndef VIA_H
#define VIA_H

#include <stdint.h>

/* VIA base addresses */
#define VIA1_BASE       0x1800
#define VIA2_BASE       0x1c00

/* VIA registers */
#define VIA_PRB         0x00    /* Port B */
#define VIA_PRA         0x01    /* Port A */
#define VIA_DDRB        0x02    /* Data direction register for port B */
#define VIA_DDRA        0x03    /* Data direction register for port A */

#define VIA_T1CL        0x04    /* Timer 1 count low */
#define VIA_T1CH        0x05    /* Timer 1 count high */
#define VIA_T1LL        0x06    /* Timer 1 latch low */
#define VIA_T1LH        0x07    /* Timer 1 latch high */
#define VIA_T2CL        0x08    /* Timer 2 count low - read only */
#define VIA_T2LL        0x08    /* Timer 2 latch low - write only */
#define VIA_T2CH        0x09    /* Timer 2 latch/count high */

#define VIA_SR          0x0a    /* Serial port shift register */
#define VIA_ACR         0x0b    /* Auxiliary control register */
#define VIA_PCR         0x0c    /* Peripheral control register */

#define VIA_IFR         0x0d    /* Interrupt flag register */
#define VIA_IER         0x0e    /* Interrupt enable register */
#define VIA_PRA_NHS     0x0f    /* Port A with no handshake */

#define VIA_REG_SIZE    0x10    /* Number of VIA registers */
#define VIA_REG_MASK    0x0f    /* Mask for VIA register */

/* Interrupt Masks  */
#define VIA_IM_IRQ      0x80    /* Control Bit */
#define VIA_IM_T1       0x40    /* Timer 1 underflow */
#define VIA_IM_T2       0x20    /* Timer 2 underflow */
#define VIA_IM_CB1      0x10    /* Handshake */
#define VIA_IM_CB2      0x08    /* Handshake */
#define VIA_IM_SR       0x04    /* Shift Register completion */
#define VIA_IM_CA1      0x02    /* Handshake */
#define VIA_IM_CA2      0x01    /* Handshake */

/* These values are sent through the MOS6502<->VIA channel */
#define VIA_CHANNEL_CMD_SHIFT    24   // => 0xCC000000
#define VIA_CHANNEL_CMD_READ     0x00 // must be 0!
#define VIA_CHANNEL_CMD_WRITE    0x01
#define VIA_CHANNEL_CMD_RESET    0x02

#define VIA_CHANNEL_VAL_SHIFT    0
#define VIA_CHANNEL_VAL_MASK     0x000000ff
#define VIA_CHANNEL_REG_SHIFT    8
#define VIA_CHANNEL_REG_MASK     0x00001c0f

/* Bit positions in IEC input register (same on XMOS and VIA) */
#define IEC_DATA_IN_MASK        0x01
#define IEC_CLK_IN_MASK         0x04
#define IEC_RST_IN_MASK         0x10
#define IEC_ATN_IN_MASK         0x80
#define IEC_ALL_IN_MASK         (IEC_ATN_IN_MASK | IEC_CLK_IN_MASK | IEC_DATA_IN_MASK)

/* Bit positions in IEC output register (same in XMOS and VIA) */
#define IEC_DATA_OUT_MASK       0x02
#define IEC_CLK_OUT_MASK        0x08
#define IEC_OUT_LED_BIT     1 // XMOS only

/* VIA has ATN-ACK output, we do this in software */
#define IEC_ATN_ACK_OUT_MASK    0x10
/* shift left IEC_ATN_ACK_OUT_MASK to match IEC_ATN_IN_MASK */
#define IEC_ATN_ACK_TO_ACK_SHL  3

#define IEC_ALL_OUT_MASK (IEC_ATN_ACK_OUT_MASK | IEC_CLK_OUT_MASK | IEC_DATA_OUT_MASK)

/* Bits in drive control register $1c00 (VIA2 PRB) */
#define VIA_DRIVE_CTRL_NSYNC_FOUND      0x80
#define VIA_DRIVE_CTRL_BITRATE_MASK     0x60
#define VIA_DRIVE_CTRL_WPROT            0x10
#define VIA_DRIVE_CTRL_LED              0x08
#define VIA_DRIVE_CTRL_MOTOR            0x04
#define VIA_DRIVE_CTRL_STEPPER_MASK     0x03

/* Job codes, used internally */
#define VIA_JOB_NONE        0
#define VIA_JOB_IEC_READ    1
#define VIA_JOB_IEC_WRITE   2

#ifdef __XC__
void via1_thread(unsigned start_time,
                 streaming chanend mos6502_via1_ce);
void via1_init(void);
void via1_set_chanends(streaming chanend iec_ce);

void via2_thread(unsigned start_time,
                streaming chanend mos6502_via2_ce,
                streaming chanend drive_ctrl_ce,
                streaming chanend drive_data_ce);
void via2_init(void);
void via2_set_chanends(streaming chanend drive_ctrl_ce,
                       streaming chanend drive_data_ce);

#endif

#ifndef __XC__
typedef unsigned (*viar_function_t)(void);
typedef void     (*viaw_function_t)(unsigned val);
#endif

void via_set_led_state(unsigned state);
unsigned via_get_led_state(void);

void via_report_access_r(unsigned addr, unsigned val);
void via_report_access_w(unsigned addr, unsigned val);
void via_report_read(const char reg[], unsigned val);
void via_report_write(const char reg[], unsigned val);

unsigned via1_memr(unsigned addr);
unsigned via2_memr(unsigned addr);
void via1_memw(unsigned addr, unsigned val);
void via2_memw(unsigned addr, unsigned val);

void via1w_prb(unsigned val);
void via1w_pra(unsigned val);
void via1w_ddrb(unsigned val);
void via1w_ddra(unsigned val);
void via1w_t1ch(unsigned val);
void via1w_t1ll(unsigned val);
void via1w_t1lh(unsigned val);
void via1w_8(unsigned val);
void via1w_9(unsigned val);
void via1w_a(unsigned val);
void via1w_acr(unsigned val);
void via1w_pcr(unsigned val);
void via1w_ifr(unsigned val);
void via1w_ier(unsigned val);
void via1w_f(unsigned val);

unsigned via1r_prb(void);
unsigned via1r_pra(void);
unsigned via1r_ddrb(void);
unsigned via1r_ddra(void);
unsigned via1r_t1cl(void);
unsigned via1r_t1ch(void);
unsigned via1r_t1ll(void);
unsigned via1r_t1lh(void);
unsigned via1r_8(void);
unsigned via1r_9(void);
unsigned via1r_a(void);
unsigned via1r_acr(void);
unsigned via1r_pcr(void);
unsigned via1r_ifr(void);
unsigned via1r_ier(void);
unsigned via1r_f(void);

void via2w_prb(unsigned val);
void via2w_pra(unsigned val);
void via2w_2(unsigned val);
void via2w_3(unsigned val);
void via2w_t1ch(unsigned val);
void via2w_t1ll(unsigned val);
void via2w_t1lh(unsigned val);
void via2w_8(unsigned val);
void via2w_9(unsigned val);
void via2w_a(unsigned val);
void via2w_acr(unsigned val);
void via2w_pcr(unsigned val);
void via2w_ifr(unsigned val);
void via2w_ier(unsigned val);
void via2w_f(unsigned val);

unsigned via2r_prb(void);
unsigned via2r_pra(void);
unsigned via2r_2(void);
unsigned via2r_3(void);
unsigned via2r_t1cl(void);
unsigned via2r_t1ch(void);
unsigned via2r_t1ll(void);
unsigned via2r_t1lh(void);
unsigned via2r_8(void);
unsigned via2r_9(void);
unsigned via2r_a(void);
unsigned via2r_acr(void);
unsigned via2r_pcr(void);
unsigned via2r_ifr(void);
unsigned via2r_ier(void);
unsigned via2r_f(void);


/*******************************************************************************
 * Global data.
 *
 ******************************************************************************/

extern uint8_t via1_reg_mirror[VIA_REG_SIZE];
extern uint8_t via1_t1_active;
extern uint8_t via1_t2_active;
extern uint16_t via1_t1;
extern uint16_t via1_t2;

extern unsigned via_access_time;

extern uint8_t via2_reg_mirror[VIA_REG_SIZE];
extern uint8_t via2_t1_active;
extern uint8_t via2_t2_active;
extern uint16_t via2_t1;
extern uint16_t via2_t2;

extern unsigned drive_data_in;
extern unsigned drive_ctrl_in;
extern unsigned via_iec_job_val;
extern unsigned via_led_state;

#endif /* VIA_H */
