/*
 * (c) 2008 Thomas Giesel <skoe@directbox.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <xs1.h>
#include <config.h>
#include <uart.h>
#include "via.h"

/* VIA timer counter registers */
uint16_t via2_t1;
uint16_t via2_t2;

/*
 * These are != 0 if there is a one-shot timer running or if the timer
 * is free-running.
 */
uint8_t via2_t1_active;
uint8_t via2_t2_active;

uint8_t via2_reg_mirror[VIA_REG_SIZE];

// copy of ctrl channel end
unsigned drive_ctrl_ce;

// copy of data channel end
unsigned drive_data_ce;

/*******************************************************************************
 *
 ******************************************************************************/
void via2_init(void)
{
    int i;

    for (i = 0; i < 16; ++i)
        via2_reg_mirror[i] = 0;

    // remember: In reg_mirror[VIA_IFR] we keep bit 7 = 0
    // note: via1_t1, via1_t2 are not cleared

    // This I/O is high impedance => seen as high by 7406 => LED on
    via_set_led_state(IEC_OUT_LED_BIT);
}


/*******************************************************************************
 * Set copies of our channel ends, so this module can use them directly.
 * 
 ******************************************************************************/
void via2_set_chanends(streaming chanend drive_ctrl,
                       streaming chanend drive_data)
{
    asm (
        "stw    %0, dp[drive_ctrl_ce]    \n"
        "stw    %1, dp[drive_data_ce]    \n"
        : /* no output */
        : "r"(drive_ctrl), "r"(drive_data)
    );
}


/*******************************************************************************
 * $1C00 - PRB - VIA2 Data Port B (SYNC, Motors, Bit Rates, LED)
 *
 * Bit  7      0 = SYNC found
 * Bits 6-5    Bit rates (*):
 *               00 = 250000 Bit/s    01 = 266667 Bit/s
 *               10 = 285714 Bit/s    11 = 307692 Bit/s
 * Bit  4      Write Protect Sense: 1 = On
 * Bit  3      Drive LED: 1 = On
 * Bit  2      Drive Motor: 1 = On
 * Bit  1-0    Step motor for head movement:
 *               Sequence 00/01/10/11/00... moves inwards
 *               Sequence 00/11/10/01/00... moves outwards
 *
 ******************************************************************************/
void via2w_prb(unsigned val)
{
    //via_report_write("2:PRB", val);
    via2_reg_mirror[VIA_PRB] = val;

    // send value to drive thread
    asm
    (
        "out     res[%0], %1            \n"
        : /* no output */
        : "r"(drive_ctrl_ce), "r"(val)
    );

    // LED state <= DDRB high and PRB high
    via_set_led_state(((via2_reg_mirror[VIA_DDRB] >> 3) & (val >> 3)) ?
                        IEC_OUT_LED_BIT : 0);
}

unsigned via2r_prb(void)
{
    unsigned val;

    // todo: use DRB
    val = via2_reg_mirror[VIA_PRB];
    val &= ~(VIA_DRIVE_CTRL_NSYNC_FOUND | VIA_DRIVE_CTRL_WPROT);
    val |= drive_ctrl_in;

    //via_report_read("2:PRB", val);
    return val;
}

/******************************************************************************
 * $1C01 - PRA - VIA2 Data Port A (Data to/from head)
 *
 ******************************************************************************/
void via2w_pra(unsigned val)
{
    via_report_write("2:PRA", val);
    via2_reg_mirror[1] = val;
}

unsigned via2r_pra(void)
{
    unsigned val;

    // todo: use DRA
    //val = via2_reg_mirror[1];
    val = drive_data_in;
    //if (val != 0x55) via_report_read("2:PRA", val);
    return val;
}

/******************************************************************************
 * $1C02 - DDRB - VIA2 Data Direction Register B (SYNC, Motors, Bit Rates, LED)
 *
 * 1541 II writes 0x6f here, everything OUT except SYNC and WRITE PROTECT.
 * At one place 0x08 is written, but with no different functionality intended.
 * This is hard-coded in our project and cannot be changed.
 *
 * Bit  x     1 = Pin PBx set to Output, 0 = Input
 *
 ******************************************************************************/
void via2w_2(unsigned val)
{
    // currently we only support values 0x6f and 0x08 to be written here
    if ((val != 0x6f) && (val != 0x08))
    {
        // report all other values
        via_report_access_w(VIA2_BASE + 2, val);
    }

    // LED state <= DDRB high and PRB high
    via_set_led_state(((val >> 3) & (via2_reg_mirror[VIA_PRB] >> 3)) ?
                       IEC_OUT_LED_BIT : 0);

    via2_reg_mirror[2] = val;
}

unsigned via2r_2(void)
{
    /* return last value written */
    return via2_reg_mirror[2];
}

/******************************************************************************
 * $1C03 - DDRA - VIA2 Data Direction Register A  (Data to/from head)
 *
 * Write currently ignored, the 1541 II writes 0xff here.
 *
 * Bit  x     1 = Pin PBx set to Output, 0 = Input
 *
 ******************************************************************************/
void via2w_3(unsigned val)
{
    if (val != 0xff)
    {
        // report all other values
        via_report_access_w(VIA2_BASE + 3, val);
    }

    via2_reg_mirror[3] = val;
}

unsigned via2r_3(void)
{
    /* return last value written */
    return via2_reg_mirror[3];
}



/*******************************************************************************
 * $1C04 - T1CL - Timer 1 Low-Byte (IRQ Timer)
 *
 ******************************************************************************/

/* (Write goes to VIA_T1LL, same as register 6) */

unsigned via2r_t1cl(void)
{
    unsigned val;

    // Clear IFR:T1
    via2_reg_mirror[VIA_IFR] &= ~VIA_IM_T1;

    // Return low byte of counter 1
    val = via2_t1 & 0xff;
    //via_report_read("2:T1CL", val);
    return val;
}


/*******************************************************************************
 * $1C05 - T1CH - Timer 1 High-Byte (IRQ Timer)
 *
 ******************************************************************************/

void via2w_t1ch(unsigned val)
{
    // write to high latch
    via2_reg_mirror[VIA_T1LH] = val;

    // transfer high:low latch into counter
    via2_t1 = (val << 8) + via2_reg_mirror[VIA_T1LL];
    via2_t1_active = 1;

    // Clear IFR:T1
    via2_reg_mirror[VIA_IFR] &= ~VIA_IM_T1;
    //via_report_write("2:T1CH", val);
}

unsigned via2r_t1ch(void)
{
    // Return high byte of counter 1
    //via_report_read("2:T1CH", via2_t1 >> 8);
    return via2_t1 >> 8;
}


/*******************************************************************************
 * $1C06 - T1LL - Timer 1-Latch Low-Byte (IRQ Timer)
 *
 ******************************************************************************/
void via2w_t1ll(unsigned val)
{
    //via_report_write("2:T1LL", val);
    via2_reg_mirror[VIA_T1LL] = val;
}

unsigned via2r_t1ll(void)
{
    //via_report_read("2:T1LL", via2_reg_mirror[VIA_T1LL]);
    return via2_reg_mirror[VIA_T1LL];
}


/*******************************************************************************
 * $1C07 - T1LH - Timer 1-Latch High-Byte (IRQ Timer)
 *
 ******************************************************************************/
void via2w_t1lh(unsigned val)
{
    // write to high latch
    via2_reg_mirror[VIA_T1LH] = val;

    // Clear IFR:T1
    via2_reg_mirror[VIA_IFR] &= ~VIA_IM_T1;
    //via_report_write("2:T1LH", val);
}

unsigned via2r_t1lh(void)
{
	unsigned val;
	val = via2_reg_mirror[VIA_T1LH];
    //via_report_read("2:T1LH", val);
    return val;
}


/*******************************************************************************
 * VIA_T2CL/VIA_T2LL - Timer 2 Low-Byte (unused)
 *
 ******************************************************************************/
void via2w_8(unsigned val)
{
    via_report_access_w(VIA2_BASE + 8, val);
    via2_reg_mirror[8] = val;
}

unsigned via2r_8(void)
{
    unsigned val;
    val = via2_reg_mirror[8];
    via_report_access_r(VIA2_BASE + 8, val);
    return val;
}

/*******************************************************************************
 * VIA_T2CH - Timer 2 High-Byte (unused)
 *
 ******************************************************************************/
void via2w_9(unsigned val)
{
    via_report_access_w(VIA2_BASE + 9, val);
    via2_reg_mirror[9] = val;
}

unsigned via2r_9(void)
{
    unsigned val;
    val = via2_reg_mirror[9];
    via_report_access_r(VIA2_BASE + 9, val);
    return val;
}


/*******************************************************************************
 * VIA_SR - Shift Register (unused)
 *
 ******************************************************************************/
void via2w_a(unsigned val)
{
    via_report_access_w(VIA2_BASE + 10, val);
    via2_reg_mirror[10] = val;
}

unsigned via2r_a(void)
{
    unsigned val;
    val = via2_reg_mirror[10];
    via_report_access_r(VIA2_BASE + 10, val);
    return val;
}


/*******************************************************************************
 * $1C0B - ACR - Auxiliary Control Register
 *
 * 1541-II DOS writes #$41 here (Continuous Interrupts, latching PA)
 *
 * Bits 7-6   Timer 1 Control:
 *              00 = Timed Interrupt when Timer 1 is loaded, no PB7
 *              01 = Continuous Interrupts, no PB7
 *              10 = Timed Interrupt when Timer 1 is loaded,
 *                   one-shot on PB7
 *              11 = Continuous Interrupts, square-wave on PB7
 * Bit  5     Timer 2 Control: 0 = Timed Interrupt
 *                             1 = Count Pulses on PB6
 * Bits 4-2   Shift Register Control:
 *              000 = Disabled
 *              001 = Shift in under control of Timer 2
 *              010 = Shift in under control of Phi2
 *              011 = Shift in under control of ext. Clock
 *              100 = Shift out free-running at Timer 2 rate
 *              101 = Shift out under control of Timer 2
 *              110 = Shift out under control of Phi2
 *              111 = Shift out under control of ext. Clock
 * Bit  1     1 = enable latching PB
 * Bit  0     1 = enable latching PA
 *
 ******************************************************************************/
void via2w_acr(unsigned val)
{
    via_report_write("2:ACR", val);
    via2_reg_mirror[VIA_ACR] = val;
}

unsigned via2r_acr(void)
{
    unsigned val;
    val = via2_reg_mirror[VIA_ACR];
    via_report_read("2:ACR", val);
    return val;
}


/*******************************************************************************
 * $1C0C - VIA_PCR - Peripheral Control Register
 *
 * Bits 7-5   CB2 Control:
 *              000 = Input negative active edge
 *              001 = Independent interrupt input negative edge
 *              010 = Input positive active edge
 *              011 = Independent interrupt input positive edge
 *              100 = Handshake output
 *              101 = Pulse output
 *              110 = Low output
 *              111 = High output
 * Bit  4     CB1 Interrupt Control: 0 = Negative active edge
 *                                   1 = Positive active edge
 * Bit  3-1   CA2 Control: see Bits 7-5
 * Bit  0     CA1 Interrupt Control: see Bit 4
 *
 * CA1 (Input):  BYTE-READY
 * CA2 (Output): SOE (High = activate BYTE-READY)
 * CB2 (Output): Head Mode (Low = Write, High = Read)
 *
 ******************************************************************************/
void via2w_pcr(unsigned val)
{
    //via_report_write("2:PCR", val);
    via2_reg_mirror[VIA_PCR] = val;
}

unsigned via2r_pcr(void)
{
    unsigned val;
    val = via2_reg_mirror[VIA_PCR];
    //via_report_read("2:PCR", val);
    return val;
}


/*******************************************************************************
 * $1C0D - IFR - Interrupt Flag Register
 *
 * Bit 7   1 = Interrupt occured
 * Bit 6   Timer 1
 * Bit 5   Timer 2
 * Bit 4   CB1
 * Bit 3   CB2
 * Bit 2   Shift Register
 * Bit 1   CA1
 * Bit 0   CA2
 *
 ******************************************************************************/
void via2w_ifr(unsigned val)
{
    via_report_write("2:IFR", val);

    // remember: In reg_mirror[VIA_IFR] we keep bit 7 = 0
    via2_reg_mirror[VIA_IFR] &= ~val;
}

unsigned via2r_ifr(void)
{
    unsigned val;
    val = via2_reg_mirror[VIA_IFR];
    if (val)
    	val |= 0x80;
    //via_report_read("2:IFR", val);
    return val;
}


/*******************************************************************************
 * $1C0E - VIA_IER - Interrupt Enable Register
 *
 * Ref. [1] page 13
 *
 * Bit 7   On Read:  Always 1 (todo: really?)
 *         On Write: 1 = Set Int.-Flags, 0 = Clear Int-.Flags
 * Bit 6   Timer 1
 * Bit 5   Timer 2
 * Bit 4   CB1
 * Bit 3   CB2
 * Bit 2   Shift Register ($1C0A)
 * Bit 1   CA1
 * Bit 0   CA2
 *
 * This code always writes a 1 in bit 7, so this register can be read from
 * the mirror. (todo: really?)
 *
 ******************************************************************************/
void via2w_ier(unsigned val)
{
    unsigned r;

    r = via2_reg_mirror[VIA_IER];
    // check bit 7 to find out if we have to set or clear bits
    if (val & 0x80)
    {
        // set
        r |= val;
    }
    else
    {
        // clear
        r &= ~val;
    }
    via2_reg_mirror[VIA_IER] = r;

    via_report_write("2:IER", val);
}

unsigned via2r_ier(void)
{
    unsigned val;
    val = via2_reg_mirror[VIA_IER];
    via_report_read("2:IER", val);
    return val;
}


/*******************************************************************************
 * VIA_PRA_NHS - Data Port A* (no handshake) (unused)
 *
 ******************************************************************************/
void via2w_f(unsigned val)
{
    via_report_access_w(VIA2_BASE + 15, val);
    via2_reg_mirror[15] = val;
}

unsigned via2r_f(void)
{
    unsigned val;
    val = via2_reg_mirror[15];
    via_report_access_r(VIA2_BASE + 15, val);
    return val;
}


