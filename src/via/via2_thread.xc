/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>
#include <config.h>
#include <uart.h>
#include <util.h>
#include <mos6502.h>

#include "via.h"

#undef VIA_TIMING_TEST
#ifdef VIA_TIMING_TEST
static out port test_port2 = PORT_TEST2;
#endif


/*******************************************************************************
 * Global data
 *
 ******************************************************************************/

/* Copy of the channel end to send events to the CPU */
unsigned g_mos6502_via2_ce;

/* Previous state received from drive controller ($1c00, VIA2 PRB) */
unsigned drive_ctrl_in;

/* Previous byte received from drive controller ($1c01, VIA2 PRA) */
unsigned drive_data_in;

/*******************************************************************************
 * Local data
 *
 ******************************************************************************/

static timer via_main_tmr;

/*******************************************************************************
 * The VIA2 emulation thread. 
 *
 ******************************************************************************/
void via2_thread(unsigned t_next_cycle,
                streaming chanend mos6502_via2_ce,
                streaming chanend drive_ctrl_ce,
                streaming chanend drive_data_ce)
{
    unsigned msg, cmd;
    unsigned addr, val;
    unsigned new_irq_state, prev_irq_state;
    unsigned new_atn_in, prev_atn_in;

    via2_init();
    via2_set_chanends(drive_ctrl_ce, drive_data_ce);

    asm (
            "stw    %0, dp[g_mos6502_via2_ce]    \n"
            : /* no output */
            : "r"(mos6502_via2_ce)
    );

    prev_irq_state = 0;

    for (;;)
    {
        select
        {
        case mos6502_via2_ce :> msg:
            mos6502_via2_ce :> via_access_time;
            cmd  = msg >> VIA_CHANNEL_CMD_SHIFT;
            addr = (msg >> VIA_CHANNEL_REG_SHIFT) & VIA_CHANNEL_REG_MASK;
            if (cmd == VIA_CHANNEL_CMD_READ)
            {
                val = via2_memr(addr);
                mos6502_via2_ce <: val;
            }
            else if (cmd & VIA_CHANNEL_CMD_WRITE)
            {
                val = (msg >> VIA_CHANNEL_VAL_SHIFT) & VIA_CHANNEL_VAL_MASK;
                via2_memw(addr, val);
            }
            else /* VIA_CHANNEL_CMD_RESET */
            {
                via2_init();
            }
            break;

        case drive_ctrl_ce :> drive_ctrl_in:
            break;

        case drive_data_ce :> drive_data_in:
            // check SOE state (Set Overflow Output Enable)
            // todo: Just to check bit 1 may be not enough...
            if (via2_reg_mirror[VIA_PCR] & 0x02)
            {
                mos6502_via2_ce <: MOS6502_CTRL_SETV;
            }
            break;

        case via_main_tmr when timerafter(t_next_cycle) :> void:
            if (mos6502_running)
            {
                if (--via2_t1 == 0)
                {
                    // free running or one-shot and active?
                    if (via2_t1_active)
                    {
                        via2_reg_mirror[VIA_IFR] |= VIA_IM_T1;
                        if (via2_reg_mirror[VIA_ACR] & (1 << 6))
                        {
                            // free running mode, reload, stay active
                            via2_t1 = (via2_reg_mirror[VIA_T1LH] << 8) +
                                      via2_reg_mirror[VIA_T1LL];
                        }
                        else
                        {
                            // one shot mode
                            via2_t1_active = 0;
                        }
                        //uart_puts("1:T1IRQ\r\n");
                    }
                }
                --via2_t2;
            }

            new_irq_state = via2_reg_mirror[VIA_IFR] & via2_reg_mirror[VIA_IER];

            if (new_irq_state != prev_irq_state)
            {
                if (new_irq_state)
                    mos6502_via2_ce <: MOS6502_CTRL_ASSERT_IRQ_VIA2;
                else
                    mos6502_via2_ce <: MOS6502_CTRL_RELEASE_IRQ_VIA2;
                prev_irq_state = new_irq_state;
            }

            // 1 cycle per us => 1 MHz
            t_next_cycle += TICKS_PER_US;
            break;
        }
    }
}
