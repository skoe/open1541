/*
 * (c) 2008 Thomas Giesel <skoe@directbox.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * References:
 * [1] mos_6522.pdf: MCS6522 Versatile Interface Adapter; MOS Technology Inc. 1977
 */

#include <stdint.h>

#include <mos6502.h>
#include <uart.h>
#include <util.h>
#include "via.h"

//#include "via_macros.S"
//#include "via1_irq.S"

/* VIA timer counter registers */
uint16_t via1_t1;
uint16_t via1_t2;

/*
 * These are != 0 if there is a one-shot timer running or if the timer
 * is free-running.
 */
uint8_t via1_t1_active;
uint8_t via1_t2_active;

uint8_t via1_reg_mirror[VIA_REG_SIZE];

// copy of IEC channel end
unsigned iec_ce;


/*******************************************************************************
 *
 ******************************************************************************/
void via1_init(void)
{
    int i;

    uart_puts("via1_init\r\n");

    for (i = 0; i < 16; ++i)
        via1_reg_mirror[i] = 0;
    // remember: In reg_mirror[VIA_IFR] we keep bit 7 = 0
    // note: via1_t1, via1_t2 are not cleared
}


/*******************************************************************************
 * Set copies of our channel ends, so this module can use them directly.
 *
 ******************************************************************************/
void via1_set_chanends(streaming chanend iec_rw)
{
    asm (
        "stw    %0, dp[iec_ce]    \n"
        : /* no output */
        : "r"(iec_rw)
    );
}


/*******************************************************************************
 * $1800 - PRB - Data Port B (ATN, CLOCK, DATA, Device #)
 *
 * Bit  7     ATN IN
 * Bits 6-5   Device address preset switches:
 *              00 = #8, 01 = #9, 10 = #10, 11 = #11
 * Bit  4     ATN acknowledge OUT
 * Bit  3     CLOCK OUT
 * Bit  2     CLOCK IN
 * Bit  1     DATA OUT
 * Bit  0     DATA IN
 *
 ******************************************************************************/
void via1w_prb(unsigned val)
{
    //via_report_write("1:PRB", val);

    via1_reg_mirror[VIA_PRB] = val;

    val &= IEC_ALL_OUT_MASK;
    // all of these lines which are switched to input are high on the pins
    val |= IEC_ALL_OUT_MASK & ~via1_reg_mirror[VIA_DDRB];

    out_uint(iec_ce, VIA_JOB_IEC_WRITE | (val << 8));
    out_uint(iec_ce, via_access_time);
}


unsigned via1r_prb(void)
{
    unsigned val;
    //via_report_read("1:PRB", val);

    out_uint(iec_ce, VIA_JOB_IEC_READ | (val << 8));
    out_uint(iec_ce, via_access_time);

    // this means: read result will be sent later
    return ~0;
}


/*******************************************************************************
 * $1801 - PRA - Data Port A (free for parallel cables)
 *
 ******************************************************************************/
void via1w_pra(unsigned val)
{
    //via_report_write("1:PRA", val);
    // value ignored, no parallel cable supported
    // Clear IFR:CA1
    via1_reg_mirror[VIA_IFR] &= ~VIA_IM_CA1;
}


unsigned via1r_pra(void)
{
    // Clear IFR:CA1
    via1_reg_mirror[VIA_IFR] &= ~VIA_IM_CA1;
    // value doesn't matter, no parallel cable supported
    //via_report_read("1:PRA", 0xff);
    return 0xff;
}


/*******************************************************************************
 * $1802 - DDRB - Data Direction Register B (ATN, CLOCK, DATA, Device #)
 *
 * Bit  x    1 = Pin PBx set to Output, 0 = Input
 *
 ******************************************************************************/
void via1w_ddrb(unsigned ddr)
{
    unsigned prb;

    //if (val != 0x1a)
    //    via_report_write("1:DDRB", ddr);

    via1_reg_mirror[VIA_DDRB] = ddr;
    prb = via1_reg_mirror[VIA_PRB];

    prb &= IEC_ALL_OUT_MASK;
    // all of these lines which are switched to input are high on the pins
    prb |= IEC_ALL_OUT_MASK & ~ddr;

    out_uint(iec_ce, VIA_JOB_IEC_WRITE | (prb << 8));
    out_uint(iec_ce, via_access_time);
}

unsigned via1r_ddrb(void)
{
    unsigned val;
    val = via1_reg_mirror[VIA_DDRB];
    // via_report_read("1:DDRB", val);
    return val;
}


/*******************************************************************************
 * $1803 - DDRA - Data Direction Register A (free for parallel cables)
 *
 * Currently ignored, the 1541 II writes 0xff here.
 *
 * Bit  x    1 = Pin PBx set to Output, 0 = Input
 *
 ******************************************************************************/
void via1w_ddra(unsigned val)
{
    // we only support value 0xff to be written here
    if (val != 0xff)
        via_report_write("1:DDRA", val);

    via1_reg_mirror[3] = val;
}

unsigned via1r_ddra(void)
{
    unsigned val;
    val = via1_reg_mirror[3];
    // via_report_read("1:DDRA", val);
    return val;
}


/*******************************************************************************
 * $1804 - T1CL - Timer 1 Low-Byte (Timeout Errors)
 *
 ******************************************************************************/

/* (Write goes to VIA_T1LL, same as register 6) */

unsigned via1r_t1cl(void)
{
    unsigned val;

    // Clear IFR:T1
    via1_reg_mirror[VIA_IFR] &= ~VIA_IM_T1;

    // Return low byte of counter 1
    val = via1_t1 & 0xff;
    //via_report_read("1:T1CL", val);
    return val;
}


/*******************************************************************************
 * $1805 - T1CH - Timer 1 High-Byte (Timeout Errors)
 *
 ******************************************************************************/

void via1w_t1ch(unsigned val)
{
    // write to high latch
    via1_reg_mirror[VIA_T1LH] = val;

    // transfer high:low latch into counter
    via1_t1 = (val << 8) + via1_reg_mirror[VIA_T1LL];
    via1_t1_active = 1;

    // Clear IFR:T1
    via1_reg_mirror[VIA_IFR] &= ~VIA_IM_T1;
    //via_report_write("1:T1CH", val);
}

unsigned via1r_t1ch(void)
{
    // Return high byte of counter 1
    //via_report_read("1:T1CH", via1_t1 >> 8);
    return via1_t1 >> 8;
}


/*******************************************************************************
 * $1806 - T1LL - Timer 1-Latch Low-Byte (Timeout Errors)
 *
 ******************************************************************************/
void via1w_t1ll(unsigned val)
{
    //via_report_write("1:T1LL", val);
    via1_reg_mirror[VIA_T1LL] = val;
}

unsigned via1r_t1ll(void)
{
    //via_report_read("1:T1LL", via2_reg_mirror[VIA_T1LL]);
    return via1_reg_mirror[VIA_T1LL];
}


/*******************************************************************************
 * $1807 - T1LH - Timer 1-Latch High-Byte (Timeout Errors)
 *
 ******************************************************************************/
void via1w_t1lh(unsigned val)
{
    // write to high latch
    via1_reg_mirror[VIA_T1LH] = val;

    // Clear IFR:T1
    via1_reg_mirror[VIA_IFR] &= ~VIA_IM_T1;
    //via_report_write("1:T1LH", val);
}

unsigned via1r_t1lh(void)
{
	unsigned val;
	val = via1_reg_mirror[VIA_T1LH];
    //via_report_read("1:T1LH", val);
    return val;
}


/*******************************************************************************
 * VIA_T2CL/VIA_T2LL - Timer 2 Low-Byte (unused)
 *
 ******************************************************************************/
void via1w_8(unsigned val)
{
    via_report_access_w(VIA1_BASE + 8, val);
    via1_reg_mirror[8] = val;
}

unsigned via1r_8(void)
{
    unsigned val;
    val = via1_reg_mirror[8];
    via_report_access_r(VIA1_BASE + 8, val);
    return val;
}


/*******************************************************************************
 * VIA_T2CH - Timer 2 High-Byte (unused)
 *
 ******************************************************************************/
void via1w_9(unsigned val)
{
    via_report_access_w(VIA1_BASE + 9, val);
    via1_reg_mirror[9] = val;
}

unsigned via1r_9(void)
{
    unsigned val;
    val = via1_reg_mirror[9];
    via_report_access_r(VIA1_BASE + 9, val);
    return val;
}


/*******************************************************************************
 * VIA_SR - Shift Register (unused)
 *
 ******************************************************************************/
void via1w_a(unsigned val)
{
    via_report_access_w(VIA1_BASE + 10, val);
    via1_reg_mirror[10] = val;
}

unsigned via1r_a(void)
{
    unsigned val;
    val = via1_reg_mirror[10];
    via_report_access_r(VIA1_BASE + 10, val);
    return val;
}


/*******************************************************************************
 * $180B - ACR - Auxiliary Control Register
 *
 * 1541-II DOS writes #$41 here (Continuous Interrupts, latching PA)
 *
 * Bits 7-6   Timer 1 Control:
 *              00 = Timed Interrupt when Timer 1 is loaded, no PB7
 *              01 = Continuous Interrupts, no PB7
 *              10 = Timed Interrupt when Timer 1 is loaded,
 *                   one-shot on PB7
 *              11 = Continuous Interrupts, square-wave on PB7
 * Bit  5     Timer 2 Control: 0 = Timed Interrupt
 *                             1 = Count Pulses on PB6
 * Bits 4-2   Shift Register Control:
 *              000 = Disabled
 *              001 = Shift in under control of Timer 2
 *              010 = Shift in under control of Phi2
 *              011 = Shift in under control of ext. Clock
 *              100 = Shift out free-running at Timer 2 rate
 *              101 = Shift out under control of Timer 2
 *              110 = Shift out under control of Phi2
 *              111 = Shift out under control of ext. Clock
 * Bit  1     1 = enable latching PB
 * Bit  0     1 = enable latching PA
 *
 ******************************************************************************/
void via1w_acr(unsigned val)
{
    via_report_write("1:ACR", val);
    via1_reg_mirror[VIA_ACR] = val;
}

unsigned via1r_acr(void)
{
    unsigned val;
    val = via1_reg_mirror[VIA_ACR];
    via_report_read("1:ACR", val);
    return val;
}


/*******************************************************************************
 * $180C - VIA_PCR - Peripheral Control Register
 *
 * Bits 7-5   CB2 Control:
 *              000 = Input negative active edge
 *              001 = Independent interrupt input negative edge
 *              010 = Input positive active edge
 *              011 = Independent interrupt input positive edge
 *              100 = Handshake output
 *              101 = Pulse output
 *              110 = Low output
 *              111 = High output
 * Bit  4     CB1 Interrupt Control: 0 = Negative active edge
 *                                   1 = Positive active edge
 * Bit  3-1   CA2 Control: see Bits 7-5
 * Bit  0     CA1 Interrupt Control: see Bit 4
 *
 * CA1 (Input): ATN IN (interrupt if ATN occurs)
 *
 ******************************************************************************/
void via1w_pcr(unsigned val)
{
    // we only support value 0x01 to be written here
	// this means IRQ on positive Edge of CA1 => negated ATN goes here
    if (val != 0x01)
      via_report_access_w(VIA1_BASE + VIA_PCR, val);

    via1_reg_mirror[VIA_PCR] = val;
}

unsigned via1r_pcr(void)
{
    unsigned val;
    val = via1_reg_mirror[VIA_PCR];
    via_report_access_r(VIA1_BASE + VIA_PCR, val);
    return val;
}


/*******************************************************************************
 * $180D - VIA_IFR - Interrupt Flag Register
 *
 * Bit 7   1 = Interrupt occured
 * Bit 6   Timer 1
 * Bit 5   Timer 2
 * Bit 4   CB1
 * Bit 3   CB2
 * Bit 2   Shift Register
 * Bit 1   CA1
 * Bit 0   CA2
 *
 ******************************************************************************/
void via1w_ifr(unsigned val)
{
    via_report_write("1:IFR", val);

    // remember: In reg_mirror[VIA_IFR] we keep bit 7 = 0
    via1_reg_mirror[VIA_IFR] &= ~(val & 0x7f);
}

unsigned via1r_ifr(void)
{
    unsigned val;
    val = via1_reg_mirror[VIA_IFR];
    if (val)
    	val |= 0x80;
    //via_report_read("1:IFR", val);
    return val;
}


/*******************************************************************************
 * $180E - VIA_IER - Interrupt Enable Register
 *
 * Ref. [1] page 13
 *
 * Bit 7   On Read:  Always 1 (todo: really?)
 *         On Write: 1 = Set Int.-Flags, 0 = Clear Int-.Flags
 * Bit 6   Timer 1
 * Bit 5   Timer 2
 * Bit 4   CB1
 * Bit 3   CB2
 * Bit 2   Shift Register ($1C0A)
 * Bit 1   CA1
 * Bit 0   CA2
 *
 * This code always writes a 1 in bit 7, so this register can be read from
 * the mirror. (todo: really?)
 *
 ******************************************************************************/
void via1w_ier(unsigned val)
{
    unsigned r;

    r = via1_reg_mirror[VIA_IER];
    // check bit 7 to find out if we have to set or clear bits
    if (val & 0x80)
    {
        // set
        r |= val;
    }
    else
    {
        // clear
        r &= ~val;
    }
    via1_reg_mirror[VIA_IER] = r;

    via_report_write("1:IER", val);
}

unsigned via1r_ier(void)
{
    unsigned val;
    val = via1_reg_mirror[VIA_IER];
    via_report_read("1:IER", val);
    return val;
}


/*******************************************************************************
 * VIA_PRA_NHS - Data Port A* (no handshake) (unused)
 *
 ******************************************************************************/
void via1w_f(unsigned val)
{
    via_report_access_w(VIA1_BASE + VIA_IFR, val);
    via1_reg_mirror[VIA_IFR] = val;
}

unsigned via1r_f(void)
{
    unsigned val;
    val = via1_reg_mirror[VIA_IFR];
    via_report_access_r(VIA1_BASE + VIA_IFR, val);
    return val;
}


