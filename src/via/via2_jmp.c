/*
 * via2_jmp.c
 *
 *  Created on: 07.06.2010
 *      Author: tgiesel
 */

#include "via.h"


/*******************************************************************************
 * Local data
 *
 ******************************************************************************/


/* jump tables for VIA write/read */

static viaw_function_t via2_memw_table[VIA_REG_SIZE] =
{
    via2w_prb,  via2w_pra,  via2w_2,    via2w_3,
    via2w_t1ll, via2w_t1ch, via2w_t1ll, via2w_t1lh,
    via2w_8,    via2w_9,    via2w_a,    via2w_acr,
    via2w_pcr,  via2w_ifr,  via2w_ier,  via2w_f,
};

static viar_function_t via2_memr_table[VIA_REG_SIZE] =
{
    via2r_prb,  via2r_pra,  via2r_2,    via2r_3,
    via2r_t1cl, via2r_t1ch, via2r_t1ll, via2r_t1lh,
    via2r_8,    via2r_9,    via2r_a,    via2r_acr,
    via2r_pcr,  via2r_ifr,  via2r_ier,  via2r_f,
};


/*******************************************************************************
 * Read a byte from a VIA register.
 *
 ******************************************************************************/
#pragma stackfunction 64
unsigned via2_memr(unsigned addr)
{
    return via2_memr_table[addr & VIA_REG_MASK]();
}


/*******************************************************************************
 * Write a byte to a VIA register.
 *
 ******************************************************************************/
#pragma stackfunction 64
void via2_memw(unsigned addr, unsigned val)
{
    via2_memw_table[addr & VIA_REG_MASK](val);
}
