/*
 * mos6502 - a 6502 emulator
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>
#include <config.h>
#include <uart.h>
#include <util.h>
#include <mos6502.h>

#include "via.h"

static void via1_main_thread(unsigned t_next_cycle,
                             streaming chanend access_ce,
                             streaming chanend atn_ce,
                             streaming chanend iec_ce);
static void via1_iec_thread(unsigned t_next_cycle,
                            streaming chanend atn_ce,
                            streaming chanend iec_ce);

#undef VIA_TIMING_TEST
#ifdef VIA_TIMING_TEST
static out port test_port2 = PORT_TEST2;
#define SET_TEST_PORT(v) test_port <: (v)
#else
#define SET_TEST_PORT(v) (void)(v)
#endif


/*******************************************************************************
 * Global data
 *
 ******************************************************************************/

/* Copy of the channel end to send events to the CPU */
unsigned g_mos6502_via1_ce;

/*******************************************************************************
 * Local data
 *
 ******************************************************************************/

static timer via_main_tmr;

static in port port_iec_in   = PORT_IEC_IN;
static out port port_iec_out = PORT_IEC_OUT;

/*******************************************************************************
 * The VIA emulation thread. This will start one thread for VIA emulation which
 * implements the register access, timers etc. and a second thread which is
 * responsible for IEC I/O.
 *
 ******************************************************************************/
void via1_thread(unsigned start_time,
                streaming chanend mos6502_via1_ce)
{
    streaming chan iec_chan;
    streaming chan atn_chan;
    via1_init();

    asm (
            "stw    %0, dp[g_mos6502_via1_ce]    \n"
            : /* no output */
            : "r"(mos6502_via1_ce)
    );

    // outputs are inverted => all high
    port_iec_out <: 0;

    par
    {
        via1_main_thread(start_time, mos6502_via1_ce,
                         atn_chan, iec_chan);
        via1_iec_thread(start_time, atn_chan, iec_chan);
    }
}


/*******************************************************************************
 * Thread for VIA emulation. Register access, timers etc.
 *
 ******************************************************************************/
static void via1_main_thread(unsigned t_next_cycle,
                            streaming chanend mos6502_via1_ce,
                            streaming chanend atn_ce,
                            streaming chanend iec_ce)
{
    unsigned msg, cmd;
    unsigned addr, val, ddr;
    unsigned new_irq_state, prev_irq_state;

    via1_set_chanends(iec_ce);
    prev_irq_state = 0;

    for (;;)
    {
        select
        {
        case iec_ce :> val:
            // response to read IEC command - contains state of ATN/CLK/DATA, rest 0
            // todo: can we precalculate the and/or values?
            // todo: Device number
            // replace all bits which are output with the last value written (This is port B!)
            ddr = via1_reg_mirror[VIA_DDRB];
            val &= ~ddr;
            val |= ddr & via1_reg_mirror[VIA_PRB];
            // The three output lines are high if they are set to input
            val |= ~ddr & IEC_ALL_OUT_MASK;
            mos6502_via1_ce <: val;
            break;

        case atn_ce :> val:
            // check for ATN edge IRQ
            // CA1 IRQ enabled and ATN changed?
            // todo: 2nd part of term can be done with XOR
            if (via1_reg_mirror[VIA_IER] & VIA_IM_CA1)
            {
                if (via1_reg_mirror[VIA_PCR] & 1)
                {
                    // check rising edge of CA1 = falling edge of ATN
                    if (val)
                        via1_reg_mirror[VIA_IFR] |= VIA_IM_CA1;
                }
                else
                {
                    // check falling edge of CA1 = rising edge of ATN
                    if (!val)
                        via1_reg_mirror[VIA_IFR] |= VIA_IM_CA1;
                }
            }
            break;

        case mos6502_via1_ce :> msg:
            mos6502_via1_ce :> via_access_time;
            cmd  = msg >> VIA_CHANNEL_CMD_SHIFT;
            addr = (msg >> VIA_CHANNEL_REG_SHIFT) & VIA_CHANNEL_REG_MASK;
            if (cmd == VIA_CHANNEL_CMD_READ)
            {
                val = via1_memr(addr);

                /* ~0 means that the result will be sent later */
                if (val != ~0)
                    mos6502_via1_ce <: val;
            }
            else if (cmd & VIA_CHANNEL_CMD_WRITE)
            {
                val = (msg >> VIA_CHANNEL_VAL_SHIFT) & VIA_CHANNEL_VAL_MASK;
                via1_memw(addr, val);
            }
            else /* VIA_CHANNEL_CMD_RESET */
            {
                via1_init();
            }
            break;

        case via_main_tmr when timerafter(t_next_cycle) :> void:

            if (mos6502_running)
            {
                if (--via1_t1 == 0)
                {
                    // free running or one-shot and active?
                    if (via1_t1_active)
                    {
                        via1_reg_mirror[VIA_IFR] |= VIA_IM_T1;
                        if (via1_reg_mirror[VIA_ACR] & (1 << 6))
                        {
                            // free running mode, reload, stay active
                            via1_t1 = (via1_reg_mirror[VIA_T1LH] << 8) +
                                      via1_reg_mirror[VIA_T1LL];
                        }
                        else
                        {
                            // one shot mode
                            via1_t1_active = 0;
                        }
                        //uart_puts("1:T1IRQ\r\n");
                    }
                }
                --via1_t2;
            }

            new_irq_state = via1_reg_mirror[VIA_IFR] & via1_reg_mirror[VIA_IER];

            if (new_irq_state != prev_irq_state)
            {
                if (new_irq_state)
                    mos6502_via1_ce <: MOS6502_CTRL_ASSERT_IRQ_VIA1;
                else
                    mos6502_via1_ce <: MOS6502_CTRL_RELEASE_IRQ_VIA1;
                prev_irq_state = new_irq_state;
            }
            // 1 cycle per us => 1 MHz
            t_next_cycle += TICKS_PER_US;
            break;
        }
    }
}


/*******************************************************************************
 * Thread for IEC I/O.
 *
 * Values to be put on IEC bus must be sent to iec_ce after the write cycle has
 * started but more than 250 ns before the end of the cycle.
 * To get this time window, the sender should wait for the start of the write
 * cycle and then send the value immediately.
 * 
 ******************************************************************************/
static void via1_iec_thread(unsigned t_next,
                            streaming chanend atn_ce,
                            streaming chanend iec_ce)
{
    timer tmr;
    unsigned val;
    unsigned current_iec_in;
    unsigned t;
    unsigned job;
    unsigned job_time;
    unsigned job_val;
    unsigned prev_atn;

    /* IEC value to be sent ~ 100 ns after cycle start */
    unsigned next_iec_out;

    current_iec_in = 0;
    job = VIA_JOB_NONE;

    for (;;)
    {
        // get job as late as possible - it may be write
        select
        {
        case iec_ce :> job:
            iec_ce :> job_time;
            job_val = job >> 8;
            job     = job & 0xff;
            tmr :> t;
            if (TIME_SAME_OR_AFTER(t, job_time))
            {
                uart_putc('$');
            }
            break;

        default:
            break;
        }

        if (job == VIA_JOB_IEC_WRITE)
        {
            // job time is at end of write cycle - will it be the next cycle?
            if (TIME_SAME_OR_AFTER(t_next, job_time))
            {
                SET_TEST_PORT(1);
                next_iec_out = job_val;
                job = VIA_JOB_NONE;
                SET_TEST_PORT(0);
                if (t_next != job_time)
                {
                    SET_TEST_PORT(1);
                    uart_putc('W');
                }
            }
        }

        /*** in ***/
        // we sample the IEC bus ~ 200 ns in advance, refer to docs/timing
        tmr when timerafter(t_next - TICKS_PER_US / 5) :> void;
        port_iec_in :> current_iec_in;
        current_iec_in &= IEC_ALL_IN_MASK; // todo: RST in?

        // send read result as early as possible not to block the CPU emulation
        if (job == VIA_JOB_IEC_READ)
        {
            // job time is at end of read cycle - did this cycle happen?
            if (TIME_SAME_OR_AFTER(t_next, job_time))
            {
                SET_TEST_PORT(1);
                iec_ce <: current_iec_in;
                job = VIA_JOB_NONE;
                SET_TEST_PORT(0);
                if (t_next != job_time)
                {
                    SET_TEST_PORT(1);
                    uart_putc('R');
                }
            }
        }

        /*** out ***/
        // wait for next clock edge
        tmr when timerafter(t_next) :> void;
        // output current register value and ATN ACK
        // check ATN-ACK: when ATN_ACK == ATN (signal inverted)
        // todo: simplify code
        val = next_iec_out | via_led_state;
        if (((val & IEC_ATN_ACK_OUT_MASK) << IEC_ATN_ACK_TO_ACK_SHL) !=
                (current_iec_in & IEC_ATN_IN_MASK))
        {
            // then pull down DATA (inverted)
            val |= IEC_DATA_OUT_MASK;
        }
        port_iec_out <: val;

        val = current_iec_in & IEC_ATN_IN_MASK;
        if (val != prev_atn)
        {
            // Send messages for ATN edges only
            atn_ce <: val;
            prev_atn = val;
        }

        t_next += TICKS_PER_US;
    }
}

