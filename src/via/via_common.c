/*
 * via_common.c
 *
 *  Created on: 27.05.2010
 *      Author: tgiesel
 */

#include <uart.h>
#include <xs1.h>

#include "mos6502.h"
#include "via.h"

#define VIA_LOG

/* from via_thread.xc */
extern unsigned mos6502_via_ce;

/* The beginning time of the emulated VIA read/write access cylce */
unsigned via_access_time;

/*
 * Value for the VIA job:
 *
 * VIA_JOB_IEC_READ - One or more of the bits IEC_ATN_IN_MASK,
 * IEC_CLK_IN_MASK, IEC_DATA_IN_MASK still may be set, the other bits are
 * prepared already.
 *
 * VIA_JOB_IEC_READ - Value to be written to the IEC bus at the end of
 * the cycle via_access_time. Only IEC_DATA_OUT_MASK, IEC_CLK_OUT_MASK,
 * IEC_ATN_ACK_OUT_MASK may be set.
 */
unsigned via_iec_job_val;

/*
 * LED state, 0 or IEC_OUT_LED_BIT. On XMOS we put the LED on the same port
 * as IEC out. That's why it's send to the port by via1_iec_thread.
 */
unsigned via_led_state;


/*******************************************************************************
 * 0 or IEC_OUT_LED_BIT
 ******************************************************************************/
void via_set_led_state(unsigned state)
{
    //via_led_state = state;
    asm (
        "stw    %0, dp[via_led_state]    \n"
        : /* no output */
        : "r"(state)
    );
}

unsigned via_get_led_state(void)
{
    return via_led_state;
}

static void via_cmd_to_cpu(unsigned cmd)
{
    asm
    (
        "out     res[%0], %1            \n"
        : /* no output */
        : "r"(mos6502_via_ce), "r"(cmd)
    );
}


void via_report_access_r(unsigned addr, unsigned val)
{
#ifdef VIA_LOG
//    via_cmd_to_cpu(MOS6502_CTRL_DISABLE);
    uart_putc('r');
    uart_puthex_padded(4, addr);
    uart_putc('>');
    uart_puthex_padded(2, val);
    uart_putcrlf();
//    via_delay(1000 * XS1_TIMER_MHZ);
//    via_cmd_to_cpu(MOS6502_CTRL_ENABLE);
#endif
}


void via_report_access_w(unsigned addr, unsigned val)
{
#ifdef VIA_LOG
//    via_cmd_to_cpu(MOS6502_CTRL_DISABLE);
    uart_putc('w');
    uart_puthex_padded(4, addr);
    uart_putc('<');
    uart_puthex_padded(2, val);
    uart_putcrlf();
//    via_delay(1000 * XS1_TIMER_MHZ);
//    via_cmd_to_cpu(MOS6502_CTRL_ENABLE);
#endif
}

void via_report_read(const char* reg, unsigned val)
{
#ifdef VIA_LOG
//    via_cmd_to_cpu(MOS6502_CTRL_DISABLE);
    uart_puts(reg);
    uart_putc('>');
    uart_puthex_padded(2, val);
    uart_putcrlf();
//    via_delay(1000 * XS1_TIMER_MHZ);
//    via_cmd_to_cpu(MOS6502_CTRL_ENABLE);
#endif
}


void via_report_write(const char* reg, unsigned val)
{
#ifdef VIA_LOG
//    via_cmd_to_cpu(MOS6502_CTRL_DISABLE);
    uart_puts(reg);
    uart_putc('<');
    uart_puthex_padded(2, val);
    uart_putcrlf();
//    via_delay(1000 * XS1_TIMER_MHZ);
//    via_cmd_to_cpu(MOS6502_CTRL_ENABLE);
#endif
}
