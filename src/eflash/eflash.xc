/*
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>
#include <xclib.h>
#include <config.h>

#include "eflash.h"

static void eflash_chip_select(void);
static void eflash_chip_deselect(void);
static void eflash_send_byte(unsigned byte);
static void eflash_send_addr(unsigned flash_addr);

static out port spi_ss   = PORT_SPI_SS_FLASH;
static out port spi_clk  = PORT_SPI_CLK;
static out port spi_mosi = PORT_SPI_MOSI;
static in  port spi_miso = PORT_SPI_MISO;

static unsigned seq_write_start_addr = ~0;

/*******************************************************************************
 * Set the chip select for the flash chip.
 *
 ******************************************************************************/
static void eflash_chip_select(void)
{
    spi_clk <: 1;
    spi_ss  <: 0;
}


/*******************************************************************************
 * Release the chip select for the flash chip.
 *
 ******************************************************************************/
static void eflash_chip_deselect(void)
{
    spi_ss  <: 1;
}


/*******************************************************************************
 * Send a byte to the SPI bus. Only the lower 8 bits are sent.
 *
 ******************************************************************************/
static void eflash_send_byte(unsigned val)
{
    unsigned i;

    // ...76543210 => ...01234567
    val = byterev(bitrev(val));

    for (i = 0; i < 8; ++i)
    {
        spi_clk <: 0;
        spi_mosi <: >> val;
        spi_clk <: 1;
    }
}


/*******************************************************************************
 * Select the flash, wait as long as the flash is busy, delesect it.
 * Return the content of the status register after the flash got ready.
 *
 ******************************************************************************/
unsigned eflash_wait_for_result(void)
{
    unsigned val;
    
    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_READ_STATUS);
    do
    {
        val = eflash_read_byte();
    }
    while (val & EFLASH_STATUS_BUSY_MASK);
    eflash_chip_deselect();

    return val;
}


/*******************************************************************************
 * Send a three bytes address to the flash.
 *
 ******************************************************************************/
static void eflash_send_addr(unsigned flash_addr)
{
    eflash_send_byte(flash_addr >> 16);
    eflash_send_byte(flash_addr >> 8);
    eflash_send_byte(flash_addr);
}


/*******************************************************************************
 * Bring the flash to a sane state.
 *
 ******************************************************************************/
void eflash_reset(void)
{
    eflash_chip_deselect();

    // who knows...
    eflash_seq_write_end();
}


/*******************************************************************************
 * Read a byte from the SPI bus. Also used for flash read access after
 * eflash_read_start().
 *
 ******************************************************************************/
unsigned eflash_read_byte(void)
{
    unsigned i, val;

    val = 0;
    for (i = 0; i < 8; ++i)
    {
        spi_clk <: 0;
        spi_clk <: 1;
        spi_miso :> >> val;
    }

    // 01234567... => ...76543210
    return bitrev(val);
}


/*******************************************************************************
 * Select the flash, read its ID and deselect it. The value returned has the
 * format 0xMMDDddll, where MM = manufacturer, DD = device byte 1, dd = device
 * byte 2, ll = length of extended information.
 *
 ******************************************************************************/
unsigned eflash_get_id(void)
{
    unsigned val;

    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_READ_ID);
    val  = eflash_read_byte() << 24;
    val |= eflash_read_byte() << 16;
    val |= eflash_read_byte() << 8;
    val |= eflash_read_byte();
    eflash_chip_deselect();

    return val;
}


/*******************************************************************************
 * Start a read array operation at flash_addr.
 * After this has been called, the functions eflash_read_byte and
 * eflash_read_bytes can be used to read data. Use eflash_read_end() to end
 * the read mode.
 * 
 ******************************************************************************/
void eflash_read_start(unsigned flash_addr)
{
    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_READ_ARRAY);
    eflash_send_addr(flash_addr);
    eflash_send_byte(0);
}


/*******************************************************************************
 * Read some byte from flash.
 * The caller must have called eflash_read_start() before.
 * 
 ******************************************************************************/
void eflash_read_bytes(uint8_t mem[], unsigned n_bytes)
{
    unsigned i;
    for (i = 0; i < n_bytes; ++i)
        mem[i] = eflash_read_byte();
}


/*******************************************************************************
 * End a read array operation.
 * 
 ******************************************************************************/
void eflash_read_end(void)
{
    eflash_chip_deselect();
}


/*******************************************************************************
 * Send the write enable command to the flash to set the WEL.
 *
 ******************************************************************************/
void eflash_write_enable(void)
{
    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_WRITE_ENABLE);
    eflash_chip_deselect();
}


/*******************************************************************************
 * Send the write disable command to the flash to clear the WEL.
 * 
 ******************************************************************************/
void eflash_write_disable(void)
{
    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_WRITE_DISABLE);
    eflash_chip_deselect();
}


/*******************************************************************************
 *
 ******************************************************************************/
void eflash_sector_unlock(unsigned addr)
{
    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_SECTOR_UNLOCK);
    eflash_send_addr(addr);
    eflash_chip_deselect();
}


/*******************************************************************************
 * Check if the sector which contains the given address is protected.
 * Return 0 if it is not protected, a different value otherwise.
 ******************************************************************************/
unsigned eflash_read_sector_prot(unsigned addr)
{
    unsigned val;

    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_READ_SECTOR_PROT);
    eflash_send_addr(addr);
    val = eflash_read_byte();
    eflash_chip_deselect();
    return val;
}


/*******************************************************************************
 * Erase a 64k sector
 ******************************************************************************/
unsigned eflash_block_erase(unsigned addr)
{
    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_BLOCK64_ERASE);
    eflash_send_addr(addr);
    eflash_chip_deselect();
    return eflash_wait_for_result();
}


/*******************************************************************************
 * Select the flash, erase chip contents, wait for result, deselect it.
 * Return the content of the status register after the flash got ready.
 * The caller must have called eflash_write_enable() before.
 *
 ******************************************************************************/
unsigned eflash_chip_erase(void)
{
    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_CHIP_ERASE);
    eflash_chip_deselect();
    return eflash_wait_for_result();
}


/*******************************************************************************
 * Start a sequencial write operation at flash_addr.
 * Return the content of the status register after the flash got ready.
 * The caller must have called eflash_write_enable() before.
 * Use eflash_seq_write_end() to end the sequencial write mode.
 * 
 ******************************************************************************/
unsigned eflash_seq_write_start(unsigned flash_addr)
{
    seq_write_start_addr = flash_addr;

    // kind of useless, but to keep a clean API...
    return 0; //eflash_wait_for_result();
}


/*******************************************************************************
 * Write a byte in sequencial write mode.
 * The caller must have called eflash_seq_write_start() before.
 * Return the content of the status register after the flash got ready.
 ******************************************************************************/
unsigned eflash_seq_write_byte(unsigned byte)
{
    eflash_chip_select();
    eflash_send_byte(EFLASH_OP_SEQ_WRITE);

    // the first byte has to be sent after the address
    if (seq_write_start_addr != ~0)
    {
        eflash_send_addr(seq_write_start_addr);
        seq_write_start_addr = ~0;
    }

    eflash_send_byte(byte);
    eflash_chip_deselect();
    return eflash_wait_for_result();
}


/*******************************************************************************
 * End the sequencial write mode.
 *
 ******************************************************************************/
void eflash_seq_write_end(void)
{
    eflash_write_disable();
}
