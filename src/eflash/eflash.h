/*
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#ifndef EFLASH_H_
#define EFLASH_H_

#include <stdint.h>

/* for AT25DF041A */
#define EFLASH_OP_READ_ID           0x9f
#define EFLASH_OP_READ_STATUS       0x05
#define EFLASH_OP_READ_ARRAY        0x0b
#define EFLASH_OP_WRITE_ENABLE      0x06
#define EFLASH_OP_WRITE_DISABLE     0x04
#define EFLASH_OP_SECTOR_LOCK       0x36
#define EFLASH_OP_SECTOR_UNLOCK     0x39
#define EFLASH_OP_READ_SECTOR_PROT  0x3c
#define EFLASH_OP_WRITE_DISABLE     0x04
#define EFLASH_OP_BLOCK64_ERASE     0xd8
#define EFLASH_OP_CHIP_ERASE        0x60
#define EFLASH_OP_SEQ_WRITE         0xad

#define EFLASH_ID_AT25DF041A        0x1f440100

#define EFLASH_STATUS_BUSY_MASK     0x01
#define EFLASH_STATUS_ERROR_MASK    0x20


void eflash_reset(void);
unsigned eflash_wait_for_result(void);
unsigned eflash_get_id(void);
void eflash_read_start(unsigned flash_addr);
unsigned eflash_read_byte(void);
void eflash_read_bytes(uint8_t mem[], unsigned n_bytes);
void eflash_read_end(void);
void eflash_write_enable(void);
void eflash_write_disable(void);
void eflash_sector_unlock(unsigned addr);
unsigned eflash_read_sector_prot(unsigned addr);
unsigned eflash_block_erase(unsigned addr);
unsigned eflash_chip_erase(void);
unsigned eflash_seq_write_start(unsigned flash_addr);
unsigned eflash_seq_write_byte(unsigned byte);
void eflash_seq_write_end(void);

#endif /* EFLASH_H_ */
