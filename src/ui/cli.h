/*
 * (c) 2008-2010 Thomas Giesel <skoe@directbox.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef CLI_H
#define CLI_H

#ifdef __XC__
void cli_thread(streaming chanend uart_ce,
                streaming chanend mos6502_ctrl_ce,
                streaming chanend cli_ui_ce);
#endif

#ifdef __XC__
void cli_set_chanends(streaming chanend mos6502_ctrl_ce);
#else
void cli_set_chanends(unsigned mos6502_ctrl_ce);
#endif

void cli_check(int c);
void cli_show_line(void);

#endif
