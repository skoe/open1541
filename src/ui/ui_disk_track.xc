/*
 * ui_disk_track.xc - Functions to load and save disk tracks
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>

#include "easy41.h"
#include "uart.h"
#include "drive.h"


/*******************************************************************************
 * Load a sector from the disk into the raw sector buffer.
 * This is called from the UI thread to make sure the drive thread doesn't
 * block for too long.
 *
 * track    0-based (!) track number 0..(MAX_NUM_TRACKS - 1)
 * sector   sector number
 *
 ******************************************************************************/
void ui_load_disk_sector(unsigned n_buffer, unsigned track, unsigned sector)
{
    unsigned offset;

    // sector being currently loaded - todo: use end of raw buffer and convert in-place
    static uint8_t sector_buffer[256];

    if (easy41_disk_image_fd == 0)
    {
        drive_fill_buffer_random(n_buffer);
        return;
    }

    // todo: check track range
    offset = drive_get_d64_track_offset(track) + 0x100 * sector;
    if (easy41_fs_seek(easy41_disk_image_fd, offset) == 0)
    {
        uart_puts("seek failed\r\n");
        drive_fill_buffer_random(n_buffer);
        return;
    }

    if (easy41_fs_read(easy41_disk_image_fd, sector_buffer, 256) != 256)
    {
        uart_puts("image read error\r\n");
        drive_fill_buffer_random(n_buffer);
        return;
    }

    drive_convert_to_raw_sector(n_buffer, sector_buffer, track, sector);
    //uart_puts("Load OK\r\n");
}
