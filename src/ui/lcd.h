/*
 * lcd.h
 *
 *  Created on: 22.05.2010
 *      Author: skoe
 */

#ifndef LCD_H_
#define LCD_H_

void lcd_init(void);
void lcd_write_string(const char str[]);
void lcd_write_command(unsigned val);
void lcd_write_data(unsigned val);
void lcd_goto_line1(void);
void lcd_goto_line2(void);
void lcd_clr(void);
#endif /* LCD_H_ */
