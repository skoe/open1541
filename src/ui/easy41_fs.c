
#include <string.h>

#include <fat.h>
#include <fat_config.h>
#include <partition.h>
#include <sd_raw.h>
#include <sd_raw_config.h>

#include <uart.h>
#include <util.h>
#include <lcd.h>

#include "easy41.h"

static struct partition_struct*     partition;
static struct fat_fs_struct*        fs;

/*******************************************************************************
 * Initialize the SD card.
 *
 ******************************************************************************/
void easy41_fs_init(void)
{
    if(!sd_raw_init())
    {
        uart_puts("Failed to initialize MMC/SD card\r\n");
        for (;;)
            usleep(0xff);
    }

    /* open first partition */
    partition = partition_open(sd_raw_read, sd_raw_read_interval, sd_raw_write,
            sd_raw_write_interval, 0);

    if (!partition)
    {
        /* No partition table */
        partition = partition_open(sd_raw_read, sd_raw_read_interval,
                sd_raw_write, sd_raw_write_interval, -1);
    }

    if (!partition)
    {
        uart_puts("Failed to open partition\r\n");
        for (;;)
            usleep(0xff);
    }

    /* open file system */
    fs = fat_open(partition);
    if (!fs)
    {
        uart_puts("Failed to open FAT\r\n");
        for (;;)
            usleep(0xff);
    }
}


/*******************************************************************************
 * Open a file, return a file handle or 0.
 *
 ******************************************************************************/
int easy41_fs_open(const char name[])
{
    struct fat_dir_entry_struct directory;
    struct fat_dir_entry_struct file_entry;
    struct fat_dir_struct* dd;

    // todo: fd rausfinden
//    *p_fd = 0;
    if (!fat_get_dir_entry_of_path(fs, "/", &directory))
    {
        uart_puts("Failed to get directory\r\n");
        return 0;
    }

    dd = fat_open_dir(fs, &directory);
    if (!dd)
    {
        uart_puts("Failed to open directory\r\n");
        return 0;
    }

    while (fat_read_dir(dd, &file_entry))
    {
        if (strcmp(file_entry.long_name, name) == 0)
        {
            fat_close_dir(dd);
            return (int) fat_open_file(fs, &file_entry);
        }
    }

    fat_close_dir(dd);
    return 0;
}


/*******************************************************************************
 * Close the file.
 * Return 0 on failure, 1 on success.
 *
 ******************************************************************************/
int easy41_fs_close(int fd)
{
    fat_close_file((struct fat_file_struct*)fd);
    return 1;
}


/*******************************************************************************
 * Call d_read.
 *
 ******************************************************************************/
int easy41_fs_read(int fd, uint8_t p[], unsigned n_bytes)
{
    return fat_read_file((struct fat_file_struct*)fd, (void*)p, n_bytes);
}


/*******************************************************************************
 * Set the file position.
 * Return 0 on failure, 1 on success.
 *
 ******************************************************************************/
int easy41_fs_seek(int fd, int32_t pos)
{
    return fat_seek_file((struct fat_file_struct*)fd, &pos, FAT_SEEK_SET);
}


/*******************************************************************************
 * Show the directory to the CLI.
 *
 ******************************************************************************/
void easy41_fs_dir_to_cli_old(void)
{
    struct fat_dir_entry_struct directory;
    struct fat_dir_entry_struct file_entry;
    struct fat_dir_struct* dd;

    if (!fat_get_dir_entry_of_path(fs, "/", &directory))
        goto failed;

    dd = fat_open_dir(fs, &directory);
    if (!dd)
        goto failed;

    while (fat_read_dir(dd, &file_entry))
    {
        uart_puts(file_entry.long_name);
        uart_putcrlf();
    }
    fat_close_dir(dd);
    return;

failed:
    uart_puts("Failed to get directory\r\n");
}
/*******************************************************************************
 * Show the directory to the CLI.
 *
 ******************************************************************************/
void easy41_fs_dir_to_cli(void) //n2c
{
    struct fat_dir_entry_struct directory;
    struct fat_dir_entry_struct file_entry;
    struct fat_dir_struct* dd;
    int dirs = 0;
    int disks = 0;
    if (!fat_get_dir_entry_of_path(fs, "/", &directory))
        goto failed;

    dd = fat_open_dir(fs, &directory);
    if (!dd)
        goto failed;

    while (fat_read_dir(dd, &file_entry))
    {
        if (file_entry.attributes & (1 << 4)) //is DIR?
        {
            //lcd_clr();
            //lcd_write_string("/");
            //lcd_write_string(file_entry.long_name);
            //lcd_goto_line2();
            //lcd_write_string("DIR entry");
            uart_puts("/");
            uart_puts(file_entry.long_name);
            uart_putcrlf();
            dirs++;
        }

        if (easy41_fs_size_is_d64(file_entry.file_size))
        {
            uart_puts(file_entry.long_name);
            uart_putcrlf();
            disks++;
        }
    }
    uart_puts("-------------\r\nDIRs: ");
    uart_putdec(dirs);
    uart_puts("\r\nD64s: ");
    uart_putdec(disks);
    uart_putcrlf();
    fat_close_dir(dd);
    return;

failed:
    uart_puts("Failed to get directory\r\n");
}


/*******************************************************************************
 * adapted from easy41_fs_dir_to_cli -> show current d64 on LCD
 *
 ******************************************************************************/
void easy41_fs_d64_to_cli(int no)
{
    unsigned dirs = 0;
    unsigned disks = 0;
    int attach_flag = 0;
    char filename[EASY41_FILENAME_SIZE];
    int i = 0;
    char entry_line[16] = "Entry: 000/000  ";
    //                     1234567890123456 display length

    // calculate all digits, put them in reverse order into buf

    if (no >= 1000)
    {
        no = no - 1000;
        attach_flag = 1;
    }

    struct fat_dir_entry_struct directory;
    struct fat_dir_entry_struct file_entry;
    struct fat_dir_struct* dd;

    if (!fat_get_dir_entry_of_path(fs, "/", &directory))
        goto failed;

    dd = fat_open_dir(fs, &directory);
    if (!dd)
        goto failed;

    while (fat_read_dir(dd, &file_entry))
    {
        msleep(1);
        if (file_entry.attributes & FAT_ATTRIB_DIR)
        {
            //lcd_clr();
            //lcd_write_string("/");
            //lcd_write_string(file_entry.long_name);
            //lcd_goto_line2();
            //lcd_write_string("DIR entry");
            //uart_puts("/");
            //uart_puts(file_entry.long_name);
            //uart_putcrlf();
            dirs++;
        }

        if (easy41_fs_size_is_d64(file_entry.file_size))
        {
            //uart_puts(file_entry.long_name);
            //uart_putcrlf();
            if (disks == no)
            {
            for (i = 0; i < EASY41_FILENAME_SIZE; ++i)
                    {
                    filename[i] = file_entry.long_name[i];
                    }
              if (strlen(filename)<15)
                  {
                   for (i=strlen(filename);i<16;i++)
                        {filename[i]=0x20;}
                  }
              filename[16]=0x00;
                if (attach_flag)
                {
                    // fixme: 0-termination?
                    easy41_fs_attach_disk(file_entry.long_name);
                    lcd_goto_line1();
                    lcd_write_string("* ");
                    lcd_write_string(filename);
                    lcd_goto_line2();
                    lcd_write_string("  d64 attached! ");
                    uart_puts("attached ");
                    uart_puts(filename);
                    //uart_putdec(disks);
                    uart_putcrlf();
                    fat_close_dir(dd);
                    return;
                }
                else
                {
                    lcd_write_command(0x80);
                    lcd_write_string(filename);
                }
            }
            disks++;
        }
    }

    lcd_goto_line2();
    entry_line[7] = (no + 1) / 100 + 0x30;
    entry_line[8] = (no + 1) % 100 / 10 + 0x30;
    entry_line[9] = (no + 1) % 10 + 0x30;

    entry_line[11] = disks / 100 + 0x30;
    entry_line[12] = disks % 100 / 10 + 0x30;
    entry_line[13] = disks % 10 + 0x30;
    lcd_write_string(entry_line);
    /*
     uart_puts("-------------");
     uart_putcrlf();
     uart_puts("DIRs: ");
     uart_putdec(dirs);
     uart_putcrlf();
     uart_puts("D64s: ");
     uart_putdec(disks);
     uart_putcrlf();
     uart_puts("Entry: ");
     uart_putdec(no);
     uart_putcrlf();
     */
    fat_close_dir(dd);
    return;

    failed: uart_puts("Failed to get directory\r\n");
}


/*******************************************************************************
 * Attach a disk image. Will refuse to work if there's still one attached.
 *
 ******************************************************************************/
void easy41_fs_attach_disk(const char name[])
{
    if (easy41_disk_image_fd)
    {
        uart_puts("Still a disk attached\r\n");
    }
    else
    {
        easy41_disk_image_fd = easy41_fs_open(name);
        if (easy41_disk_image_fd)
            uart_puts("OK\r\n");
        else
        {
            uart_puts("Cannot open \"");
            uart_puts(name);
            uart_puts("\"\r\n");
        }
    }
}


/*******************************************************************************
 * Detach the current disk image, if any.
 *
 ******************************************************************************/
void easy41_fs_detach_disk(void)
{
    if (easy41_disk_image_fd)
    {
        easy41_fs_close(easy41_disk_image_fd);
        // todo: do the write protect stuff so the drive can see the disk swap
        uart_puts("Disk now detached\r\n");
        lcd_clr();
        lcd_goto_line2();
        lcd_write_string("d64 now detached");
        easy41_disk_image_fd = 0;
    }
}


/*******************************************************************************
 * Return 1 if the given file size equates that of a d64 image, 0 otherwise.
 *
 *   Disk type                  Size
 *   ---------                  ------
 *   35 track, no errors        174848
 *   35 track, 683 error bytes  175531
 *   40 track, no errors        196608
 *   40 track, 768 error bytes  197376
 *
 ******************************************************************************/
__attribute__((noinline)) unsigned easy41_fs_size_is_d64(unsigned size)
{
    if ((size == 174848) ||
        (size == 196608) ||
        (size == 175531) ||
        (size == 197376))
        return 1;
    else
        return 0;
}

