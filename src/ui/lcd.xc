/*
 * lcd.xc
 *
 *  Created on: 21.05.2010
 *      Author: skoe
 */

#include <xs1.h>
#include <util.h>

#include <config.h>

// define command bytes for the LCD
#define LCD_CLR         0x01
#define LCD_HOME        0x02

#define LCD_CTL             0x08
#define LCD_CTL_LCD_ON      0x04
#define LCD_CTL_CRSR_ON     0x02
#define LCD_CTL_CRSR_BLINK  0x02

#define LCD_ENTRY           0x04
#define LCD_ENTRY_INC       0x02    // otherwise: decrement
#define LCD_ENTRY_SHIFT     0x01    // otherwise: don't shift

out port port_lcd_e    = PORT_LCD_E;
out port port_lcd_rs   = PORT_LCD_RS;
out port port_lcd_data = PORT_LCD_DATA;


/*******************************************************************************
 * Send 4 bits to the LCD.
 *
 ******************************************************************************/
static void lcd_write_nibble(unsigned val)
{
    port_lcd_data <: val;
    usleep(1);
    port_lcd_e <: 1;
    usleep(1);
    port_lcd_e <: 0;
    usleep(1);
}


/*******************************************************************************
 *
 ******************************************************************************/
void lcd_write_command(unsigned val)
{
    port_lcd_rs <: 0;
    lcd_write_nibble(val >> 4);
    lcd_write_nibble(val);
    msleep(2);
}


/*******************************************************************************
 *
 ******************************************************************************/
void lcd_write_data(unsigned val)
{
    port_lcd_rs <: 1;
    lcd_write_nibble(val >> 4);
    lcd_write_nibble(val);
    msleep(1);
}


/*******************************************************************************
 *
 ******************************************************************************/
void lcd_goto_line1(void)
{
    lcd_write_command(0x80);
}


/*******************************************************************************
 *
 ******************************************************************************/
void lcd_goto_line2(void)
{
    lcd_write_command(0xc0);
}


/*******************************************************************************
 * Clear the display and go to the start of line 1
 *
 ******************************************************************************/
void lcd_clr(void)
{
    lcd_write_command(LCD_CLR);
    lcd_write_command(0x80);
}


/*******************************************************************************
 *
 ******************************************************************************/
void lcd_write_string(const char str[])
{
    unsigned i;
    for (i = 0; str[i]; ++i)
        lcd_write_data(str[i]);
}


/*******************************************************************************
 *
 ******************************************************************************/
void lcd_init()
{
    port_lcd_e    <: 0;
    port_lcd_rs   <: 0;
    port_lcd_data <: 0;
    // wake it up
    lcd_write_nibble(0x03);
    msleep(30);
    lcd_write_nibble(0x03);
    msleep(10);
    lcd_write_nibble(0x03);
    msleep(10);
    // select 4 bit mode
    lcd_write_nibble(0x02);

    // 4-bit, 2 lines, 5x8 dots
    lcd_write_command(0x28);

    lcd_write_command(LCD_CTL | LCD_CTL_LCD_ON);
    lcd_write_command(LCD_ENTRY | LCD_ENTRY_INC);

//define chars
    lcd_write_command(0x40);
    lcd_write_string("11111111"); //char 00 - skip it

    lcd_write_string("\x80\x04\x0e\x15\x4\x4\x4\x80"); //char 01
    lcd_write_string("\x80\x4\x4\x04\x15\x0e\x4\x80"); //char 02

    
    lcd_write_string("\x80\x80\x04\x04\x80\x04\x04\x80"); //char 03 disk 0
    lcd_write_string("\x80\x80\x10\x08\x80\x02\x01\x80"); //char 04 disk 1
    lcd_write_string("\x80\x80\x80\x80\x1b\x80\x80\x80"); //char 05 disk 2
    lcd_write_string("\x80\x80\x01\x02\x80\x08\x10\x80"); //char 06 disk 3

    lcd_write_command(0x80);
    lcd_write_string("2/3: \x01up/\x02");
    lcd_write_string("down");
//                    1234567890123456 display length
    lcd_write_command(0xc0);
    lcd_write_string("1/4: \x7f");
    lcd_write_string("eject/\x7eok");
  //                  1234567890123456 display length

}

