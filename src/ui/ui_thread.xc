/*
 * ui_thread.xc - Disk drive emulation
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>

#include <config.h>
#include <uart.h>
#include <drive.h>
#include <lcd.h>
#include <cli.h>
#include <mos6502.h>
#include "easy41.h"

static void ui_buttons_changed(unsigned buttons_state);
static void ui_check_drive_msg(streaming chanend drive_ui_ce,
                               unsigned msg);
static void load_rom(void);

/*******************************************************************************
 * Global data
 *
 ******************************************************************************/

/* FD of the disk image currently attached, 0 if none */
int easy41_disk_image_fd;

// no of dir shown on LCD
int dir_entry_no;

/*******************************************************************************
 * Local data
 *
 ******************************************************************************/

in port port_buttons = PORT_BUTTONS;

/*******************************************************************************
 * 
 *
 ******************************************************************************/
void ui_thread(streaming chanend drive_ui_ce,
               streaming chanend mos6502_ui_ce,
               streaming chanend uart_input_ce)
{
    unsigned msg;
    unsigned buttons_state;
    unsigned buttons_changed;
    unsigned t_buttons_stable;
    timer    tmr;

    lcd_init();
    cli_set_chanends(mos6502_ui_ce);

    uart_puts("= Easy41 CLI =\r\n");
    uart_puts("Try to load ROM\r\n");
    easy41_fs_init();
    load_rom();

    mos6502_ui_ce <: MOS6502_CTRL_RESET;
    mos6502_ui_ce <: MOS6502_CTRL_RUN;

    easy41_disk_image_fd = 0;
    dir_entry_no = 0;

    buttons_changed = 0;
    buttons_state = ~0;

    for (;;)
    {
        select
        {
        case drive_ui_ce :> msg:
            ui_check_drive_msg(drive_ui_ce, msg);
            break;

        case uart_input_ce :> msg:
            // this is a byte received from UART
            cli_check(msg);
            break;

        case port_buttons when pinsneq(buttons_state) :> buttons_state:
            buttons_changed = 1;
            // 20 ms debounce time
            tmr :> t_buttons_stable;
            t_buttons_stable += 20 * XS1_TIMER_KHZ;
            break;

        case buttons_changed => tmr when
                timerafter(t_buttons_stable) :> void:
            ui_buttons_changed(~buttons_state);
            buttons_changed = 0;
            break;
        }
    }
}


/*******************************************************************************
 * At least one button changed its state.
 *
 ******************************************************************************/
static void ui_buttons_changed(unsigned buttons_state)
{
    static unsigned buttons_old;
    unsigned events;

    events = buttons_state & ~buttons_old;

    if (events & BUTTON_ESC)
    {
        easy41_fs_detach_disk();
    }

    if ((events & BUTTON_UP) && !easy41_disk_image_fd)
    {
        if (dir_entry_no >= 1)
        {
            dir_entry_no--;
        }
        easy41_fs_d64_to_cli(dir_entry_no);
    }

    if ((events & BUTTON_DOWN) && !easy41_disk_image_fd)
    {
        dir_entry_no++;
        easy41_fs_d64_to_cli(dir_entry_no);
    }

    if (events & BUTTON_OK)
    {
        easy41_fs_d64_to_cli(dir_entry_no + 1000);
    }

    buttons_old = buttons_state;
}


/*******************************************************************************
 * Execute a command from the drive emulation.
 *
 ******************************************************************************/
static void ui_check_drive_msg(streaming chanend drive_ui_ce,
                               unsigned msg)
{
    unsigned val, n_buffer, track, sector;
static uint8_t state;
    int anim;
    switch (msg)
    {
    case DRIVE_UI_LOAD_D64_SECTOR_REQ:
        drive_ui_ce :> val;
        n_buffer = val >> 16;
        track    = (val >> 8) & 0xff;
        sector   = val & 0xff;
#if 0
        uart_puts("Load ");
        uart_putdec(track);
        uart_putc('/');
        uart_putdec(sector);
        uart_putcrlf();
#endif
        ui_load_disk_sector(n_buffer, track, sector);
        state++;
        anim=(state>>5)&3;
        //state=(sector/20)%4+3;
        lcd_goto_line2();
        lcd_write_data(anim+3);
        // confirmation
        drive_ui_ce <: DRIVE_UI_LOAD_D64_SECTOR_REQ;
        break;
    }
}


/*******************************************************************************
 * Load the DOS ROM image.
 *
 ******************************************************************************/
static void load_rom(void)
{
    int rv, i, fd, sum;

    fd = easy41_fs_open("rom.bin");
    if (fd == 0)
    {
        uart_puts("Failed to open rom.bin (");
        uart_putdec_str(fd, ")\r\n");
        for(;;);
    }

    rv = easy41_fs_read(fd, rom, MOS6502_ROM_SIZE);
    if (rv != MOS6502_ROM_SIZE)
    {
        uart_puts("Failed to read rom.bin or wrong size (");
        uart_putdec_str(rv, ")\r\n");
        for(;;);
    }

    sum = 0;
    for (i = 0; i < 16 * 1024; ++i)
        sum += rom[i];

    uart_puts("ROM loaded.\r\n");
    uart_putdec(sum);
    uart_putcrlf();
    easy41_fs_close(fd);
}
