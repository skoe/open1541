/*
 * (c) 2008-2010 Thomas Giesel <skoe@directbox.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef EASY41_H
#define EASY41_H

#include <stdint.h>

#define EASY41_DIR_REQ          0
#define EASY41_DETACH_DISK_REQ  1
#define EASY41_ATTACH_DISK_REQ  2

/*
 * Size of a file name including 0-termination.
 * Must be same as fat_dir_entry_struct.long_name.
 */
#define EASY41_FILENAME_SIZE    32


#ifdef __XC__
void ui_thread(streaming chanend drive_ui_ce,
               streaming chanend mos6502_ui_ce,
               streaming chanend uart_input_ce);
#endif

void ui_load_disk_sector(unsigned n_buffer, unsigned track, unsigned sector);

/* from easy41_fs.c */
void easy41_fs_init(void);
int easy41_fs_open(const char name[]);
int easy41_fs_close(int fd);
int easy41_fs_read(int fd, uint8_t p[], unsigned n_bytes);
int easy41_fs_seek(int fd, int32_t pos);
unsigned easy41_fs_size_is_d64(unsigned size);

void easy41_fs_dir_to_cli(void);
void easy41_fs_attach_disk(const char name[]);
void easy41_fs_detach_disk(void);

void easy41_fs_d64_to_cli(int no); //n2c

extern int easy41_disk_image_fd;

#endif
