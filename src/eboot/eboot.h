

#ifndef EBOOT_H
#define EBOOT_H

#include <stdint.h>

// Maximal update image size in bytes, including header and checksum
#define EBOOT_MAX_IMAGE_SIZE 50000

// Address in flash eprom for application image
#define EBOOT_FLASH_APP_ADDR 0x10000

// Name for the booter image file on SD card
#define EBOOT_BOOTER_FILENAME "eboot.img"



extern uint32_t image[EBOOT_MAX_IMAGE_SIZE / 4];

unsigned eboot_check_image_crc(void);

void eboot_start_image(void);
int eboot_init_sdcard(void);
void eboot_check_update_booter(void);
void eboot_check_update_app(void);
void eboot_start_app_from_flash(void);
unsigned eboot_load_image_from_uart(void);


#ifndef __XC__
void eboot_start_from_ram(void* function_buffer, uint32_t* image,
                          unsigned n_words);
#endif

#endif
