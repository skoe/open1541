/*
 * Easy41
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>
#include <stdio.h>
#include <xclib.h>
#include <string.h>
#include "config.h"

#include <fat.h>
#include <fat_config.h>
#include <partition.h>
#include <sd_raw.h>
#include <sd_raw_config.h>

#include "uart.h"
#include "util.h"
#include "eflash.h"

#include "eboot.h"

static unsigned eboot_load_image(const char* name);
static struct fat_file_struct* eboot_open_file(const char* name);
static unsigned eboot_wait_for_uart(int val);
static void eboot_receive_file_from_uart(void);

static struct partition_struct*     partition;
static struct fat_fs_struct*        fs;

uint32_t image[EBOOT_MAX_IMAGE_SIZE / 4];


/*******************************************************************************
 * Start the software in image[].
 *
 ******************************************************************************/
void eboot_start_image(void)
{
    unsigned asm_function_buffer[64];

    // will never return, hopefully
    eboot_start_from_ram(asm_function_buffer, image + 1,
                         EBOOT_MAX_IMAGE_SIZE / 4 - 1);
    for (;;);
}


/*******************************************************************************
 * Initialize the SD card.
 * Return 1 of am SD card with FAT has been found, 0 otherwise.
 *
 ******************************************************************************/
int eboot_init_sdcard(void)
{
    if(!sd_raw_init())
    {
        uart_puts("Failed to initialize MMC/SD card\r\n");
        return 0;
    }

    /* open first partition */
    partition = partition_open(sd_raw_read, sd_raw_read_interval, sd_raw_write,
            sd_raw_write_interval, 0);

    if (!partition)
    {
        /* No partition table */
        partition = partition_open(sd_raw_read, sd_raw_read_interval,
                sd_raw_write, sd_raw_write_interval, -1);
    }

    if (!partition)
    {
        uart_puts("Failed to open partition\r\n");
        return 0;
    }

    /* open file system */
    fs = fat_open(partition);
    if (!fs)
    {
        uart_puts("Failed to open FAT\r\n");
        return 0;
    }
    return 1;
}


/*******************************************************************************
 * Check if the booter has to be updated. If yes, do it.
 * eboot_init_sdcard must have been called already.
 *
 ******************************************************************************/
void eboot_check_update_booter(void)
{
    unsigned len, i, rv, byte;

    len = eboot_load_image(EBOOT_BOOTER_FILENAME);
    if (!len)
        return;

    if (!eboot_check_image_crc())
    {
        uart_puts("CRC bad!\r\n");
        return;
    }

    // check if the booter is unchanged
    eflash_read_start(0);
    for (i = 0; i < len; ++i)
    {
        byte = bitrev(byterev(eflash_read_byte()));
        if (byte != ((uint8_t*)image)[i])
            break;
    }
    eflash_read_end();
    if (i == len)
    {
        uart_puts("Image up to date.\r\n");
        return;
    }

    // write it to flash
    eflash_write_enable();
    eflash_sector_unlock(0);
    eflash_write_enable();
    eflash_block_erase(0);
    eflash_write_enable();
    eflash_seq_write_start(0);
    for (i = 0; i < len; ++i)
    {
        byte = ((uint8_t*)image)[i];
        // XMOS wants to boot LSB first
        byte = bitrev(byterev(byte));
        rv = eflash_seq_write_byte(byte);
        if (rv & EFLASH_STATUS_ERROR_MASK)
        {
            uart_puts("Flash error\r\n");
            return;
        }
    }
    eflash_seq_write_end();
    uart_puts("Written to flash.\r\n");
}


/*******************************************************************************
 * Check if the application has to be updated. If yes, do it.
 * eboot_init_sdcard must have been called already.
 *
 ******************************************************************************/
void eboot_check_update_app(void)
{
    unsigned len, i, rv, byte;

    len = eboot_load_image("easy41.img");
    if (!len)
        return;

    if (!eboot_check_image_crc())
    {
        uart_puts("CRC bad!\r\n");
        return;
    }

    // check if the app is unchanged
    eflash_read_start(EBOOT_FLASH_APP_ADDR);
    for (i = 0; i < len; ++i)
    {
        byte = eflash_read_byte();
        if (byte != ((uint8_t*)image)[i])
            break;
    }
    eflash_read_end();
    if (i == len)
    {
        uart_puts("Image up to date.\r\n");
        return;
    }

    // write it to flash
    eflash_write_enable();
    eflash_sector_unlock(EBOOT_FLASH_APP_ADDR);
    eflash_write_enable();
    eflash_block_erase(EBOOT_FLASH_APP_ADDR);
    eflash_write_enable();
    eflash_seq_write_start(EBOOT_FLASH_APP_ADDR);
    for (i = 0; i < len; ++i)
    {
        byte = ((uint8_t*)image)[i];
        rv = eflash_seq_write_byte(byte);
        if (rv & EFLASH_STATUS_ERROR_MASK)
        {
            uart_puts("Flash error\r\n");
            return;
        }
    }
    eflash_seq_write_end();
    uart_puts("Written to flash.\r\n");
}


/*******************************************************************************
 * Read a file into the image buffer. Return the number of bytes loaded.
 *
 ******************************************************************************/
static unsigned eboot_load_image(const char* name)
{
    struct fat_file_struct* file;
    unsigned len;

    uart_puts("Check ");
    uart_puts(name);
    uart_puts("... ");

    file = eboot_open_file(name);
    if (!file)
    {
        uart_puts("not found\r\n");
        return 0;
    }

    len = fat_read_file(file, (void*)image, sizeof(image));
    fat_close_file(file);

    uart_putdec_str(len, " bytes loaded\r\n");

    return len;
}


/*******************************************************************************
 * Open a file, return a file handle or 0.
 *
 ******************************************************************************/
static struct fat_file_struct* eboot_open_file(const char* name)
{
    struct fat_dir_entry_struct directory;
    struct fat_dir_entry_struct file_entry;
    struct fat_dir_struct* dd;

    if (!fat_get_dir_entry_of_path(fs, "/", &directory))
    {
        uart_puts("Failed to get directory\r\n");
        return 0;
    }

    dd = fat_open_dir(fs, &directory);
    if (!dd)
    {
        uart_puts("Failed to open directory\r\n");
        return 0;
    }

    while (fat_read_dir(dd, &file_entry))
    {
        if (strcmp(file_entry.long_name, name) == 0)
        {
            fat_close_dir(dd);
            return fat_open_file(fs, &file_entry);
        }
    }

    fat_close_dir(dd);
    return 0;
}


/*******************************************************************************
 *
 ******************************************************************************/
void eboot_start_app_from_flash(void)
{
    unsigned i;

    eflash_read_start(EBOOT_FLASH_APP_ADDR);
    for (i = 0; i < EBOOT_MAX_IMAGE_SIZE; ++i)
    {
        ((uint8_t*)image)[i] = eflash_read_byte();
    }
    eflash_read_end();
    eboot_start_image();
}


/*******************************************************************************
 * Try to load a software image over uart.
 * Return 1 if it has been received and the CRC is ok. Return 0 otherwise.
 *
 ******************************************************************************/
unsigned eboot_load_image_from_uart(void)
{
    unsigned retry;

    uart_puts("Wait for UART boot... ");

    // try to load the file 3 times
    for (retry = 0; retry < 3; ++retry)
    {
        // wait for start symbol
        if (!eboot_wait_for_uart('!'))
            return 0;

        // tell them that we're listening
        uart_putc('?');

        // wait for single sync byte
        if (!eboot_wait_for_uart('>'))
            return 0;

        eboot_receive_file_from_uart();

        // file complete, check it
        if (eboot_check_image_crc())
        {
            uart_putc('A');
            return 1;
        }
        else
        {
            uart_putc('N');
            return 1;
        }
    }

    return 0;
}


/*******************************************************************************
 * Wait until the given value is received over UART. Ignore and discard all
 * other values. If no character is received for 200 ms, return 0.
 * Return 1 if the right byte has been received.
 *
 ******************************************************************************/
static unsigned eboot_wait_for_uart(int val)
{
    int c;
    do
    {
        // wait 250 ms for a start byte from UART
        c = uart_getc(XS1_TIMER_KHZ * 200);
        if (c == -1)
        {
            // timeout: don't load from UART
            uart_puts("timeout\r\n");
            return 0;
        }
    }
    while (c != val);
    return 1;
}


/*******************************************************************************
 * Load a file from UART. Try to receive up to EBOOT_MAX_IMAGE_SIZE bytes.
 * If there's not byte for 500 ms, assume EOF.
 *
 ******************************************************************************/
static void eboot_receive_file_from_uart()
{
    int c;
    unsigned i;

    for (i = 0; i < EBOOT_MAX_IMAGE_SIZE; ++i)
    {
        // wait 500 ms for each byte from UART
        c = uart_getc(XS1_TIMER_KHZ * 500);
        if (c == -1)
        {
            // timeout == EOF
            return;
        }
        else
            ((uint8_t*) image)[i] = c;
    }
}
