/*
 * Easy41
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>
#include <stdio.h>
#include "config.h"

#include "uart.h"
#include "util.h"
#include "eflash.h"
#include "eboot.h"

/*******************************************************************************
 * Do it.
 *
 ******************************************************************************/
int main(void)
{
    unsigned flash_type, sd_ok;

    uart_init();
    uart_puts("\r\n=== EBoot ===\r\n");

    if (eboot_load_image_from_uart())
    {
        uart_puts("Start image from UART:\r\n");
        eboot_start_image();
    }

    eflash_reset();
    uart_puts("Found flash: 0x");
    flash_type = eflash_get_id();
    uart_puthex_padded(8, flash_type);
    uart_putcrlf();

    sd_ok = eboot_init_sdcard();
    uart_puts("SD card ");
    if (!sd_ok)
        uart_puts("not ");
    uart_puts("ok\r\n");

    if (flash_type == EFLASH_ID_AT25DF041A)
    {
        if (sd_ok)
        {
            eboot_check_update_booter();
            eboot_check_update_app();
        }
    }
    else
    {
        uart_puts("Unknown flash type, update skipped\r\n");
    }

    uart_puts("Start image from flash:\r\n");
    eboot_start_app_from_flash();
    return 0;
}


/*******************************************************************************
 * Check if the software image in RAM has a valid CRC.
 * Return 1 if it is okay, 0 otherwise.
 *
 ******************************************************************************/
unsigned eboot_check_image_crc(void)
{
    unsigned words, i, crc;
    words = image[0];

    if ((words < 1) || (words > EBOOT_MAX_IMAGE_SIZE / 4 - 2))
        return 0;

    crc = 0xffffffff;
    crc32(crc, words, 0xedb88320);
    for (i = 1; i < words + 2; ++i)
    {
        crc32(crc, image[i], 0xedb88320);
    }
    crc = ~crc;

    if (crc)
        return 0; // bad

    return 1;
}

