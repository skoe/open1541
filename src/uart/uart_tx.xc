/* 
 * Low resource UART implementation.
 * Version 0.2
 * 
 * (c) 2010 Thomas 'skoe' Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>
#include <uart.h>
#include <util.h>
#include <config.h>

#define BIT_RATE 115200
#define BIT_TIME ((XS1_TIMER_HZ + BIT_RATE / 2) / BIT_RATE)

out port txd = PORT_UART_TX;


/*******************************************************************************
 * Initialize UART
 *
 ******************************************************************************/
void uart_init(void)
{
    timer tmr;
    unsigned t;

    // send 10 one-bits to get a clean start state
    txd <: 1;
    tmr :> t;
    tmr when timerafter(t + 10 * BIT_TIME) :> void;
}


/*******************************************************************************
 * Print the given character to UART.
 *
 ******************************************************************************/
void uart_putc(int c)
{
    unsigned i, time;
    timer    tmr;

    // add stop bit and start bit
    c |= 0x100;
    c <<= 1;
   
    // send these 10 bits
    tmr :> time;
    for (i = 0; i < 10; ++i)
    {
        txd <: >> c;
        time += BIT_TIME;
        tmr when timerafter(time) :> void;
    }
}
