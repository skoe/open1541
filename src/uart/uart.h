/*
 * uart.h - Poor mans debugging interface
 *
 * (c) 2008-2010 Thomas Giesel <skoe@directbox.com>
 *
 */

#ifndef UART_H_
#define UART_H_

#ifdef __XC__
#include <xs1.h>
#define UART_BIT_RATE 115200
#define UART_BIT_TIME ((XS1_TIMER_HZ + UART_BIT_RATE / 2) / UART_BIT_RATE)
#endif

#ifdef __XC__
void uart_rx_thread(streaming chanend ce);
void uart_tx_thread(streaming chanend ce);
#endif

void uart_init(void);

void uart_putc(int c);
int uart_getc(unsigned timeout);

void uart_putnc(int n, int c);
void uart_putcrlf(void);

void uart_puts(const char text[]);
void uart_puterror(const char reason[]);

void uart_puthex_padded(int size, unsigned num);
void uart_puthex_padded_sp(int size, unsigned num);
void uart_puthex(unsigned  num);
void uart_putdec_padded(int size, unsigned num);
void uart_putdec(unsigned num);
void uart_putdec_str(unsigned num, const char text[]);

#endif /*UART_H_*/
