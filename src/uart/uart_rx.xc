/* 
 * Low resource UART implementation.
 * Version 0.2
 * 
 * (c) 2010 Thomas 'skoe' Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>
#include <uart.h>
#include <util.h>
#include <config.h>

in port rxd = PORT_UART_RX;


/*******************************************************************************
 * Receive a character with timeout.
 * Timeout is in system clock ticks, 0 to wait forever.
 *
 * Return the character received or -1 when a timeout occured. 
 *
 ******************************************************************************/
int uart_getc(unsigned timeout)
{
    unsigned i, time, rx_value;
    timer    tmr;

    tmr :> time;
    time += timeout;

    // wait until RX goes high (idle or rest of previous stop bit)
    select
    {
    case timeout => tmr when timerafter(time) :> void:
        return -1;

    case rxd when pinsneq(0) :> void:
        break;
    }

    // wait until RX goes low (edge of start bit)
    select
    {
    case timeout => tmr when timerafter(time) :> void:
        return -1;

    case rxd when pinsneq(1) :> void:
        break;
    }

    // receive 8 bits
    tmr :> time;
    time += UART_BIT_TIME + UART_BIT_TIME / 2;
    rx_value = 0;
    for (i = 0; i < 8; ++i)
    {
        tmr when timerafter(time) :> void;
        rxd :> >> rx_value;
        time += UART_BIT_TIME;
    }

    return rx_value >> 24;
}
