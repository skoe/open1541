/*
 * uart.c - Poor mans debugging interface
 * 
 * (c) 2008-2010 Thomas Giesel <skoe@directbox.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <stdio.h>
#include <uart.h>

static inline void uart_puthex_digit(int digit);

/*******************************************************************************
 * Print the given character n times to UART. Wait if the FIFO is full.
 *
 ******************************************************************************/
void uart_putnc(int n, int c)
{
    int i;
    for (i = 0; i < n; i++)
    {
        uart_putc(c);
    }
}

/*******************************************************************************
 * Print CR LF to UART.
 *
 ******************************************************************************/
void uart_putcrlf(void)
{
    uart_putc(13);
    uart_putc(10);
}

/*******************************************************************************
 * Print the given string to UART.
 *
 ******************************************************************************/
void uart_puts(const char* text)
{
    char ch;

    while ((ch = *text++))
    {
        uart_putc(ch);
    }
}

/*******************************************************************************
 * Print a C64 style error message containing the given reason:
 * ?<reason> ERROR<\r><\n>
 *
 ******************************************************************************/
void uart_puterror(const char* reason)
{
    uart_putc('?');
    uart_puts(reason);
    uart_puts(" ERROR\r\n");
}

/*******************************************************************************
 * Print the given number in hex format. Fill up leading '0' on the left side
 * to get at least size digits.
 * If more then size digits are needed, use more.
 *
 ******************************************************************************/
void uart_puthex_padded(int size, unsigned num)
{
    int i;
    unsigned tmp;

    // always print at laest one digit
    if (size < 1)
        size = 1;

    for (i = 8; i != 0; i--)
    {
        tmp = (num & 0xf0000000) >> 28;

        if (tmp || i <= size)
        {
            uart_puthex_digit(tmp);

            // a digit != 0 was found, print all digits from now
            size = i;
        }

        num <<= 4;
    }
}


/*******************************************************************************
 * Call uart_puthex_padded and append a space character.
 *
 ******************************************************************************/
void uart_puthex_padded_sp(int size, unsigned num)
{
    uart_puthex_padded(size, num);
    uart_putc(' ');
}


/*******************************************************************************
 * Print the given number in hex format. Use as many digits as are needed,
 *
 ******************************************************************************/
void uart_puthex(unsigned num)
{
    uart_puthex_padded(0, num);
}

/*******************************************************************************
 * Print the given number in decimal format. Fill up leading spaces on the left
 * side to get at least *size* digits.
 * If more then *size* digits are needed, use more.
 *
 ******************************************************************************/
void uart_putdec_padded(int size, unsigned num)
{
    char *p, buf[11];
    int digits;

    // calculate all digits, put them in reverse order into buf
    p = buf;
    if (num == 0)
        *p++ = 0; // force 0 to be printed
    else
    {
        while (num != 0)
        {
            *p++ = num % 10;
            num /= 10;
        }
    }

    // print leading spaces
    digits = p - buf;
    while (digits++ < size)
        uart_putc(' ');

    // print the digits
    while (p != buf)
        uart_putc('0' + *--p);
}


/*******************************************************************************
 * Print the given number in decimal format. Use as many digits as are needed,
 *
 ******************************************************************************/
void uart_putdec(unsigned num)
{
    uart_putdec_padded(0, num);
}


/*******************************************************************************
 * Print the number in decimal format followed a string.
 *
 ******************************************************************************/
void uart_putdec_str(unsigned num, const char* text)
{
    uart_putdec_padded(0, num);
    uart_puts(text);
}


#if 0
/*******************************************************************************
 * Print a single character.
 *
 ******************************************************************************/
void uart_putc(int c)
{
    putchar(c);
}
#endif


/*******************************************************************************
 * Print one hex digit.
 *
 * digit    0..15, other values will break everything
 *
 ******************************************************************************/
static inline void uart_puthex_digit(int digit)
{
    // yes, this is smaller on ARM than if/else
    static const char digits[] = "0123456789abcdef";

    uart_putc(digits[digit]);
}
