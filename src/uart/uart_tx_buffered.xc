/* 
 * Low resource UART implementation.
 * Version 0.2
 * 
 * (c) 2010 Thomas 'skoe' Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <xs1.h>
#include <uart.h>
#include <util.h>
#include <config.h>

// size for TX buffer, must be 1<<n
#define FIFO_SIZE 512

#define BIT_RATE 115200
#define BIT_TIME ((XS1_TIMER_HZ + BIT_RATE / 2) / BIT_RATE)

// port for TX, must be 1 bit port
out port txd = PORT_UART_TX;

// this thread has enough time to care for the LCD contrast
out port port_lcd_contrast = PORT_LCD_CONTRAST;

// copy of channel end id for uart thread
static unsigned uart_chanend;

// circular buffer
static char uart_fifo[FIFO_SIZE];

// indexes into uart_fifo, empty when read == write
static unsigned fifo_next_write;
static unsigned fifo_next_read;

// this is redundant, but makes the code easier
static unsigned fifo_len;

/*******************************************************************************
 *
 ******************************************************************************/
void uart_rx_thread(streaming chanend ce)
{
    int c;

    for (;;)
    {
        c = uart_getc(0);
        ce <: c;
    }
}

/*******************************************************************************
 *
 ******************************************************************************/
void uart_tx_thread(streaming chanend ce)
{
    /* 
     * State for TX: We put also the stop bit into this sequence. We send and
     * shift this value until it becomes 0. Then all bits have been sent. When
     * the last bit is shifted out, we may start to send the next byte
     * imediately. That's why the last bit is set two times to get a whole
     * stop bit.
     */
    unsigned contrast = 50;
    unsigned tx_value = 0;
    unsigned contrast_pwm_cnt = 0;
    unsigned tmp;
    int t_next_tx;
    timer    tmr;

    asm
    (
        "stw     %0, dp[uart_chanend]   \n"
        : /* no output */
        : "r"(ce)
    );

    // send 10 one-bits to get a clean start state
    txd <: 1;
    tmr :> t_next_tx;
    t_next_tx += 10 * BIT_TIME;

    for (;;)
    {
        select
        {
        case fifo_len < FIFO_SIZE => ce :> tmp:
            // discard stop token
            sinct(ce);
            // Put new value into FIFO
            uart_fifo[fifo_next_write] = tmp;
            fifo_next_write = (fifo_next_write + 1) & (FIFO_SIZE - 1);
            ++fifo_len;
            break;

        case tmr when timerafter(t_next_tx) :> void:
            if (tx_value)
            {
                txd <: >> tx_value;
            }
            else if (fifo_len)
            {
                // start bit
                txd <: 0;
                // add stop bit
                tx_value = uart_fifo[fifo_next_read] | 0x100;
                fifo_next_read = (fifo_next_read + 1) & (FIFO_SIZE - 1);
                --fifo_len;
            }
            // has nothing to do with UART, but here is a good place:
            if (contrast_pwm_cnt < contrast)
                port_lcd_contrast <: 0;
            else
                port_lcd_contrast <: 1;
            if (++contrast_pwm_cnt >= 64)
                contrast_pwm_cnt = 0;

            t_next_tx += BIT_TIME;
            break;
        }
    }
}


/*******************************************************************************
 * Print the given character to UART. Wait if the queue is full.
 * May be called from different threads.
 * This function blocks until the uart_thread is up and running.
 *
 ******************************************************************************/
void uart_putc(int c)
{
    asm 
    (
        // allocate channel end
        // todo: check if it was available
        "getr    r2, 2                  \n" /* XS1_RES_TYPE_CHANEND */

        // set channel destination, wait until it gets != 0
        "nodest:                        \n"
        // todo: This direct access to uart_chanend w/o constraint isn't fine
        "ldw     r3, dp[uart_chanend]   \n"
        "bf      r3, nodest             \n"
        "setd    res[r2], r3            \n"
        // send byte and end token
        "out     res[r2], %0            \n"
        "outct   res[r2], 1             \n" /* XS1_CT_END */
        // release channel end
        "freer   res[r2]                \n"
        : /* no output */
        : "r"(c)
        : "r2", "r3"
    );

    // avoid warning
    (void)uart_chanend;
}
