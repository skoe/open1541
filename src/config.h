
#ifndef CONFIG_H
#define CONFIG_H

// flash memory
#define PORT_SPI_MISO       XS1_PORT_1A     // X0D0
#define PORT_SPI_SS_FLASH   XS1_PORT_1B     // X0D1
#define PORT_SPI_CLK        XS1_PORT_1C     // X0D10
#define PORT_SPI_MOSI       XS1_PORT_1D     // X0D11

#define PORT_TEST1          XS1_PORT_1E     // X0D22

#define PORT_UART_TX        XS1_PORT_1H     // X0D23
#define PORT_UART_RX        XS1_PORT_1I     // X0D24

// SD card
#define PORT_SD_SPI_SS      XS1_PORT_1N     // X0D37
#define PORT_SD_SPI_CLK     XS1_PORT_1K     // X0D34
#define PORT_SD_SPI_MOSI    XS1_PORT_1M     // X0D36
#define PORT_SD_SPI_MISO    XS1_PORT_1L     // X0D35
#define PORT_SD_WP          XS1_PORT_1O     // X0D38
#define PORT_SD_DETECT      XS1_PORT_1P     // X0D39

#define PORT_IEC_IN         XS1_PORT_8A     // X0D2..X0D9

// 0 = LED, 1 = DATA, 2 = ATN, 3 = CLK
#define PORT_IEC_OUT        XS1_PORT_4C     // X0D14, X0D15, X0D20, X0D21

// 4 Buttons, 0 = A, 1 = B, 2 = C, 3 = D
#define PORT_BUTTONS        XS1_PORT_4E     // X0D26, X0D27, X0D32, X0D33

#define BUTTON_ESC          0x01
#define BUTTON_UP           0x02
#define BUTTON_DOWN         0x04
#define BUTTON_OK           0x08
#define BUTTON_ALL          0x0f

#define PORT_LCD_DATA       XS1_PORT_4D     // X0D16, X0D17, X0D18, X0D19
#define PORT_LCD_E          XS1_PORT_1F     // X0D13
#define PORT_LCD_RS         XS1_PORT_1G     // X0D22
#define PORT_LCD_CONTRAST   XS1_PORT_1J     // X0D25

#endif /* CONFIG_H */
