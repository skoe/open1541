/*
 * Easy41
 *
 * (c) 2008-2010 Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <safestring.h>
#include <xs1.h>
#include "config.h"

#include <uart.h>
#include <cli.h>
#include <lcd.h>
#include <util.h>
#include <mos6502.h>
#include <via.h>
#include <drive.h>
#include <easy41.h>


static void io_init();


unsigned manufacturer, product, word;


/*******************************************************************************
 * Do it.
 *
 ******************************************************************************/
int main(void)
{
    streaming chan mos6502_chan;
    streaming chan uart_input_chan;
    streaming chan uart_output_chan;
    streaming chan mos6502_via1_chan;
    streaming chan mos6502_via2_chan;
    streaming chan drive_ctrl_chan;
    streaming chan drive_data_chan;
    streaming chan drive_ui_chan;
    streaming chan mos6502_ui_chan;
    streaming chan cli_ui_chan;
    unsigned start_time;

    // Emulation will start in 10 ms
    {
        timer tmr;
        tmr :> start_time;
    }
    start_time += 10 * XS1_TIMER_KHZ;

    par
    {
        uart_rx_thread(uart_input_chan);
        uart_tx_thread(uart_output_chan);
        via1_thread(start_time, mos6502_via1_chan);
        via2_thread(start_time, mos6502_via2_chan,
                    drive_ctrl_chan, drive_data_chan);
        drive_thread(drive_ctrl_chan, drive_data_chan, drive_ui_chan);
        mos6502_thread(start_time, mos6502_chan,
                       mos6502_via1_chan, mos6502_via2_chan, mos6502_ui_chan);
        ui_thread(drive_ui_chan, mos6502_ui_chan, uart_input_chan);
    }

    return 0;
}

