#
# Rules.mk - Rules for creating files
#
# (c) 2008 Thomas Giesel <skoe@directbox.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

rodata_section_start := .section .dp.data, \"awd\", @progbits

#############################################################################
# Create .o in objdir from .c in the corresponding srcdir
#
# "-Xpreprocessor -MQ -Xpreprocessor $@" is a workaround for the MDD-bug
#
$(objdir)%.o: $(srcdir)%.c
	echo 'compiling $(notdir $<) ...'
	mkdir -p $(dir $@)
	$(CC) -MMD -c $(CFLAGS) $(DEFINE) $(INCLUDE) -o $@ $< -Xpreprocessor -MQ -Xpreprocessor $@

#############################################################################
# Create .o in objdir from .xc in the corresponding srcdir
#
# "-Xpreprocessor -MQ -Xpreprocessor $@" is a workaround for the MDD-bug
#
$(objdir)%.o: $(srcdir)%.xc
	echo 'compiling $(notdir $<) ...'
	mkdir -p $(dir $@)
	$(CC) -MMD -c $(CFLAGS) $(DEFINE) $(INCLUDE) -o $@ $< -Xpreprocessor -MQ -Xpreprocessor $@

#############################################################################
# Create .o in objdir from .S in the corresponding srcdir
#
$(objdir)%.o: $(srcdir)%.S
	echo 'compiling $(notdir $<) ...'
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $(DEFINE) $(INCLUDE) -o $@ $<

#############################################################################
# Create .o from .s
#
%.o: %.s
	echo 'compiling $(notdir $<) ...'
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $(DEFINE) $(INCLUDE) -o $@ $<

#############################################################################
# Create .s in objdir from .bin in the corresponding srcdir
#
$(objdir)%.s: $(srcdir)%.bin $(hexdump)
	echo $(rodata_section_start) > $@
	echo .global $(*F) >> $@
	echo $(*F): >> $@
	$(hexdump) $< >> $@

#############################################################################
# Create .s from .bin
#
%.s: %.bin
	echo $(rodata_section_start) > $@
	echo .global $(*F) >> $@
	echo $(*F): >> $@
	hexdump -v -e '".byte " 1/1 "0x%02x\n"' $< >> $@

#############################################################################
# Create .d in objdir from .c in the corresponding srcdir
#
#$(objdir)%.d: $(srcdir)%.c $(xnfile)
#	echo 'dependencies for $(notdir $<) ...'
#	mkdir -p $(dir $@)
#	$(CC) -MD $(CFLAGS) $(DEFINE) $(INCLUDE) -o $@ $^

#############################################################################
# Create .hex from elf-file in same directory
#
%.hex: %
	echo 'creating $(notdir $@) ...'
	$(OBJCOPY) -O ihex $< $@
	$(SIZE) $<

#############################################################################
# Create .elf from .xe in same directory
#
%.elf: %.xe
	cd $(dir $@) && $(OBJDUMP) --split $<
	mv $(dir $@)image_n0c0.elf $@

#############################################################################
# Create .img (boot image) from .bin in same directory
#
%.img: %.bin
	$(XBOOTIMG) $< $@

#############################################################################
# Create .bin from .stripped.xe in same directory
#
%.bin: %.stripped.xe
	cd $(dir $@) && $(OBJDUMP) --split $<
	mv $(dir $@)image_n0c0.bin $@

#############################################################################
# Create .stripped.xe from .xe in same directory
#
%.stripped.xe: %.xe
	$(OBJDUMP) --strip -o $@ $<

#############################################################################
# Copy config to autoconf.h 
#
$(objdir)/autoconf.h: $(config)
	mkdir -p $(dir $@)
	cp $^ $@

