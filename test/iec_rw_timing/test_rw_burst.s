
;
; This test executes bursts of IEC read/write accesses on the 1541.
; If the emulation is too slow to executed these bursts on time, it will detect
; and report timing errors.
;

.rodata
; -----------------------------------------------------------------------------
.export _drive_code_rw_burst
_drive_code_rw_burst:
; -----------------------------------------------------------------------------
.org $0600
start:
        sei
        ldx #0
        ldy #$ff
loop:
        lda $1800
        stx $1800
        lda $1800
        sty $1800
        lda $1800
        stx $1800
        lda $1800
        sty $1800
        nop
        nop
        nop
        nop
        lda $1800
        adc #$aa
        stx $1800
        lda $1800
        adc #$aa
        sty $1800
        lda $1800
        adc #$aa
        stx $1800
        lda $1800
        adc #$aa
        sty $1800        
        jmp loop
.reloc
; -----------------------------------------------------------------------------
.export _drive_code_rw_burst_end
_drive_code_rw_burst_end:
; -----------------------------------------------------------------------------


.code
; -----------------------------------------------------------------------------
; just to make some noise
.export _host_code_rw_burst
_host_code_rw_burst:
        sei
        ldy #$0f
        ldx #$07
hloop:
        sty $dd00
        stx $dd00
        sty $dd00
        stx $dd00
        sty $dd00
        stx $dd00
        sty $dd00
        stx $dd00
        jmp hloop

