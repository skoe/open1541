

;
; Between capturing ATN and showing the reaction on DATA or CLK there
; should be ~11 us. On an original 1541-II I found about 11.3 us.
;
; To do this measurement we use an ATN signal which has a slightly
; different clock base than the 1541 or replacement drive. With this
; there's a shift between ATN and the time of capturing. A logic
; analyzer shows us indirectly when the drive captured ATN and how
; long it took to place the output to DATA or CLK.
;
; By identifying these two situations the time interval can be verified:
;     ___________          ______________________________________
; ATN           .\________/
;               . .
; Case 1:       . .
;               . |-------------- 11.3 us -------------|
;     ___________________________________________________________
; CLK           .                                      .
;     ____________          ___________________________.
; DAT           . \(ATNACK)/                           \________/
;               .
; Case 2:       .
;               |-------------- 11.3 us -------------|
;     _______________________________________________.          _
; CLK                                                \_________/
;     ____________          _____________________________________
; DAT             \(ATNACK)/
;

.rodata
; -----------------------------------------------------------------------------
.export _drive_code_rw_timing
_drive_code_rw_timing:
; -----------------------------------------------------------------------------
.org $0600
start:
        sei
        ldx #0
loop:
        lda $1800       ;  4
        bpl atn_high    ;  6  +2 +3 ATN_IN low = ATN high

        lda #$02        ;  8  +2    DATA low when ATN was low
        bne store       ; 11  +3
atn_high:
        lda #$08        ;        +2 CLK low when ATN was high
        nop             ;        +2
store:
        sta $1800       ; 15  +4 +4 DATA or CLK low
        stx $1800       ; 19  11 11 DATA and CLK high
        jmp loop        ; 22
.reloc
; -----------------------------------------------------------------------------
.export _drive_code_rw_timing_end
_drive_code_rw_timing_end:
; -----------------------------------------------------------------------------


.code
; -----------------------------------------------------------------------------
.export _host_code_rw_timing
_host_code_rw_timing:
        sei
hloop:
        ldy #$0f        ;  2 ATN low
        ldx #$07        ;  4 ATN high
        sty $dd00       ;  8
        stx $dd00       ; 12
        nop
        nop             ; 16
        bit $ff         ; 19
        jmp hloop       ; 22

