
#include <cbm.h>
#include <stdio.h>
#include <conio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "tests.h"

static char start_drive_code(char* start, char* end);

int main(void)
{
    char    key; 
    uint8_t rv;

    for (;;)
    {
        puts("Select test case:");
        puts("1 - RW Timing");
        puts("2 - RW Burst Speed");

        key = cgetc();

        switch (key)
        {
        case '1':
            rv = start_drive_code(drive_code_rw_timing,
                                  drive_code_rw_timing_end);
            if (!rv)
            {
                puts("RW Timing test running...");
                host_code_rw_timing();
            }
            break;

        case '2':
            rv = start_drive_code(drive_code_rw_burst,
                                  drive_code_rw_burst_end);
            if (!rv)
            {
                puts("RW Burst test running...");
                host_code_rw_burst();
            }
            break;
        }
     }

    return 0;
}

static char start_drive_code(char* start, char* end)
{
    char     cmd[40];
    uint8_t  size, block_size;
    uint8_t  rv;
    uint16_t addr;

    rv = cbm_open(1, 8, 15, "");
    if (rv)
    {
        puts("Open failed");
        puts(strerror(errno));
        return 1;
    }

    size = end - start;
    printf("Send %d bytes to drive\n", size);

    addr = 0x0600;
    strcpy(cmd, "m-w");
    while (size)
    {
        block_size = size;
        if (block_size > 16)
            block_size = 16;

        // create m-w arguments
        cmd[3] = addr & 0xff;
        cmd[4] = addr >> 8;
        cmd[5] = block_size;
        memcpy(cmd + 6, start, block_size);

        if (cbm_write(1, cmd, 6 + block_size) != 6 + block_size)
        {
            puts("Failed to write drive code");
            puts(strerror(errno));
            return 1;
        }
        size -= block_size;
        start += block_size;
        addr += block_size;
    }

    if (cbm_write(1, "m-e\x00\x06", 5) != 5)
    {
        puts("Failed to start drive code");
        puts(strerror(errno));
        return 1;
    }

    return 0;
}
