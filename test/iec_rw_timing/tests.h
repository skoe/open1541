
#ifndef TESTS_H
#define TESTS_H

extern char drive_code_rw_timing[];
extern char drive_code_rw_timing_end[];
extern char drive_code_rw_burst[];
extern char drive_code_rw_burst_end[];

void host_code_rw_timing();
void host_code_rw_burst();

#endif
